//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_wavetank_Metadata_h
#define smtk_simulation_wavetank_Metadata_h

#include "smtk/simulation/wavetank/Exports.h"

#include <string>

namespace smtk
{
namespace simulation
{
namespace wavetank
{

/**\brief Metadata specific to WaveTank projects.
 *
 * Includes both static constants used to across the code base and
 * instance data that are initialized by the application.
 */

struct SMTKWAVETANK_EXPORT Metadata
{
  // Static constants - initialized in the cxx file
  static const std::string PROJECT_TYPE;            // "foam.wavetank"
  static const std::string PROJECT_FILE_EXTENSION;  // ".project.smtk";
  static const std::string PROJECT_ATTRIBUTES_ROLE; // "attributes"
  static const std::string PROJECT_MODEL_ROLE;      // "model"

  // Directory containing wavetank.sbt and wavetank.py
  // Applications must set this!
  static std::string WORKFLOWS_DIRECTORY;

  // Directory containing python operations
  static std::string OPERATIONS_DIRECTORY;
};

} // namespace wavetank
} // namespace simulation
} // namespace smtk

#endif
