//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_wavetank_qt_qtControlsViewConfig_h
#define smtk_simulation_wavetank_qt_qtControlsViewConfig_h

#include "smtk/simulation/wavetank/qt/Exports.h"

#include <QObject>

#include <QString>
#include <QStringList>

#include <string>

#include "smtk/PublicPointerDefs.h"

namespace smtk
{
namespace simulation
{
namespace wavetank
{

/** \brief Encapsulates data used in pqWaveTankControlsView. */
class SMTKWAVETANKQTEXT_EXPORT qtControlsViewConfig : QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  qtControlsViewConfig(const smtk::view::ConfigurationPtr config, QObject* parent = nullptr);
  ~qtControlsViewConfig() = default;

  const QString& viewName() const { return m_viewName; }
  const QString& category() const { return m_category; }
  const QString& foamApplication() const { return m_foamApp; } // blockMesh, refineMesh, etc
  const QStringList& foamArguments() const { return m_foamArguments; }
  const QString& shellScript() const { return m_shellScript; } // Allrun, Allclean, etc
  bool runInParentDirectory() const { return m_runInParentDirectory; }
  bool canAbort() const { return m_canAbort; }
  const QString& caseDirectory();
  bool deleteCaseDirectory() const { return m_deleteCaseDirectory; }
  const QString& foamfileName() const { return m_foamfileName; }
  const QString& foamfilePath();
  const QString& resourceRole() const { return m_resourceRole; }
  const QString& logfilePath();
  const QString& defaultRepresentation() const { return m_defaultRepresentation; }

  const std::string& operationType() const { return m_operationType; }
  bool useOperationDialog() const { return m_useOperationDialog; }

  bool isExport() const { return m_isExport; }

  /** \brief Solver name can change at runtime */
  void setSolverName(const QString& solverName);

  void dump() const; // Writes values to qDebug()

protected:
  void initialize(const smtk::view::ConfigurationPtr config);

  enum GeometryType
  {
    None = 0,
    Resource,
    FoamFile
  };

  // Configuration data
  QString m_viewName;
  QString m_category;
  QString m_foamApp;
  QStringList m_foamArguments;
  QString m_shellScript;
  bool m_runInParentDirectory = false; // run foam app or script in case_dir's parent
  bool m_canAbort = false;
  QString m_caseDirectory;
  bool m_deleteCaseDirectory = false; // delete case dir before running operation
  GeometryType m_geometryType = GeometryType::None;
  QString m_foamfileName; // create empty file when app/script completes successfully
  QString m_resourceRole; // project role that resource is assigned
  QString m_defaultRepresentation;

  std::string m_operationType;
  bool m_useOperationDialog = false;
  bool m_isExport = false;

  // Project-specific data
  QString m_caseName;
  QString m_foamfilePath;
  QString m_logfilePath;

private:
};

} // namespace wavetank
} // namespace simulation
} // namespace smtk

#endif
