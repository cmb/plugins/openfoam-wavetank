//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_wavetank_qt_qtOpenFoamRunner_h
#define smtk_simulation_wavetank_qt_qtOpenFoamRunner_h

#include "smtk/simulation/wavetank/qt/Exports.h"

#include <QObject>
#include <QString>
#include <QStringList>
#include <QTextStream>

class QDir;
class QFile;
class QProcess;
class QWidget;

class qtLogDialog;

#define DEFAULT_CONTAINER_ENGINE "docker" // for testing, substitute something like "nodocker"

#define OPENFOAM_DOCKER_REGISTRY "docker.io"
#define OPENFOAM_DOCKER_IMAGE_TAG "2112" // for testing, substitute something like "2000"
#define BASE_DOCKER_IMAGE "opencfd/openfoam-run:" OPENFOAM_DOCKER_IMAGE_TAG
#define OPENFOAM_DOCKER_IMAGE "cmb/openfoam-run:" OPENFOAM_DOCKER_IMAGE_TAG

namespace smtk
{
namespace simulation
{
namespace wavetank
{

/**\brief Singleton class for launching OpenFOAM apps as external processes.
 *
 * Application MUST call setParentWidget() before using, so that the
 * qtLogDialog is instantiated.
*/

class SMTKWAVETANKQTEXT_EXPORT qtOpenFoamRunner : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static qtOpenFoamRunner* instance(QObject* parent = nullptr);
  void setParentWidget(QWidget* widget);

  enum ProcessState
  {
    NotRunning = 0,
    Starting,
    Running
  };

  void useOpenFoamDocker(bool enabled) { m_useDocker = enabled; }

  /** \brief Launch OpenFOAM application.
     *
     * Runs the specified application as a detached process, setting up log
     * file, polling for completion, and notifying app when process has finished.
     */
  void start(
    const QString& viewName,
    const QString& application,
    const QString& caseFolder,
    const QStringList& arguments,
    bool runInParent = false,
    bool canAbort = false);

  /** \brief Launch shell script.
     *
     * Runs the specified script as a separate process, sending stdout to progress dialog,
     * and notifying app when process has finished.
     */
  void startShellScript(
    const QString& viewName,
    const QString& script,
    const QString& caseFolder,
    bool runInParent = false,
    bool canAbort = false);

  ProcessState state() const { return m_state; }

  void showLogFile(const QString& path);

Q_SIGNALS:
  void finished(qint64 pid, int exitCode, QString viewName);
  void started(qint64 pid, QString viewName);

public Q_SLOTS:

protected Q_SLOTS:
  void onProcessStarted();
  void onAbortClicked();

protected:
  qtOpenFoamRunner(QObject* parent = nullptr);
  ~qtOpenFoamRunner() override;

  bool
  setupProcess(const QString& executable, const QString& runFolder, const QStringList& arguments);
  void startRefineMesh(const QString& caseFolder);
  void continueRefineMesh();
  bool isAbortSupported() const;

  // Store name of view that invokes processing, to pass to emitted signals
  QString m_viewName;

  QProcess* m_process = nullptr;
  QWidget* m_parentWidget = nullptr;
  QString m_caseFolder;
  ProcessState m_state = ProcessState::NotRunning;
  qint64 m_pid = 0;
  bool m_canAbort = false;

  // Classes for displaying the logfile
  qtLogDialog* m_logDialog = nullptr;
  QFile* m_logFile = nullptr;

  bool m_useDocker = false;

  // Internal vars for sequencing refineMesh runs
  int m_refineMeshCurrentStep = 0; // current iteration number
  int m_refineMeshLastStep = 0;    // final iteration number
  QString m_refineMeshApp;         // toggles between "topoSet" and "refineMesh"
  QString m_refineMeshCaseFolder;
};

} // namespace wavetank
} // namespace simulation
} // namespace smtk

#endif
