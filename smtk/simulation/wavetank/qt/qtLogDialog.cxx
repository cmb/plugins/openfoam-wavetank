//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "qtLogDialog.h"
#include "ui_qtLogDialog.h"

qtLogDialog::qtLogDialog(QWidget* parent)
  : QDialog(parent)
  , m_ui(new Ui::qtLogDialog)
{
  this->initialize();
}

void qtLogDialog::setCloseButtonEnabled(bool enable)
{
  m_ui->m_closeButton->setEnabled(enable);
}

void qtLogDialog::setAbortButtonVisible(bool visible)
{
  m_ui->m_abortButton->setVisible(visible);
}

void qtLogDialog::setAbortButtonEnabled(bool enable)
{
  m_ui->m_abortButton->setEnabled(enable);
}

void qtLogDialog::clearText()
{
  m_ui->m_textEdit->clear();
}

void qtLogDialog::appendPlainText(const QString& text)
{
  m_ui->m_textEdit->appendPlainText(text);
}

void qtLogDialog::setPlainText(const QString& text)
{
  m_ui->m_textEdit->setPlainText(text);
}

void qtLogDialog::initialize()
{
  m_ui->setupUi(this);
  this->setModal(false);
  this->setSizeGripEnabled(true);

  // Hide Abort button by default
  m_ui->m_abortButton->setVisible(false);

  // Set font
  const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
  m_ui->m_textEdit->document()->setDefaultFont(fixedFont);

  // Hide the question mark in the Window Title Bar
  this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

  this->resize(640, 480);
  this->setMinimumSize(300, 260);

  // Connect buttons to external signals
  QObject::connect(m_ui->m_closeButton, &QPushButton::clicked, [this]() {
    Q_EMIT this->closeClicked();
    this->close();
  });
  QObject::connect(m_ui->m_abortButton, &QPushButton::clicked, this, &qtLogDialog::abortClicked);
}
