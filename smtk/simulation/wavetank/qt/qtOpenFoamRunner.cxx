//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/wavetank/qt/qtOpenFoamRunner.h"

#include "smtk/simulation/wavetank/qt/qtLogDialog.h"
#include "smtk/simulation/wavetank/qt/qtSessionData.h"

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFont>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QProcess>
#include <QProcessEnvironment>
#include <QTextStream>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include <QtGlobal>

#include <nlohmann/json.hpp>

#define CONTAINER_NAME "OpenFOAM_WaveTank"

namespace
{
// Singleton instance
static smtk::simulation::wavetank::qtOpenFoamRunner* g_instance = nullptr;
} // namespace

namespace smtk
{
namespace simulation
{
namespace wavetank
{

qtOpenFoamRunner* qtOpenFoamRunner::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new qtOpenFoamRunner(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

qtOpenFoamRunner::qtOpenFoamRunner(QObject* parent)
  : QObject(parent)
  , m_process(new QProcess(this))
  , m_logFile(new QFile(this))
{
  QObject::connect(m_process, &QProcess::started, this, &qtOpenFoamRunner::onProcessStarted);

  QObject::connect(m_process, &QProcess::readyReadStandardOutput, [this]() {
    QByteArray byteArray = m_process->readAllStandardOutput();
    QString text = byteArray.data();
    m_logDialog->appendPlainText(text);
    if (m_logFile->isOpen())
    {
      m_logFile->write(byteArray);
    }
  });

  QObject::connect(m_process, &QProcess::readyReadStandardError, [this]() {
    m_process->setReadChannel(QProcess::StandardError);
    while (m_process->canReadLine())
    {
      QByteArray byteArray = m_process->readLine();
      qCritical() << byteArray.data();
      if (m_logFile->isOpen())
      {
        m_logFile->write(byteArray);
      }
    }
  });

  QObject::connect(
    m_process,
    QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
    [this](int exitCode, QProcess::ExitStatus exitStatus) {
      qInfo() << "Process " << this->m_pid << "finished; exit code" << exitCode;
      Q_EMIT this->finished(m_pid, exitCode, m_viewName);

      m_pid = 0;
      this->m_state = ProcessState::NotRunning;

      if (this->m_refineMeshLastStep > 0)
      {
        this->continueRefineMesh();
        return;
      }

      m_viewName = "";
      // qInfo() << "Closing logfile";
      m_logFile->close();
      m_logDialog->setCloseButtonEnabled(true);
      m_logDialog->setAbortButtonEnabled(false);
    });
}

qtOpenFoamRunner::~qtOpenFoamRunner()
{
  if (m_logFile->isOpen())
  {
    m_logFile->close();
  }
}

void qtOpenFoamRunner::setParentWidget(QWidget* widget)
{
  // qInfo() << "Enter setParentWidget()";
  m_parentWidget = widget;

  // Initialize m_logDialog
  m_logDialog = new qtLogDialog(widget);

  // Set connections
  QObject::connect(
    m_logDialog, &qtLogDialog::abortClicked, this, &qtOpenFoamRunner::onAbortClicked);
}

void qtOpenFoamRunner::start(
  const QString& viewName,
  const QString& application,
  const QString& caseFolder,
  const QStringList& arguments,
  bool runInParent,
  bool canAbort)
{
  m_viewName = viewName;
  m_caseFolder = caseFolder;
  auto* sessionData = qtSessionData::instance();
  auto foamMode = sessionData->openFoamMode();
  m_canAbort = canAbort && this->isAbortSupported();

  // If we aren't in the middle of a refineMesh sequence...
  if (m_refineMeshCurrentStep == 0)
  {
    // Set up logs directory
    QDir logsDir(caseFolder);
    logsDir.mkdir("logs");
    logsDir.cd("logs");

    QString logFile = QString("%1.log").arg(application);
    QString logPath = logsDir.absoluteFilePath(logFile);
    // Should we use a separate file for stderr?

    m_logDialog->setWindowTitle(logFile);
    m_logDialog->setAbortButtonVisible(m_canAbort);
    m_logDialog->setAbortButtonEnabled(false);
    m_logFile->setFileName(logPath);

    if (!m_logFile->open(QIODevice::Truncate | QIODevice::WriteOnly))
    {
      qWarning() << "Failed to open logfile" << m_logFile->fileName();
      m_viewName = "";
      return;
    }

    // Check if starting refineMesh sequence
    if (application == "refineMesh")
    {
      this->startRefineMesh(caseFolder);
      return;
    }
  } // if (m_refineMeshStep == 0)

  // Set runtime directory
  QString runFolder = caseFolder;
  if (runInParent)
  {
    QDir caseDir(caseFolder);
    caseDir.cdUp();
    runFolder = caseDir.absolutePath();
  }

  if (foamMode == OpenFoamMode::TestLocal)
  {
    qInfo() << "TestLocal Run:" << application << arguments;
    qInfo() << "Working Directory:" << runFolder;
    m_viewName = "";
    return;
  }

  if (!this->setupProcess(application, runFolder, arguments))
  {
    m_viewName = "";
    return;
  }

  m_process->start(QIODevice::ReadOnly);
  m_state = ProcessState::Starting;
}

void qtOpenFoamRunner::startShellScript(
  const QString& viewName,
  const QString& script,
  const QString& caseFolder,
  bool runInParent,
  bool canAbort)
{
  m_viewName = viewName;
  m_caseFolder = caseFolder;
  auto* sessionData = qtSessionData::instance();
  auto foamMode = sessionData->openFoamMode();
  m_canAbort = canAbort && this->isAbortSupported();

  if (foamMode == OpenFoamMode::TestLocal)
  {
    m_viewName = "";
    qInfo() << "TestLocal Run:" << script;
    qInfo() << "Working Directory:" << caseFolder;
    return;
  }

  // Set up logs directory
  QDir logsDir(caseFolder);
  logsDir.mkdir("logs");
  logsDir.cd("logs");

  QString logFile = QString("%1.log").arg(script);
  QString logPath = logsDir.absoluteFilePath(logFile);

  m_logDialog->setWindowTitle(logFile);
  m_logDialog->setAbortButtonVisible(m_canAbort);
  m_logDialog->setAbortButtonEnabled(false);

  m_logFile->setFileName(logPath);
  if (!m_logFile->open(QIODevice::Truncate | QIODevice::WriteOnly))
  {
    qWarning() << "Failed to open logfile" << m_logFile->fileName();
    m_viewName = "";
    return;
  }

  QStringList arguments;
  arguments << ">" << logFile << "2>&1";

  QString runFolder = caseFolder;
  if (runInParent)
  {
    QDir caseDir(caseFolder);
    caseDir.cdUp();
    runFolder = caseDir.absolutePath();
  }

  if (!this->setupProcess(script, runFolder, arguments))
  {
    m_viewName = "";
    return;
  }

  m_process->start(QIODevice::ReadOnly);
  m_state = ProcessState::Starting;
}

void qtOpenFoamRunner::onProcessStarted()
{
  m_pid = this->m_process->processId();
  qInfo() << "Started process" << m_pid;
  m_state = ProcessState::Running;
  Q_EMIT this->started(m_pid, m_viewName);

  if (m_refineMeshCurrentStep == 0)
  {
    m_logDialog->clearText();
  }

  m_logDialog->show(); // non-modal
  m_logDialog->raise();
  m_logDialog->setAbortButtonEnabled(m_canAbort);
}

void qtOpenFoamRunner::showLogFile(const QString& path)
{
  if (this->state() == ProcessState::Running)
  {
    m_logDialog->show();
    m_logDialog->raise();
    return;
  }

  // Sanity check
  if (m_logFile->isOpen())
  {
    qWarning() << "Closing m_logFile";
    m_logFile->close();
  }

  QFile file;
  file.setFileName(path);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    qWarning() << "Failed to open logfile" << path;
    return;
  }

  // We don't expect big (MB-size) files, so just read the whole thing
  QByteArray byteArray = file.readAll();
  QString text = byteArray.data();
  m_logDialog->setPlainText(text);
  m_logDialog->show(); // non-modal
  m_logDialog->raise();
}

void qtOpenFoamRunner::onAbortClicked()
{
  qInfo() << "Abort button clicked";
  auto state = m_process->state();
  if (state == QProcess::NotRunning)
  {
    qWarning() << "Process is not running";
    return;
  }

  auto* sessionData = qtSessionData::instance();
  auto foamMode = sessionData->openFoamMode();
  if (foamMode == OpenFoamMode::ContainerEngine)
  {
    m_logDialog->appendPlainText("Stopping OpenFOAM container...");
    qInfo() << "Stopping OpenFOAM container";
    QString program = sessionData->containerEngine();
    QStringList args;
    args << "stop" << CONTAINER_NAME;
    QProcess::execute(program, args);

    args.clear();
    args << "rm" << CONTAINER_NAME;
    QProcess::execute(program, args);
  }
  else
  {
    // First kill child processes - only supported on linux
#if defined(Q_OS_LINUX)
    m_logDialog->appendPlainText(QString("\n\n==> Killing children processes...\n"));
    QProcess process;
    QStringList args;
    args << "--ppid" << QString::number(m_pid) << "-o"
         << "pid"
         << "--no-heading";
    process.start("ps", args);
    process.waitForFinished(5000);
    QString stdout = process.readAllStandardOutput();
    QStringList childPidList = stdout.split('\n', Qt::SkipEmptyParts);
    QProcess::execute("kill", childPidList);
#endif

    // Then kill main process
    qInfo() << "Killing process" << m_pid;
    m_logDialog->appendPlainText(QString("==> Killing main process...\n"));
    m_process->kill();
  }

  m_logDialog->setAbortButtonEnabled(false);

  m_logDialog->appendPlainText(QString("==> Processing aborted.\n"));
  QDir caseDir(m_caseFolder);
  QString note = QString("%1 case not in a valid state: %1.\n").arg(caseDir.dirName());
  m_logDialog->appendPlainText(note);
  qInfo() << "Processing aborted: " << note;
}

bool qtOpenFoamRunner::setupProcess(
  const QString& executable,
  const QString& runFolder,
  const QStringList& arguments)
{
  if (this->state() != ProcessState::NotRunning)
  {
    qWarning() << __FILE__ << __LINE__ << "QProcess is already running";
    return false;
  }

  auto* sessionData = qtSessionData::instance();

  QString program;
  QStringList processArguments;

  auto foamMode = sessionData->openFoamMode();
  if (foamMode == OpenFoamMode::LocalInstall)
  {
    program = executable;
    processArguments = arguments;
  }
  else if (foamMode == OpenFoamMode::ContainerEngine)
  {
    program = sessionData->containerEngine();
    processArguments << "run"
                     << "--rm";

    if (program.contains("podman"))
    {
      processArguments << "--userns=keep-id";
    }
    else
    {
      QString uid = sessionData->dockerUID();
      QString gid = sessionData->dockerGID();
      QString userArg = QString("--user=%1:%2").arg(uid).arg(gid);
      processArguments << userArg;
    }

    QString volumeArg = QString("--volume=%1:/home/openfoam:z").arg(runFolder);
    processArguments << volumeArg;

    processArguments << "--name" << CONTAINER_NAME;
    processArguments << OPENFOAM_DOCKER_IMAGE << executable << arguments;
  }
  else
  {
    QString modeString = sessionData->openFoamModeAsString();
    qWarning() << "Internal Error: cannot run OpenFOAM in mode" << modeString;
    return false;
  }

  qInfo() << "Process Program:" << program;
  qInfo() << "Process Args:" << processArguments;
  qInfo() << "Process Directory:" << runFolder;

  m_process->setProgram(program);
  m_process->setArguments(processArguments);
  m_process->setWorkingDirectory(runFolder);

  // Also set PWD in env - needed for some binary OpenFOAM installs
  QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
  env.insert("PWD", runFolder);
  m_process->setProcessEnvironment(env);

  return true;
}

void qtOpenFoamRunner::startRefineMesh(const QString& caseFolder)
{
  // The operation created refineMeshDict and topoSetDict.* files
  // in the system subdirectory. Find out how many.
  QDir systemDir(caseFolder);
  systemDir.cd("system");
  QStringList nameFilters;
  nameFilters << "topoSetDict.*";
  QStringList topoSetList = systemDir.entryList(nameFilters);
  int count = topoSetList.size();
  if (count < 1)
  {
    qWarning() << "Did not find any topoSetDict.* files";
    return;
  }

  m_refineMeshCurrentStep = 0;
  m_refineMeshLastStep = count;
  m_refineMeshApp.clear();
  m_refineMeshCaseFolder = caseFolder;

  m_logDialog->clearText();
  this->continueRefineMesh();
}

void qtOpenFoamRunner::continueRefineMesh()
{
  QStringList arguments;

  if (m_refineMeshApp.isEmpty())
  {
    // Proceed to first iteration
    m_refineMeshApp = "topoSet";
    m_refineMeshCurrentStep = 1;
    arguments << "-dict"
              << "system/topoSetDict.1";
  }
  else if (m_refineMeshApp == "topoSet")
  {
    m_refineMeshApp = "refineMesh";
    arguments << "-dict"
              << "system/refineMeshDict"
              << "-overwrite";
  }
  else if (m_refineMeshApp == "refineMesh")
  {
    // Check if we are done
    if (m_refineMeshCurrentStep >= m_refineMeshLastStep)
    {
      m_refineMeshCurrentStep = 0;
      m_refineMeshLastStep = 0;
      m_refineMeshApp.clear();
      m_refineMeshCaseFolder.clear();

      m_logFile->close();
      this->m_state = ProcessState::NotRunning;
      m_logDialog->appendPlainText("\nAll refineMesh processing steps completed.\n");
      return;
    }

    // (else) Proceed to next iteration
    m_refineMeshCurrentStep++;
    m_refineMeshApp = "topoSet";
    QString dictFile = QString("system/topoSetDict.%1").arg(m_refineMeshCurrentStep);
    arguments << "-dict" << dictFile;
  }
  else
  {
    qWarning() << "Internal error: unrecognized m_refineMeshApp" << m_refineMeshApp;
    return;
  }

  // Add message to the dialog
  QString divider = QString("\n%1 %2 of %3:\n\n")
                      .arg(m_refineMeshApp)
                      .arg(m_refineMeshCurrentStep)
                      .arg(m_refineMeshLastStep);
  m_logDialog->appendPlainText(divider);

  this->start(m_viewName, m_refineMeshApp, m_refineMeshCaseFolder, arguments);
}

bool qtOpenFoamRunner::isAbortSupported() const
{
  // Current abort logic only works on linux or when using container
#if defined(Q_OS_LINUX)
  return true;
#else
  auto* sessionData = qtSessionData::instance();
  auto foamMode = sessionData->openFoamMode();
  return foamMode == OpenFoamMode::ContainerEngine;
#endif
}

} // namespace wavetank
} // namespace simulation
} // namespace smtk
