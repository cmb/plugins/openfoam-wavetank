//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_wavetank_qt_qtLogDialog
#define smtk_simulation_wavetank_qt_qtLogDialog

#include <QDialog>

#include "smtk/simulation/wavetank/qt/Exports.h"

namespace Ui
{
class qtLogDialog;
}

/**
 * \brief Dialog for showing logfiles generated when running foam applications.
 *
 * By default, Close button visible and Abort button hidden.
 */
class SMTKWAVETANKQTEXT_EXPORT qtLogDialog : public QDialog
{
  Q_OBJECT

Q_SIGNALS:
  /** \brief Signal emitted when the 'Close' button is clicked - for connecting to external slots. */
  void closeClicked();
  /** \brief Signal emitted when the 'Abort' button is clicked - for connecting to external slots. */
  void abortClicked();

public:
  qtLogDialog(QWidget* parentWidget);

  void setCloseButtonEnabled(bool enable = true);
  void setAbortButtonVisible(bool visible);
  void setAbortButtonEnabled(bool enable = true);

  void clearText();
  void appendPlainText(const QString& text);
  void setPlainText(const QString& text);

public Q_SLOTS:

private Q_SLOTS:

private:
  Ui::qtLogDialog* m_ui;

  void initialize();
};

#endif
