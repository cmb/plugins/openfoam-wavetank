//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_wavetank_qt_qtSessionData_h
#define smtk_simulation_wavetank_qt_qtSessionData_h

#include <QObject>

#include "smtk/simulation/wavetank/qt/Exports.h"

#include "smtk/extension/qt/qtAnalysisView.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/view/Manager.h"

#include <QString>

#include <memory>

/** \brief Singleton class for tracking application data at runtime.

This class provides a common place for stashing data used by various
UI software components. It is similar to the "qtProjectRuntime"
class used in other SMTK plugins.
*/

namespace smtk
{
namespace simulation
{
namespace wavetank
{

// OpenFOAM executable modes
enum OpenFoamMode
{
  Undefined = 0,
  LocalInstall,
  ContainerEngine,
  NotUsed,
  TestLocal // for testing LocalInstall mode without OpenFOAM
};

class SMTKWAVETANKQTEXT_EXPORT qtSessionData : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static qtSessionData* instance(QObject* parent = nullptr);

  std::shared_ptr<smtk::project::Project> project() const { return m_project; }
  void setProject(std::shared_ptr<smtk::project::Project> p) { m_project = p; }
  bool unsetProject(std::shared_ptr<smtk::project::Project> p);

  smtk::operation::ManagerPtr operationManager() const { return m_operationManager; }
  void setOperationManager(smtk::operation::ManagerPtr operationManager)
  {
    m_operationManager = operationManager;
  }

  smtk::view::ManagerPtr viewManager() const { return m_viewManager; }
  void setViewManager(smtk::view::ManagerPtr viewManager) { m_viewManager = viewManager; }

  QString projectDirectory() const;
  QString assetsDirectory() const;
  QString foamDirectory() const;
  QString resourcesDirectory() const;

  QWidget* mainWidget() const { return m_mainWidget; }
  void setMainWidget(QWidget* w) { m_mainWidget = w; }
  smtk::extension::qtAnalysisView* analysisView() const { return m_analysisView; }

  OpenFoamMode openFoamMode() const { return m_openfoamMode; }
  QString openFoamModeAsString() const;
  void setOpenFoamMode(OpenFoamMode mode) { m_openfoamMode = mode; }

  QString containerEngine() const { return m_containerEngine; }
  void setContainerEngine(const QString& engine) { m_containerEngine = engine; }

  QString dockerUID() const { return m_dockerUID; }
  void setDockerUID(const QString& uid) { m_dockerUID = uid; }

  QString dockerGID() const { return m_dockerGID; }
  void setDockerGID(const QString& gid) { m_dockerGID = gid; }

protected:
  qtSessionData(QObject* parent = nullptr)
    : Superclass(parent)
  {
  }
  ~qtSessionData() override;

private:
  Q_DISABLE_COPY(qtSessionData);

  std::shared_ptr<smtk::project::Project> m_project;

  smtk::operation::ManagerPtr m_operationManager;
  smtk::view::ManagerPtr m_viewManager;
  QWidget* m_mainWidget = nullptr;
  smtk::extension::qtAnalysisView* m_analysisView = nullptr;

  OpenFoamMode m_openfoamMode = OpenFoamMode::Undefined;
  QString m_containerEngine;
  QString m_dockerUID;
  QString m_dockerGID;
};

} // namespace wavetank
} // namespace simulation
} // namespace smtk
#endif
