//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/wavetank/Metadata.h"

namespace smtk
{
namespace simulation
{
namespace wavetank
{

// Static constants
const std::string Metadata::PROJECT_TYPE = "foam.wavetank";
const std::string Metadata::PROJECT_FILE_EXTENSION = ".project.smtk";
const std::string Metadata::PROJECT_ATTRIBUTES_ROLE = "attributes";
const std::string Metadata::PROJECT_MODEL_ROLE = "model";
std::string Metadata::WORKFLOWS_DIRECTORY = "";  // must be initalized by application
std::string Metadata::OPERATIONS_DIRECTORY = ""; // must be initalized by application

} // namespace wavetank
} // namespace simulation
} // namespace smtk
