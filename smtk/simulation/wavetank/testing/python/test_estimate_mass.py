# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

import math
import os

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project
import smtk.resource
import smtk.testing

from base_test_case import BaseTestCase

EXPECTED_MASS = 1013.0  # found by inspection
OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)


class TestEstimateMass(BaseTestCase):
    def test_estimate_mass(self):
        self.op = self.import_operation('estimate_mass.py')

        solver = 'overInterDyMFoam'
        project_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', solver)
        smtk_filename = '{}.project.smtk'.format(solver)
        project_file = os.path.join(project_dir, smtk_filename)
        project = self.read_project(project_file)

        # Tweak the water surface so that we get a non-zero volume
        att_resource_set = project.resources().findByRole('attributes')
        att_resource = att_resource_set.pop()
        water_att = att_resource.findAttribute('WaveModel')
        surface_item = water_att.findDouble('WaterSurface')
        surface_item.setValue(0.1)

        # Run the operation
        mass_op = self.op_manager.createOperation('estimate_mass.EstimateMass')
        mass_op.parameters().associate(project)
        mass_result = mass_op.operate()
        mass_outcome = mass_result.findInt('outcome').value()
        if mass_outcome != OP_SUCCEEDED:
            print(self.op.log().convertToString())
            self.assertTrue(False, 'operation outcome {}'.format(mass_outcome))

        mass = mass_result.findDouble('mass').value()
        print('Mass: {}'.format(mass))
        diff = math.fabs(mass - EXPECTED_MASS)
        self.assertLess(diff, 0.1)
        # print(self.op.log().convertToString())


if __name__ == '__main__':
    smtk.testing.process_arguments()
    smtk.testing.main()
