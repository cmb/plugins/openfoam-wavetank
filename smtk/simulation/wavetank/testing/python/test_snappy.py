# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

import os
from pathlib import Path

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project
import smtk.resource
import smtk.session.mesh
import smtk.testing

from base_test_case import BaseTestCase

TEST_PROJECT_NAME = 'overInterDyMFoam'
OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)


class TestSnappyHexMesh(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.project_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', TEST_PROJECT_NAME)

        # Import SnappyHexMesh and SurfaceFeatureExtract operations
        source_dir = Path(__file__).parent
        operations_dir = source_dir / '../../operations'
        operation_files = [
            'snappyhexmesh.py',
            'surfacefeatureextract.py',
        ]

        import_op = self.op_manager.createOperation('smtk::operation::ImportPythonOperation')
        for op_file in operation_files:
            op_path = operations_dir / op_file
            import_op.parameters().findFile('filename').setValue(str(op_path))
            import_result = import_op.operate()
            import_outcome = import_result.findInt('outcome').value()
            self.assertEqual(import_outcome, OP_SUCCEEDED, f'failed to import {op_path}')

    def test_setup(self):
        project_file = os.path.join(self.project_dir, f'{TEST_PROJECT_NAME}.project.smtk')
        project = self.read_project(project_file)
        self._setup_snappymesh(project)

    def _write_project(self, project):
        write_op = self.op_manager.createOperation('smtk::project::Write')
        self.assertIsNotNone(write_op)
        write_op.parameters().associate(project)
        write_result = write_op.operate()
        write_outcome = write_result.findInt('outcome').value()
        self.assertEqual(write_outcome, OP_SUCCEEDED,
                         'write project op outcome {}'.format(write_outcome))

    def _setup_snappymesh(self, project):
        """"""
        setup_op = self.op_manager.createOperation('snappyhexmesh.SnappyHexMesh')
        self.assertIsNotNone(setup_op, 'SnappyHexMesh operation is null')
        setup_op.parameters().associate(project)
        setup_op.parameters().findString('run').setIsEnabled(False)  # don't run OpenFOAM app
        setup_result = setup_op.operate()
        setup_outcome = setup_result.findInt('outcome').value()
        if setup_outcome != OP_SUCCEEDED:
            print(setup_op.log().convertToString())
            self.assertTrue(False, 'operation outcome {}'.format(setup_outcome))

        # Check for copied and generated files
        case_dir = os.path.join(self.project_dir, 'foam/overset')
        rel_paths = [
            'constant/include/snappyHexMeshConfig', 'system/controlDict', 'system/fvSchemes',
            'system/fvSolution', 'system/snappyHexMeshDict', ]
        for rel_path in rel_paths:
            path = os.path.join(case_dir, rel_path)
            if not os.path.exists(path):
                # self.assertTrue(False, 'file not found: {}'.format(path))
                print('file not found: {}'.format(path))


if __name__ == '__main__':
    smtk.testing.process_arguments()
    smtk.testing.main()
