# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

import os
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project
import smtk.resource
import smtk.testing

PROJECT_NAME = 'foam.wavetank'
OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)


class BaseTestCase(smtk.testing.TestCase):
    """"""

    def setUp(self):
        smtk.workflows_folder = os.path.join(smtk.testing.SOURCE_DIR, 'simulation-workflows')

        # Initialize resource & operation managers
        self.res_manager = smtk.resource.Manager.create()
        self.op_manager = smtk.operation.Manager.create()

        smtk.attribute.Registrar.registerTo(self.res_manager)
        smtk.attribute.Registrar.registerTo(self.op_manager)

        smtk.operation.Registrar.registerTo(self.op_manager)
        self.op_manager.registerResourceManager(self.res_manager)

        self.proj_manager = smtk.project.Manager.create(self.res_manager, self.op_manager)
        smtk.project.Registrar.registerTo(self.proj_manager)

        self.proj_manager.registerProject(PROJECT_NAME, set(), set(), '1.0.0')

        # Some extra stuff needed for projects
        self.managers = smtk.common.Managers.create()
        self.managers.insertOrAssign(self.res_manager)
        self.managers.insertOrAssign(self.op_manager)
        self.managers.insertOrAssign(self.proj_manager)
        self.op_manager.setManagers(self.managers)

    def tearDown(self):
        self.res_manager = None
        self.op_manager = None
        self.proj_manager = None

    def import_operation(self, filename):
        """Imports python operation"""
        import_op = self.op_manager.createOperation('smtk::operation::ImportPythonOperation')
        self.assertIsNotNone(import_op)

        relative_path = 'smtk/simulation/wavetank/operations/{}'.format(filename)
        path = os.path.join(smtk.testing.SOURCE_DIR, relative_path)
        self.assertTrue(os.path.exists(path), 'operation file not found: {}'.format(path))

        import_op.parameters().findFile("filename").setValue(path)
        import_res = import_op.operate()
        import_outcome = import_res.findInt('outcome').value()
        self.assertEqual(import_outcome, OP_SUCCEEDED,
                         'import python op outcome {}'.format(import_outcome))

        # Instantiate and run the blockMesh operation
        op_name = import_res.findString('unique_name').value()
        op = self.op_manager.createOperation(op_name)
        self.assertIsNotNone(op, '{} operation is None'.format(op_name))
        return op

    def read_project(self, project_file: str) -> smtk.project.Project:
        """"""
        read_op = self.op_manager.createOperation('smtk::project::Read')
        read_op.parameters().findFile('filename').setValue(project_file)
        read_result = read_op.operate()
        read_outcome = read_result.findInt('outcome').value()
        if read_outcome != OP_SUCCEEDED:
            print(read_op.log().convertToString())
            self.assertTrue(False, 'read project outcome {}'.format(read_outcome))

        project = read_result.findResource('resource').value()
        self.assertIsNotNone(project, 'project is null')
        return project

    def write_project(self, project):
        """"""
        write_op = self.op_manager.createOperation('smtk::project::Write')
        self.assertIsNotNone(write_op)
        write_op.parameters().associate(project)
        write_result = write_op.operate()
        write_outcome = write_result.findInt('outcome').value()
        self.assertEqual(write_outcome, OP_SUCCEEDED,
                         'write project op outcome {}'.format(write_outcome))
