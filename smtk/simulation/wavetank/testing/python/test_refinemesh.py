# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

import os

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project
import smtk.resource
import smtk.session.mesh
import smtk.testing

from base_test_case import BaseTestCase

TEST_PROJECT_NAME = 'overInterDyMFoam'
OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)


class TestRefineMesh(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.project_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', TEST_PROJECT_NAME)

    def test_setup(self):
        project_file = os.path.join(self.project_dir, f'{TEST_PROJECT_NAME}.project.smtk')
        project = self.read_project(project_file)
        self._add_refinement_region(project)
        self._setup_refinemesh(project)

    def _add_refinement_region(self, project):
        """"""
        att_resource_set = project.resources().findByRole('attributes')
        att_resource = att_resource_set.pop()

        # Check for RefinementDirection attribute
        dir_att = att_resource.findAttribute('RefinementDirection')
        if dir_att is None:
            dir_att = att_resource.createAttribute('RefinementDirection')

        # Check for existing RefinementRegion attribute
        att_list = att_resource.findAttributes('RefinementRegion')
        if att_list:
            region_att = att_list[0]
        else:
            region_att = att_resource.createAttribute('RefinementRegion')

        # Create default refinement region surrounding wave input
        box_item = region_att.findDouble('box')
        # Values are xmin, xmax, ymin, ymax, zmin, zmax
        coords = [-20.0, 20.0, -5.0, 5.0, -2.0, 2.0]
        for i, coord in enumerate(coords):
            box_item.setValue(i, coord)

        # Save changes
        self._write_project(project)

    def _write_project(self, project):
        write_op = self.op_manager.createOperation('smtk::project::Write')
        self.assertIsNotNone(write_op)
        write_op.parameters().associate(project)
        write_result = write_op.operate()
        write_outcome = write_result.findInt('outcome').value()
        self.assertEqual(write_outcome, OP_SUCCEEDED,
                         'write project op outcome {}'.format(write_outcome))

    def _setup_refinemesh(self, project):
        setup_op = self.import_operation('refinemesh.py')
        self.assertIsNotNone(setup_op, 'RefineMesh operation is null')
        setup_op.parameters().associate(project)
        setup_op.parameters().findString('run').setIsEnabled(False)  # don't run OpemFOAM app
        # setup_op.parameters().findVoid('TestMode').setIsEnabled(True)
        setup_result = setup_op.operate()
        setup_outcome = setup_result.findInt('outcome').value()
        if setup_outcome != OP_SUCCEEDED:
            print(setup_op.log().convertToString())
            self.assertTrue(False, 'operation outcome {}'.format(setup_outcome))

        # Check for copied and generated files
        case_dir = os.path.join(self.project_dir, f'foam/overset')
        rel_paths = []
        for rel_path in rel_paths:
            path = os.path.join(case_dir, rel_path)
            if not os.path.exists(path):
                # self.assertTrue(False, 'file not found: {}'.format(path))
                print('file not found: {}'.format(path))


if __name__ == '__main__':
    smtk.testing.process_arguments()
    smtk.testing.main()
