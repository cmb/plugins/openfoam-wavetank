# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

import os
from pathlib import Path
import shutil

import smtk
import smtk.attribute
import smtk.io
import smtk.mesh
import smtk.operation
import smtk.project
import smtk.resource
import smtk.session.mesh
import smtk.testing

# Must also be import smtk.session.mesh, but I dont know why.
# Without it, the smtk::mesh::Import operation used in ImportModel is "unable to operate".
import smtk.session.mesh

from base_test_case import BaseTestCase

TEST_PROJECT_NAME = 'overInterDyMFoam'
OUTPUT_DIRECTORY_NAME = 'allrun_example'
OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)


class TestCreateAllrun(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.project_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', TEST_PROJECT_NAME)

        # Register smtk.mesh so project can load model
        smtk.mesh.Registrar.registerTo(self.res_manager)
        smtk.mesh.Registrar.registerTo(self.op_manager)

        # Import CreateAllrun operation and all operations is runs
        source_dir = Path(__file__).parent
        operations_dir = source_dir / '../../operations'
        operation_files = [
            'blockmesh.py',
            'create_allrun.py',
            'mergemeshes.py',
            'overinterdymfoam.py',
            'refinemesh.py',
            'setfields.py',
            'snappyhexmesh.py',
            'surfacefeatureextract.py',
        ]

        import_op = self.op_manager.createOperation('smtk::operation::ImportPythonOperation')
        for op_file in operation_files:
            op_path = operations_dir / op_file
            import_op.parameters().findFile('filename').setValue(str(op_path))
            import_result = import_op.operate()
            import_outcome = import_result.findInt('outcome').value()
            self.assertEqual(import_outcome, OP_SUCCEEDED, f'failed to import {op_path}')

        self.output_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', OUTPUT_DIRECTORY_NAME)
        if os.path.exists(self.output_dir):
            smtk.io.Logger.instance().addRecord(smtk.io.Logger.Info,
                                                f'deleting results directory {self.output_dir}')
            shutil.rmtree(self.output_dir)

    def test_setup(self):
        project_file = os.path.join(self.project_dir, f'{TEST_PROJECT_NAME}.project.smtk')
        project = self.read_project(project_file)

        att_resource_set = project.resources().findByRole('attributes')
        att_resource = att_resource_set.pop()

        # Set object mass (wavebot)
        mass_att = att_resource.findAttribute('MassProperties')
        mass_item = mass_att.findDouble('Mass')
        mass_item.setValue(854.76828)

        # Set overset blockMesh options
        blockmesh_att = att_resource.findAttribute('OversetBlockMesh')
        box_item = blockmesh_att.findDouble('box')
        box_values = [-2, 2, -2, 2, -1, 1]
        for i, value in enumerate(box_values):
            box_item.setValue(i, value)
        mesh_item = blockmesh_att.findString('MeshSize')
        mesh_item.setValue('numcells')
        cells_item = mesh_item.findChild(
            'numcells-eachdir', smtk.attribute.SearchStyle.ACTIVE_CHILDREN)
        cell_values = [25, 25, 12]
        for i, value in enumerate(cell_values):
            cells_item.setValue(i, value)

        # Set overset snappyHexMesh options
        cast_att = att_resource.findAttribute('Castellation')
        cast_group = cast_att.findGroup('Castellation')
        point_item = cast_group.find('InsidePoint', smtk.attribute.SearchStyle.IMMEDIATE)
        point_coords = [-1.5, 0, 0]
        for i, coord in enumerate(point_coords):
            point_item.setValue(i, coord)

        # Set parallel option
        parallel_att = att_resource.findAttribute('Parallel')
        parallel_group = parallel_att.findGroup('Parallel')
        parallel_group.setIsEnabled(True)

        # Write project
        att_resource.setClean(False)
        self.write_project(project)

        # Generate package (allrun files)
        self._create_allrun(project)
        self._check_allrun_files()

    def _create_allrun(self, project):
        allrun_op = self.op_manager.createOperation('create_allrun.CreateAllrun')
        self.assertIsNotNone(allrun_op, 'CreateAllrun operation not imported')

        allrun_op.parameters().associate(project)
        allrun_op.parameters().findDirectory("Directory").setValue(self.output_dir)
        allrun_result = allrun_op.operate()
        allrun_outcome = allrun_result.findInt('outcome').value()

        if allrun_outcome != OP_SUCCEEDED:
            print(allrun_op.log().convertToString())
            self.assertEqual(allrun_outcome, OP_SUCCEEDED, 'CreateAllrun operation did not succeed')
        else:
            # For now, *always* print operation log
            print('CreateAllrun log:')
            print(allrun_op.log().convertToString())

    def _check_allrun_files(self):
        """Spot check output files"""
        self.assertTrue(os.path.exists(self.output_dir),
                        f'no output directory at {self.output_dir} ')


if __name__ == '__main__':
    smtk.testing.process_arguments()
    smtk.testing.main()
