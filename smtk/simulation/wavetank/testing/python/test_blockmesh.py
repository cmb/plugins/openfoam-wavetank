# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

import os

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project
import smtk.resource
import smtk.testing

from base_test_case import BaseTestCase

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)


class TestSetupBlockMesh(BaseTestCase):
    def test_blockmesh(self):
        self.op = self.import_operation('blockmesh.py')
        # self._interfoam()
        self._overset()

    def _interfoam(self):
        """Test interFoam project."""
        self.op.parameters().removeAllAssociations(False)
        solver = 'interFoam'

        project_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', solver)
        smtk_filename = '{}.project.smtk'.format(solver)
        project_file = os.path.join(project_dir, smtk_filename)
        project = self.read_project(project_file)

        att_names = ['BlockMesh']
        self._blockmesh(project, solver, att_names)
        del project

    def _overset(self):
        """Test overInterDyMFoam project."""
        self.op.parameters().removeAllAssociations(False)
        solver = 'overInterDyMFoam'

        project_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', solver)
        smtk_filename = '{}.project.smtk'.format(solver)
        project_file = os.path.join(project_dir, smtk_filename)
        project = self.read_project(project_file)

        att_names = ['BackgroundBlockMesh', 'OversetBlockMesh']
        self._blockmesh(project, solver, att_names)
        del project

    def _blockmesh(self, project: str, solver: str, att_names: list[str]):
        """Test files generated for blockMesh"""
        project_dir = os.path.dirname(project.location())
        print('PROJECT_DIR', project_dir)

        self.op.parameters().associate(project)

        att_resource_set = project.resources().findByRole('attributes')
        att_resource = att_resource_set.pop()

        for att_name in att_names:
            bm_att = att_resource.findAttribute(att_name)
            self.op.parameters().findComponent('Specification').setValue(bm_att)

            self.op.parameters().findString('run').setIsEnabled(False)  # don't run OpemFOAM app
            setup_result = self.op.operate()
            setup_outcome = setup_result.findInt('outcome').value()
            if setup_outcome != OP_SUCCEEDED:
                print(self.op.log().convertToString())
                self.assertTrue(False, 'operation outcome {}'.format(setup_outcome))

            # Set relative path to the foam case file
            if solver == 'interFoam':
                relative_path = 'foam/interFoam'
            elif solver == 'overInterDyMFoam':
                subdir_dict = dict(BackgroundBlockMesh='background', OversetBlockMesh='overset')
                subdir = subdir_dict.get(att_name)
                self.assertIsNotNone(subdir, msg='Unrecognized attribute name {}'.format(att_name))
                relative_path = 'foam/{}'.format(subdir)
            else:
                self.assertTrue(False, msg='Unrecognized solver type {}'.format(solver))
                return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

            # Check for generated files
            case_dir = os.path.join(project_dir, relative_path)
            rel_paths = ['system/controlDict', 'system/blockMeshDict',
                         'constant/include/blockMeshConfig']
            for rel_path in rel_paths:
                path = os.path.join(case_dir, rel_path)
                self.assertTrue(os.path.exists(path), 'file not found: {}'.format(path))

            # Create a fake "polyMesh" folder for downstream testing
            mesh_dir = os.path.join(case_dir, 'constant/polyMesh')
            if not os.path.exists(mesh_dir):
                os.makedirs(mesh_dir)
            readme_path = os.path.join(mesh_dir, 'placeholder.txt')
            with open(readme_path, 'w') as fp:
                fp.write('Placeholder file for internal testing.\n')


if __name__ == '__main__':
    smtk.testing.process_arguments()
    smtk.testing.main()
