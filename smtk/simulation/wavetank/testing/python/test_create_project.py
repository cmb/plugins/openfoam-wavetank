# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

import os
import shutil
import sys

import smtk
import smtk.attribute
import smtk.operation
import smtk.project
import smtk.resource
import smtk.testing

from base_test_case import BaseTestCase

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)


class TestProjectCreate(BaseTestCase):
    def test_interfoam(self):
        """Create interFoam project."""
        self._create('interFoam')

    def test_overset(self):
        """Create overInterDyMFoam project."""
        self._create('overInterDyMFoam')

    def _create(self, solver):
        """"""
        create_op = self.import_operation('create_project.py')
        self.assertIsNotNone(create_op, 'Create operation is null')

        project_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', solver)
        if os.path.exists(project_dir):
            print('Removing directory', project_dir)
            shutil.rmtree(project_dir)
        # create_op.parameters().findVoid('overwrite').setIsEnabled(True)
        create_op.parameters().findDirectory('directory').setValue(project_dir)
        create_op.parameters().findString('solver').setValue(solver)
        create_result = create_op.operate()
        outcome = create_result.findInt('outcome').value()
        if outcome != OP_SUCCEEDED:
            print(create_op.log().convertToString())
            self.assertTrue(False, 'operation outcome {}'.format(outcome))

        # Check that project was created
        project = create_result.findResource('resource').value()
        self.assertIsNotNone(project, 'project is null')

        # Write to disk
        self.write_project(project)

        # Verify project file was created
        smtk_filename = '{}.project.smtk'.format(solver)
        project_file = os.path.join(project_dir, smtk_filename)
        self.assertTrue(os.path.exists(project_file))


if __name__ == '__main__':
    smtk.testing.process_arguments()
    smtk.testing.main()
