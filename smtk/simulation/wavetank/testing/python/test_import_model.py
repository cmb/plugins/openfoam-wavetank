# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

import os

import smtk
import smtk.attribute
import smtk.mesh
import smtk.operation
import smtk.project
import smtk.resource
import smtk.testing

from base_test_case import BaseTestCase

# Must also be import smtk.session.mesh, but I dont know why.
# Without it, the smtk::mesh::Import operation used in ImportModel is "unable to operate".
import smtk.session.mesh

MODEL_DIRECTORY = 'stl'
MODEL_FILENAME = 'waveBOT.stlb'
TEST_PROJECT_NAME = 'overInterDyMFoam'
OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)


class TestImportModel(BaseTestCase):
    def setUp(self):
        super().setUp()

        # Additional stuff for import model
        smtk.mesh.Registrar.registerTo(self.res_manager)
        smtk.mesh.Registrar.registerTo(self.op_manager)

        self.project_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', TEST_PROJECT_NAME)

    def test_import(self):
        project_file = os.path.join(self.project_dir, f'{TEST_PROJECT_NAME}.project.smtk')
        project = self.read_project(project_file)
        self._import_model(project)
        self._write_project(project)

    def _import_model(self, project):
        """"""
        model_op = self.import_operation('import_model.py')
        self.assertIsNotNone(model_op, 'ImportModel operation is null')

        model_op.parameters().associate(project)

        model_file = os.path.join(smtk.testing.DATA_DIR, 'model/3d',
                                  MODEL_DIRECTORY, MODEL_FILENAME)
        self.assertTrue(os.path.exists(model_file), 'model file not found {}'.format(model_file))
        model_op.parameters().findFile('filename').setValue(model_file)
        model_op.parameters().findVoid('overwrite').setIsEnabled(True)  # for idempotence

        # Import model file to create mesh-session resource
        model_result = model_op.operate()
        model_outcome = model_result.findInt('outcome').value()
        if model_outcome != OP_SUCCEEDED:
            print(model_op.log().convertToString())
            self.assertTrue(False, 'import model op outcome {}'.format(model_outcome))

        # Verify that project contains model resource
        resource_set = project.resources().findByRole('model')
        self.assertEqual(len(resource_set), 1, 'Expected 1 "model" resource in the project')

        # Verify that filename item is also set
        resource_set = project.resources().findByRole('attributes')
        self.assertEqual(len(resource_set), 1, 'Expected 1 "attribute" resource in the project')
        att_resource = resource_set.pop()
        object_att = att_resource.findAttribute('Object')
        object_filename_item = object_att.findString('Filename')
        filename = object_filename_item.value()
        self.assertEqual(filename, MODEL_FILENAME, 'Wrong filename')

    def _write_project(self, project):
        write_op = self.op_manager.createOperation('smtk::project::Write')
        self.assertIsNotNone(write_op)
        write_op.parameters().associate(project)
        write_result = write_op.operate()
        write_outcome = write_result.findInt('outcome').value()
        self.assertEqual(write_outcome, OP_SUCCEEDED,
                         'write project op outcome {}'.format(write_outcome))

        # Confirm that source model file was save plus .vtp
        obj_path = os.path.join(self.project_dir, 'assets', MODEL_FILENAME)
        self.assertTrue(os.path.exists(obj_path), 'missing asset file {}'.format(obj_path))

        # Confirm 2 resource files
        resource_folder = os.path.join(self.project_dir, 'resources')
        file_list = os.listdir(resource_folder)
        if len(file_list) < 2:
            print(file_list)
            text = ', '.join(file_list)
            self.log().addRecord(smtk.io.Logger.Info, text)
            text = 'Expected 2 resources in {} instead got {}'.format(
                resource_folder, len(file_list))
            self.assertTrue(False, text)


if __name__ == '__main__':
    smtk.testing.process_arguments()
    smtk.testing.main()
