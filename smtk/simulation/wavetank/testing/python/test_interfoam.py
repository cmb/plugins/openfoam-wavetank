# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

import os
import shutil
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project
import smtk.resource
import smtk.testing

PROJECT_NAME = 'foam.wavetank'
OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)


class TestInterFoam(smtk.testing.TestCase):
    def setUp(self):
        # Initialize resource & operation managers
        self.res_manager = smtk.resource.Manager.create()
        self.op_manager = smtk.operation.Manager.create()

        smtk.attribute.Registrar.registerTo(self.res_manager)
        smtk.attribute.Registrar.registerTo(self.op_manager)

        smtk.operation.Registrar.registerTo(self.op_manager)
        self.op_manager.registerResourceManager(self.res_manager)

        self.proj_manager = smtk.project.Manager.create(self.res_manager, self.op_manager)
        smtk.project.Registrar.registerTo(self.proj_manager)

        self.proj_manager.registerProject(PROJECT_NAME, set(), set(), '1.0.0')

    def tearDown(self):
        self.res_manager = None
        self.op_manager = None
        self.proj_manager = None

    def test_setup_interFoam(self):
        """Test files generated for interFoam"""
        # Read the test project
        project_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', 'interFoam')
        project_file = os.path.join(project_dir, 'interFoam.project.smtk')

        read_op = self.op_manager.createOperation('smtk::project::Read')
        read_op.parameters().findFile('filename').setValue(project_file)
        read_result = read_op.operate()
        read_outcome = read_result.findInt('outcome').value()
        if read_outcome != OP_SUCCEEDED:
            print(read_op.log().convertToString())
            self.assertTrue(False, 'read project outcome {}'.format(read_outcome))

        project = read_result.findResource('resource').value()
        self.assertIsNotNone(project, 'project is null')

        # Import the test operation (InterFoam)
        import_op = self.op_manager.createOperation('smtk::operation::ImportPythonOperation')
        self.assertIsNotNone(import_op)

        path = os.path.join(
            smtk.testing.SOURCE_DIR, 'smtk/simulation/wavetank/operations/interfoam.py')
        self.assertTrue(os.path.exists(path), 'operation file not found: {}'.format(path))

        import_op.parameters().findFile("filename").setValue(path)
        import_res = import_op.operate()
        import_outcome = import_res.findInt('outcome').value()
        self.assertEqual(import_outcome, OP_SUCCEEDED,
                         'import python op outcome {}'.format(import_outcome))

        # Instantiate and run the interFoam operation
        op_name = import_res.findString('unique_name').value()
        setup_op = self.op_manager.createOperation(op_name)
        self.assertIsNotNone(setup_op, 'InterFoam operation is null')
        setup_op.parameters().associate(project)
        setup_op.parameters().findString('run').setIsEnabled(False)  # don't run OpemFOAM app
        setup_result = setup_op.operate()
        setup_outcome = setup_result.findInt('outcome').value()
        if setup_outcome != OP_SUCCEEDED:
            print(setup_op.log().convertToString())
            self.assertTrue(False, 'operation outcome {}'.format(setup_outcome))

        # Get the analysis specified in the attribute resource
        att_resource_set = project.resources().findByRole('attributes')
        att_resource = att_resource_set.pop()
        att = att_resource.findAttribute('Analysis')
        if att is None:
            self.assertTrue(False, 'Missing analysis attribute')
        item = att.findString('Analysis')
        if item is None:
            self.assertTrue(False, 'Missing analysis item')
        analysis = item.value()

        # Set relative path to the foam case file
        if analysis == 'interFoam':
            relative_path = 'foam/interFoam'
        else:
            self.log().addError('Unrecognized analysis type {}'.format(analysis))
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Check for generated files
        case_dir = os.path.join(project_dir, relative_path)
        rel_paths = ['constant/include/wavePropertiesConfig']
        for rel_path in rel_paths:
            path = os.path.join(case_dir, rel_path)
            self.assertTrue(os.path.exists(path), 'file not found: {}'.format(path))


if __name__ == '__main__':
    smtk.testing.process_arguments()
    smtk.testing.main()
