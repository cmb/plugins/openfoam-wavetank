<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5">
  <Definitions>
    <AttDef Type="surface-inertia" Label="Surface Inertia" BaseType="operation">
      <BriefDescription>
        Specify inputs for OpenFOAM surfaceInertia executable
      </BriefDescription>
      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       LockType="DoNotLock" Extensible="false" OnlyResources="true">
        <Accepts>
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Density">
          <BriefDescription>kg/m3 for solid properties, kg/m2 for shell properties</BriefDescription>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="False">0.0</Min>
          </RangeInfo>
        </Double>
        <String Name="Properties">
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Solid">solid</Value>
            <Value Enum="Shell">shell</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="result(setup)" BaseType="result">
      <ItemDefinitions>
        <!-- Command line arguments for foam executable (surfaceInertia) -->
        <String Name="CommandLineArguments" Extensible="true" NumberOfRequiredValues="0" />
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
