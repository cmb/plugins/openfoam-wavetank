<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5">
  <!-- Import geometry file as the object to insert in the wave tank. -->

  <Definitions>
    <AttDef Type="import-model" Label="Import Geometry" BaseType="operation">
      <BriefDescription>
        Import geometry file as the target to place in the wave tank.
      </BriefDescription>
      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       LockType="DoNotLock" Extensible="false" OnlyResources="true">
        <Accepts>
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <File Name="filename" ShouldExist="1" NumberOfRequiredValues="1" FileFilters="Supported Files (*.obj *.stl *.stlb)" />
        <Void Name="overwrite" Label="Overwrite?" Optional="true" IsEnabledByDefault="false" />
        <Void Name="import-vtp" Label="Import .vtp file?" Optional="true" IsEnabledByDefault="true">
          <BriefDescription>vtkPolyData files sometimes import into mesh session when stl and obj dont</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="result(setup)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" HoldReference="true" Extensible="true" NumberOfRequiredValues="0" >
          <Accepts>
            <Resource Name="smtk::model::Resource"/>
            <Resource Name="smtk::mesh::Resource"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
