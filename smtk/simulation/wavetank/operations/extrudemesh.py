# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""ExtrudeMesh operation"""

import os
import shutil
import subprocess
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.abspath(os.path.dirname(__file__))
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin
from process_status import ProcessStatus


class ExtrudeMesh(smtk.operation.Operation, FoamMixin):
    """Configures and optionally runs extrudeMesh and createPatch.

    Uses predefined "foam/overset" directory
    Uses foam_operation.sbt for template.
    NOTE: This operation is NOT idempotent.
    """

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "Setup and run extrudeMesh and createPatch"

    def createSpecification(self):
        spec = self._create_specification(app='extrudeMesh')
        return spec

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Check attributes
        att_names = ['controlDict']
        if not self._check_attributes(att_resource, att_names):
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        workflows_folder = self._get_workflows_folder()
        if workflows_folder is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        solver = self.cd_writer.get_application_name(att_resource)
        if solver != 'overInterDyMFoam':
            message = 'This operation was not designed for {}'.format(solver)
            self.log().addWarning(message)

        project_dir = os.path.abspath(os.path.dirname(project.location()))
        foam_dir = os.path.join(project_dir, 'foam')
        case_dir = os.path.join(foam_dir, 'overset')

        # Copy extrudeMesh and createPath files
        system_dir = os.path.join(case_dir, 'system')
        if not os.path.exists(system_dir):
            os.makedirs(system_dir)

        from_dir = os.path.join(workflows_folder, 'internal/foam/overInterDyMFoam/system')
        for filename in ['extrudeMeshDict', 'createPatchDict']:
            from_path = os.path.join(from_dir, filename)
            to_path = os.path.join(system_dir, filename)
            shutil.copyfile(from_path, to_path)

        include_dir = os.path.join(case_dir, 'constant/include')
        if not os.path.exists(include_dir):
            os.makedirs(include_dir)

        # Generate constant/include/extrudeMeshConfig file
        spec_att = att_resource.findAttribute('OversetBlockMesh')
        box_item = spec_att.findDouble('box')
        ymin = box_item.value(2)
        ymax = box_item.value(3)
        thickness = ymax - ymin
        kv_list = [('thickness', thickness)]
        path = os.path.join(include_dir, 'extrudeMeshConfig')
        with open(path, 'w') as fp:
            self.cd_writer.write_kvlist(fp, kv_list)

        # Check "Run" option
        run_item = self.parameters().findString('run')
        if run_item.isEnabled():
            if not self._run_foam(case_dir, run_item):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        result = self._create_result()
        return result

    def _run_foam(self, case_dir, run_item):
        """Runs extrudeMesh & createPath.

        HARD-CODED to only run synchronous
        """
        self.log().reset()
        smtk.InfoMessage(self.log(), 'Using case_dir {}'.format(case_dir))

        run_mode = run_item.value()
        if run_mode == 'async':  # launch and return
            self.log().addError('Error - async run_mode not supported')
            return False

        elif run_mode != 'sync':
            self.log().addError('internal error - unrecognized run_mode {}'.format(run_mode))
            return False

        args_prefix = []
        if hasattr(smtk, 'use_openfoam_docker') and smtk.use_openfoam_docker:
            args_prefix = ['openfoam-docker', '/']

        log_dir = os.path.join(case_dir, 'logs')
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        # Set PWD for standard (non-docker) OpenFOAM binaries
        env = os.environ.copy()
        env['PWD'] = case_dir

        # Run extrudeMesh then createPatch
        for app in ['extrudeMesh', 'createPatch']:
            logfilename = '{}.log'.format(app)
            logfile = os.path.join(log_dir, logfilename)
            args = args_prefix + [app]
            if app == 'createPatch':
                args += ['-overwrite']
            with open(logfile, 'w') as fp:
                completed_proc = subprocess.run(
                    args, stdout=fp, stderr=fp, cwd=case_dir, env=env, universal_newlines=True)
                if completed_proc.returncode == 0:
                    self.status = ProcessStatus.Completed
                else:
                    self.log().addError(
                        '{} returned code {}'.format(app, completed_proc.returncode))
                    self.status = ProcessStatus.Error
                    return False

        return True
