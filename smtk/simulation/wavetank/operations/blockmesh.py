# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""BlockMesh operation"""

import os
import shutil
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.abspath(os.path.dirname(__file__))
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin
from control_dict_writer import ControlDictWriter
from process_status import ProcessStatus


class BlockMesh(smtk.operation.Operation, FoamMixin):
    """Configures and optionally runs blockMesh to generate background mesh.

    Generates controlDict and blockMeshDict files.
    Runs blockMesh either sync or async mode.
    """

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "Setup and run blockMesh"

    def createSpecification(self):
        spec = self._create_specification(app='blockMesh', sbt_file='blockmesh.sbt')
        return spec

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        # print('att_resource:', att_resource)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        workflows_folder = self._get_workflows_folder()
        if workflows_folder is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Check attributes
        att_names = ['controlDict']
        if not self._check_attributes(att_resource, att_names):
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        solver = self.cd_writer.get_application_name(att_resource)

        # Because there are more than 1 BlockMesh attributes,
        # get the selected BlockMesh on from the parameters
        comp_att = self.parameters().findComponent('Specification')
        spec_att = comp_att.value()

        # Get case directory
        case_att = self.parameters().findDirectory('CaseDirectory')
        if case_att.isEnabled():
            case_dir = case_att.value()
            case_name = os.path.basename(case_dir)
        else:
            case_name = self._get_case_name(solver, spec_att)
            if case_name is None:
                return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

            project_dir = os.path.abspath(os.path.dirname(project.location()))
            case_dir = os.path.join(project_dir, 'foam', case_name)

        constant_dir = os.path.join(case_dir, 'constant')
        system_dir = os.path.join(case_dir, 'system')
        for path in [constant_dir, system_dir]:
            if not os.path.exists(path):
                os.makedirs(path)

        # Generate controlDict file
        if not self.cd_writer.generate_controlDict(case_dir, att_resource):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        foam_dir = os.path.join(workflows_folder, 'internal/foam/{}'.format(solver))

        from_path = os.path.join(foam_dir, 'system')
        shutil.copytree(from_path, system_dir, dirs_exist_ok=True)

        # For overInterDyMFoam solver, select and copy the individual blockMeshDict file
        if solver == 'overInterDyMFoam':
            bmd_filename = 'blockMeshDict'
            if self._is_2d_analysis(att_resource):
                bmd_filename += '_2d'
            dict_file = os.path.join(foam_dir, case_name, bmd_filename)
            to_file = os.path.join(system_dir, 'blockMeshDict')
            shutil.copy(dict_file, to_file)

        # Generate blockMeshConfig file
        if not self._generate_blockMeshConfig(case_dir, att_resource):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Check "Run" option
        run_item = self.parameters().findString('run')
        if run_item.isEnabled():
            foamfile = None
            if solver == 'interFoam':
                foamfile = 'mesh.foam'
            elif solver == 'overInterDyMFoam':
                if case_name in ['background', 'overset']:
                    foamfile = 'background.foam'
                elif case_name == 'analysis':
                    foamfile = 'merged.foam'
            if not self._run_openfoam('blockMesh', case_dir, run_item, foamfile=foamfile):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        result = self._create_result()
        return result

    def _generate_blockMeshConfig(self, case_dir: str, att_resource: smtk.attribute.Resource) -> bool:
        """Writes constant/include/blockMeshConfig file. Returns True on success."""
        # Build (key,value) list
        kv_list = list()
        complete = False

        scale = 1.0
        scale_att = att_resource.findAttribute('Scale')
        if scale_att is not None:
            scale_item = scale_att.findDouble('scale')
            scale = scale_item.value()
        kv_list.append(('scale', scale))

        comp_att = self.parameters().findComponent('Specification')
        spec_att = comp_att.value()
        box_item = spec_att.itemAtPath('box')
        names = ['Xmin', 'Xmax', 'Ymin', 'Ymax', 'Zmin', 'Zmax']
        for i, name in enumerate(names):
            kv_list.append((name, box_item.value(i)))

        size_item = spec_att.findString('MeshSize')
        cell_counts = self._get_cell_counts(size_item, box_item)
        if cell_counts is None:
            return False
        names = ['xCells', 'yCells', 'zCells']
        for t in zip(names, cell_counts):
            kv_list.append(t)

        # Make sure directory exists
        include_dir = os.path.join(case_dir, 'constant/include')
        if not os.path.exists(include_dir):
            os.makedirs(include_dir)

        path = os.path.join(include_dir, 'blockMeshConfig')
        with open(path, 'w') as fp:
            self.cd_writer.write_kvlist(fp, kv_list, double_space=False)
            complete = True

        return complete

    def _get_cell_counts(self, meshsize_item: smtk.attribute.StringItem, box_item: smtk.attribute.DoubleItem) -> bool:
        """Parses options in MeshSize item."""
        # Each option has one child item, so find the active one
        numcells_item = meshsize_item.findChild(
            'numcells-eachdir', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
        if numcells_item is not None:
            cell_counts = [None] * 3
            for i in range(3):
                cell_counts[i] = numcells_item.value(i)
            return cell_counts

        # Remaining cases need the lengths of each direction and the max length
        lengths = [None] * 3
        lengths[0] = box_item.value(1) - box_item.value(0)
        lengths[1] = box_item.value(3) - box_item.value(2)
        lengths[2] = box_item.value(5) - box_item.value(4)
        max_length = max(lengths)

        # Determine target cell size
        numcells = [None] * 3
        cell_size = None

        maxdir_child = meshsize_item.findChild(
            'numcells-maxdir', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
        if maxdir_child is not None:
            cell_size = max_length / maxdir_child.value()

        relative_child = meshsize_item.findChild(
            'relative-meshsize', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
        if relative_child is not None:
            cell_size = max_length * relative_child.value()

        absolute_child = meshsize_item.findChild(
            'absolute-meshsize', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
        if absolute_child is not None:
            cell_size = absolute_child.value()

        if cell_size is None:
            self.log().addError('Error finding mesh size')
            return None

        for i in range(3):
            numcells[i] = int(lengths[i] / cell_size)

        return numcells
