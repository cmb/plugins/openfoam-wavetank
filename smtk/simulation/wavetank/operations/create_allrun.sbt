<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5">
  <!-- Generate directories & scripts for standalone Allrun. -->

  <Definitions>
    <AttDef Type="create-allrun" Label="Create Allrun Files" BaseType="operation">
      <BriefDescription>
        Generate directories and scripts to run standalone
      </BriefDescription>
      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       LockType="DoNotLock" Extensible="false" OnlyResources="true">
        <Accepts>
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <Directory Name="Directory" ShouldExist="1" NumberOfRequiredValues="1">
          <BriefDescription>Output directory (existing contents will be overwritten)</BriefDescription>
        </Directory>
        <Void Name="Export" Optional="true" IsEnabledByDefault="true" AdvanceLevel="99">
          <BriefDescription>An internal switch: write to either external directory or project analysis directory</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Result(create-allrun)" BaseType="result">
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
