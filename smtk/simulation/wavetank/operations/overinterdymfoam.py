# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""overInterDyMFoam operation"""

import os
import shutil
import stat
import subprocess
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

FOAM_APP = 'overInterDyMFoam'  # save some typing
OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.dirname(__file__)
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin, Allrun_CONTENT


class OverInterDyMFoam(smtk.operation.Operation, FoamMixin):
    """Configures and optionally runs overInterDyMFoam application.

    Uses foam_operation.sbt for template.
    """

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return 'Setup and run {}'.format(FOAM_APP)

    def createSpecification(self):
        spec = self._create_specification(app=FOAM_APP)
        return spec

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        # print('att_resource:', att_resource)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        workflows_dir = self._get_workflows_folder()
        if workflows_dir is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

       # Get solver
        solver = self.cd_writer.get_application_name(att_resource)
        if solver != FOAM_APP:
            self.log().addError('Unrecognized solver: {}'.format(solver))
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Get project and case directories
        project_dir = os.path.abspath(os.path.dirname(project.location()))
        case_att = self.parameters().findDirectory('CaseDirectory')
        if case_att.isEnabled():
            case_dir = case_att.value()
        else:
            case_name = 'analysis'
            case_dir = os.path.join(project_dir, 'foam', case_name)
        version = att_resource.templateVersion()

        # Check if controlDict and physics attributes are valid
        att_names = ['controlDict', 'WaveModel']
        if version > 0:
            att_names.append('TransportProperties')
        if not self._check_attributes(att_resource, att_names):
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Run mergeMesh operation (which deletes current analysis directory)
        merge_op = self.manager().createOperation("mergemeshes.MergeMeshes")
        merge_op.parameters().associate(project)
        merge_op.parameters().findDirectory('CaseDirectory').setIsEnabled(True)
        merge_op.parameters().findDirectory('CaseDirectory').setValue(str(case_dir))
        merge_op.parameters().findString('run').setIsEnabled(False)

        merge_result = merge_op.operate()
        merge_outcome = merge_result.findInt('outcome').value()
        if merge_outcome != OP_SUCCEEDED:
            self.log().addError(
                f'SetFields operation did not succeed; returned {merge_outcome}')
            return False

        # (Re)generate controlDict file
        libs = ['overset', 'fvMotionSolvers']
        if not self.cd_writer.generate_controlDict(case_dir, att_resource, libs=libs):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Run setfields operation
        setfields_op = self.manager().createOperation("setfields.SetFields")
        setfields_op.parameters().associate(project)
        setfields_op.parameters().findDirectory('CaseDirectory').setIsEnabled(True)
        setfields_op.parameters().findDirectory('CaseDirectory').setValue(str(case_dir))
        setfields_op.parameters().findString('run').setIsEnabled(False)

        setfields_result = setfields_op.operate()
        setfields_outcome = setfields_result.findInt('outcome').value()
        if setfields_outcome != OP_SUCCEEDED:
            self.log().addError(
                f'SetFields operation did not succeed; returned {setfields_outcome}')
            return False

        # Generate transportPropertiesConfig file
        self._generate_transportPropertiesConfig(case_dir, att_resource)

        # Generate wavePropertiesConfig file
        self._generate_wavePropertiesConfig(case_dir, att_resource)

        # Generate decomposeParDict (if enabled)
        is_parallel = self._generate_decomposeParDict(case_dir, att_resource)

        # Write Allclean file
        self._write_allclean_file(case_dir)

        # Generate Allrun file
        self._generate_allrun(case_dir, is_parallel)

        # Copy merge_solve.sh script to parent directory
        script_filename = 'merge_solve.sh'
        script_location = os.path.join(workflows_dir, 'internal/package-scripts', script_filename)
        run_dir = os.path.join(case_dir, os.pardir)
        shutil.copy(script_location, run_dir)

        # Make sure script is executable
        copy_location = os.path.join(run_dir, script_filename)
        st = os.stat(copy_location)
        os.chmod(copy_location, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)

        # Check "Run" option
        run_item = self.parameters().findString('run')
        if run_item.isEnabled():
            self.log().addError('Sorry: Local run not implemented')
            return self.createResult(smtk.operation.Operation.Outcome.FAIL)

        result = self._create_result()
        return result

    def _generate_allrun(self, case_dir: str, is_parallel: bool):
        """"""
        run_cmd = 'runParallel' if is_parallel else 'runApplication'
        err_msg = f'Error ==> Check log files in the analysis case. The full path is {case_dir}'
        check_cmd = f'if [ $? -ne 0 ]; then echo "{err_msg}"; exit 1; fi'
        decompose_cmd = f'runApplication decomposePar\n{check_cmd}' if is_parallel else ''
        reconstruct_cmd = f'runApplication reconstructPar\n{check_cmd}' if is_parallel else ''

        script = Allrun_CONTENT + f"""
rm -rf 0
cp -r 0.orig 0

{decompose_cmd}

{run_cmd} topoSet
{check_cmd}

{run_cmd} setFields
{check_cmd}

{run_cmd} overInterDyMFoam
{check_cmd}

{reconstruct_cmd}

touch analysis.foam
echo "Allrun finished"
"""

        script_location = os.path.join(case_dir, 'Allrun')
        with open(script_location, 'w') as fp:
            fp.write(script)

        # Set execute permissions
        st = os.stat(script_location)
        os.chmod(script_location, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)

        # Check "Run" option
        run_item = self.parameters().findString('run')
        if run_item.isEnabled():
            self.log().addError('Sorry: Local run not implemented')
            return self.createResult(smtk.operation.Operation.Outcome.FAIL)

        result = self._create_result()
        return result
