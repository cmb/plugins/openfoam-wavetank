# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""SurfaceInertia operation"""

import os
import pathlib
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.abspath(os.path.dirname(__file__))
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin
from process_status import ProcessStatus


class SurfaceInertia(smtk.operation.Operation, FoamMixin):
    """Acquires input data for running surfaceInertia executable.

    Does not generate any case files.
    Inputs then copies density and shell/surface option to result attribute.
    """

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "Get inputs for surfaceInertia executable"

    def createSpecification(self):
        spec = self._create_specification(sbt_file='surfaceinertia.sbt')
        return spec

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Get object filename and relative location from case directory
        object_att = att_resource.findAttribute('Object')
        object_filename_item = object_att.findString('Filename')
        if not object_filename_item.isEnabled():
            self.log().addError('Model filename item not enabled in Object attribute')
            return None
        object_filename = object_filename_item.value()
        if not object_filename:
            self.log().addError('Empty model filename in Object attribute')
            return None
        object_location = f'constant/triSurface/{object_filename}'

        # Get user inputs
        density = self.parameters().findDouble('Density').valueAsString()
        is_shell = self.parameters().findString('Properties').value() == 'shell'

        # Build results with command line args for surfaceInertia
        result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        args_item = result.findString('CommandLineArguments')
        if is_shell:
            args_item.setNumberOfValues(4)
            args_item.setValue(2, '-shellProperties')
            args_item.setValue(3, object_location)
        else:
            args_item.setNumberOfValues(3)
            args_item.setValue(2, object_location)
        args_item.setValue(0, '-density')
        args_item.setValue(1, density)

        return result
