<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5">
  <!-- .sbt file for blockMesh operation -->
  <!-- Only load from operation which preloads smtk::operation base definitions) -->

  <Definitions>
    <AttDef Type="setup" BaseType="operation">
      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       LockType="DoNotLock" Extensible="false" OnlyResources="true">
        <Accepts>
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Component Name="Specification" NumberOfRequiredValues="1">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='AbstractBlockMesh']" />
          </Accepts>
        </Component>

        <Directory Name="CaseDirectory" Label="Custom Case Directory" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>For writing case directory outside the project directory.</BriefDescription>
        </Directory>

        <String Name="run" Label="Run OpenFOAM application" Optional="true" IsEnabledByDefault="true">
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Wait For Completion">sync</Value>
            <Value Enum="Launch And Return">async</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="result(setup)" BaseType="result">
      <ItemDefinitions>
        <Int Name="status"><DefaultValue>0</DefaultValue></Int>
        <Int Name="pid"><DefaultValue>0</DefaultValue></Int>
        <String Name="logfile"></String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
