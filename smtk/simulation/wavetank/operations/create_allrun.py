# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""CreateAttrun operation"""

import os
from pathlib import Path
import shutil
import stat
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.abspath(os.path.dirname(__file__))
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin, Allrun_CONTENT
from control_dict_writer import ControlDictWriter


class CreateAllrun(smtk.operation.Operation, FoamMixin):
    """"Create files for standalone Allrun scipts

    Uses predefined foam directory names:
        background: the background mesh
        overset: the moving object mesh
        analysis: the analysis (merged) mesh

    IMPORTANT: application must import wavetank operations to smtk Operation Manager
    """

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "CreateAllrun"

    def createSpecification(self):
        spec = self._create_specification(sbt_file='create_allrun.sbt')
        return spec

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        # print('att_resource:', att_resource)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # TODO confirm model resource exists with stl/obj file

        # Check attributes
        att_names = ['controlDict']
        if not self._check_attributes(att_resource, att_names):
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        workflows_folder = self._get_workflows_folder()
        if workflows_folder is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        solver = self.cd_writer.get_application_name(att_resource)
        if solver != 'overInterDyMFoam':
            message = 'This operation was not designed for {}'.format(solver)
            self.log().addWarning(message)

        # Check that the Object attribute is assigned a geometry file ("Filename" item)
        object_att = att_resource.findAttribute('Object')
        object_filename_item = object_att.findString('Filename')
        if not object_filename_item.isEnabled():
            self.log().addError('Cannot export: no model file stored with the project')
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Create output directory if needed
        output_dir = self.parameters().findDirectory("Directory").value()
        output_path = Path(output_dir)
        if not output_path.exists():
            output_path.mkdir(parents=True)

        # Check Export parameter
        export_item = self.parameters().findVoid('Export')
        is_export = export_item is not None and export_item.isEnabled()

        if is_export:
            # Write background, overset, & analysis directories
            if not self._write_background_files(project, output_path):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)
            if not self._write_overset_files(project, output_path):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)
            if not self._write_mergemesh_files(project, output_path):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Write analysis files
        if not self._write_analysis_files(project, output_path):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Copy package Allclean and Allrun scripts
        workflows_path = Path(workflows_folder)
        scripts_path = workflows_path / 'internal/package-scripts'
        for path in scripts_path.glob('All*'):
            shutil.copy(path, output_dir)

            # Make sure script is executable
            copy_path = output_path / path.name
            location = str(copy_path)
            st = os.stat(location)
            os.chmod(location, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)

        # Remove merge_solve.sh script generated by OverInterDyMFoam operation
        ms_path = output_path / 'merge_solve.sh'
        ms_path.unlink(missing_ok=True)

        result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        return result

    def _write_background_files(self, project: smtk.project.Project, output_path: Path) -> bool:
        """"""
        att_resource = self._get_attribute_resource(project)
        case_path = output_path / 'background'

        # Run blockmesh operation
        blockmesh_op = self.manager().createOperation("blockmesh.BlockMesh")
        blockmesh_op.parameters().associate(project)
        spec_att = att_resource.findAttribute('BackgroundBlockMesh')
        blockmesh_op.parameters().findComponent('Specification').setValue(spec_att)
        blockmesh_op.parameters().findDirectory('CaseDirectory').setIsEnabled(True)
        blockmesh_op.parameters().findDirectory('CaseDirectory').setValue(str(case_path))
        blockmesh_op.parameters().findString('run').setIsEnabled(False)

        blockmesh_result = blockmesh_op.operate()
        blockmesh_outcome = blockmesh_result.findInt('outcome').value()
        if blockmesh_outcome != OP_SUCCEEDED:
            self.log().addError(
                f'BlockMesh operation did not succeed; returned {blockmesh_outcome}')
            return False

        # Run refinemesh operation if there are refinement attributes
        refine_atts = att_resource.findAttributes('RefinementRegion')
        if refine_atts:
            refinemesh_op = self.manager().createOperation("refinemesh.RefineMesh")
            refinemesh_op.parameters().associate(project)
            refinemesh_op.parameters().findDirectory('CaseDirectory').setIsEnabled(True)
            refinemesh_op.parameters().findDirectory('CaseDirectory').setValue(str(case_path))
            refinemesh_op.parameters().findString('run').setIsEnabled(False)

            refinemesh_result = refinemesh_op.operate()
            refinemesh_outcome = refinemesh_result.findInt('outcome').value()
            if refinemesh_outcome != OP_SUCCEEDED:
                self.log().addError(
                    f'BlockMesh operation did not succeed; returned {refinemesh_outcome}')
                return False

        # Write Allclean file
        self._write_allclean_file(str(case_path))

        # Write Allrun file
        allrun_path = case_path / 'Allrun.pre'
        with open(allrun_path, 'w') as fp:
            fp.write(Allrun_CONTENT)
            fp.write('set -e \n')

            fp.write('\nrunApplication blockMesh\n')

            # Get region attributes
            att_list = att_resource.findAttributes('RefinementRegion')
            if att_list:
                num_atts = len(att_list)
                fp.write('\n')
                fp.write(f'for i in $(seq 1 {num_atts});')
                loop = \
                    """
do
  runApplication -s "$i" topoSet -dict system/topoSetDict.${i}
  runApplication -s "$i" refineMesh -overwrite
done
"""
                fp.write(loop)

            fp.write('\ntouch background.foam\n')

        # Set execute permissions
        allrun_location = str(allrun_path)
        st = os.stat(allrun_location)
        os.chmod(allrun_location, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)

        return True

    def _write_overset_files(self, project: smtk.project.Project, output_path: Path) -> bool:
        """"""
        att_resource = self._get_attribute_resource(project)
        case_path = output_path / 'overset'

        # Run blockmesh operation
        blockmesh_op = self.manager().createOperation("blockmesh.BlockMesh")
        blockmesh_op.parameters().associate(project)
        spec_att = att_resource.findAttribute('OversetBlockMesh')
        blockmesh_op.parameters().findComponent('Specification').setValue(spec_att)
        blockmesh_op.parameters().findDirectory('CaseDirectory').setIsEnabled(True)
        blockmesh_op.parameters().findDirectory('CaseDirectory').setValue(str(case_path))
        blockmesh_op.parameters().findString('run').setIsEnabled(False)

        blockmesh_result = blockmesh_op.operate()
        blockmesh_outcome = blockmesh_result.findInt('outcome').value()
        if blockmesh_outcome != OP_SUCCEEDED:
            self.log().addError(
                f'BlockMesh operation did not succeed; returned {blockmesh_outcome}')
            return False

        # Run surfaceFeatureExtract operation
        feature_op = self.manager().createOperation("surfacefeatureextract.SurfaceFeatureExtract")
        feature_op.parameters().associate(project)
        feature_op.parameters().findDirectory('CaseDirectory').setIsEnabled(True)
        feature_op.parameters().findDirectory('CaseDirectory').setValue(str(case_path))
        feature_op.parameters().findString('run').setIsEnabled(False)

        feature_result = feature_op.operate()
        feature_outcome = feature_result.findInt('outcome').value()
        if feature_outcome != OP_SUCCEEDED:
            self.log().addError(
                f'SurfaceFeatureExtract operation did not succeed; returned {feature_outcome}')
            return False

        # Run snappyHexMesh operation
        snappy_op = self.manager().createOperation("snappyhexmesh.SnappyHexMesh")
        snappy_op.parameters().associate(project)
        snappy_op.parameters().findDirectory('CaseDirectory').setIsEnabled(True)
        snappy_op.parameters().findDirectory('CaseDirectory').setValue(str(case_path))
        snappy_op.parameters().findString('run').setIsEnabled(False)

        snappy_result = snappy_op.operate()
        snappy_outcome = snappy_result.findInt('outcome').value()
        if snappy_outcome != OP_SUCCEEDED:
            self.log().addError(
                f'SnappyHexMesh operation did not succeed; returned {snappy_outcome}')
            return False

        # Copy geometry file
        project_dir = os.path.abspath(os.path.dirname(project.location()))
        geometry_dir = os.path.join(project_dir, 'foam/overset/constant/triSurface')
        dest_dir = os.path.join(case_path, 'constant/triSurface')
        shutil.copytree(geometry_dir, dest_dir)

        # Write Allclean file
        self._write_allclean_file(str(case_path))

        # Write Allrun file
        allrun_path = case_path / 'Allrun.pre'
        with open(allrun_path, 'w') as fp:
            fp.write(Allrun_CONTENT)
            fp.write('set -e \n')

            fp.write('\nrunApplication blockMesh\n')
            fp.write('\nrunApplication surfaceFeatureExtract\n')
            fp.write('\nrunApplication snappyHexMesh -overwrite\n')
            fp.write('\ntouch overset.foam\n')

        # Set execute permissions
        allrun_location = str(allrun_path)
        st = os.stat(allrun_location)
        os.chmod(allrun_location, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)

        return True

    def _write_mergemesh_files(self, project: smtk.project.Project, output_path: Path) -> bool:
        """"""
        case_path = output_path / 'analysis'

        # Run mergemesh operation
        mergemesh_op = self.manager().createOperation("mergemeshes.MergeMeshes")
        mergemesh_op.parameters().associate(project)
        mergemesh_op.parameters().findDirectory('CaseDirectory').setIsEnabled(True)
        mergemesh_op.parameters().findDirectory('CaseDirectory').setValue(str(case_path))
        mergemesh_op.parameters().findString('run').setIsEnabled(False)

        mergemesh_result = mergemesh_op.operate()
        mergemesh_outcome = mergemesh_result.findInt('outcome').value()
        if mergemesh_outcome != OP_SUCCEEDED:
            self.log().addError(
                f'MergeMeshes operation did not succeed; returned {mergemesh_outcome}')
            return False

        return True

    def _write_analysis_files(self, project: smtk.project.Project, output_path: Path) -> bool:
        """"""
        case_path = output_path / 'analysis'

        # Run overinterdymfoam operation
        solver_op = self.manager().createOperation("overinterdymfoam.OverInterDyMFoam")
        solver_op.parameters().associate(project)
        solver_op.parameters().findDirectory('CaseDirectory').setIsEnabled(True)
        solver_op.parameters().findDirectory('CaseDirectory').setValue(str(case_path))
        solver_op.parameters().findString('run').setIsEnabled(False)

        solver_result = solver_op.operate()
        solver_outcome = solver_result.findInt('outcome').value()
        if solver_outcome != OP_SUCCEEDED:
            self.log().addError(
                f'MergeMeshes operation did not succeed; returned {solver_outcome}')
            return False

        return True
