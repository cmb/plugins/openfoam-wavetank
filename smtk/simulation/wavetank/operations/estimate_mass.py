# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""Calculate object mass from displaced water (fluid)."""

import os
import shutil
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

# vtk modules
from vtkmodules.vtkCommonDataModel import (
    vtkPlane,
    vtkPolyData,
)
from vtkmodules.vtkFiltersCore import (
    vtkClipPolyData,
    vtkMassProperties,
)

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.abspath(os.path.dirname(__file__))
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin
from control_dict_writer import ControlDictWriter


SPECIFICATION = """
<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5">
  <!-- Only loads from operation which preloads smtk::operation base definitions) -->
  <Definitions>
    <AttDef Type="estimate_mass" BaseType="operation">
      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       LockType="DoNotLock" Extensible="false" OnlyResources="true">
        <Accepts>
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>
    </AttDef>

    <AttDef Type="result(setup)" BaseType="result">
      <ItemDefinitions>
        <Double Name="mass"><DefaultValue>0.0</DefaultValue></Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
"""


class EstimateMass(smtk.operation.Operation, FoamMixin):
    """Calculates the object mass from. Writes result to operation log."""

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "Estimate Object Mass"

    def createSpecification(self):
        spec = self.createBaseSpecification()

        reader = smtk.io.AttributeReader()
        hasErr = reader.readContents(spec, SPECIFICATION, self.log())
        if hasErr:
            message = 'Error loading specification'
            self.log().addError(message)
            raise RuntimeError(message)
        return spec

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Get the geometry filename
        object_att = att_resource.findAttribute('Object')
        # print('object_att is null?', (object_att is null))
        object_filename_item = object_att.findString('Filename')
        object_filename = object_filename_item.value()
        if not object_filename:
            self.log().addError('Object filename missing')
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)
        _base, ext = os.path.splitext(object_filename)

        # Get path to geometry file in the assets folder
        project_dir = os.path.abspath(os.path.dirname(project.location()))
        object_path = os.path.join(project_dir, 'assets', object_filename)

        # Load geometry as vtkPolydata
        object_polydata = self._read_geometry(object_path)
        if object_polydata is None:
            self.log().addError('Problem reading geometry file {}'.format(object_path))
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        vol1 = self._compute_volume(object_polydata)

        # Need to know which solver is being used in order to get the block mesh dims
        solver = self.cd_writer.get_application_name(att_resource)
        if solver == 'interFoam':
            att_name = 'BlockMesh'
        elif solver == 'overInterDyMFoam':
            att_name = 'OversetBlockMesh'
        else:
            self.log().addError('Unrecognized solver name {}'.format(solver))
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Get the bounds of the block mesh and copy to list
        mesh_att = att_resource.findAttribute(att_name)
        box_item = mesh_att.findDouble('box')
        if box_item.numberOfValues() != 6:
            msg = 'Expected 6 values for OversetBlockMesh not {}'.format(box_item.numberOfValues())
            self.log().addError(msg)
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        bounds = [0.0] * 6
        for i in range(6):
            bounds[i] = box_item.value(i)

        # For ZMax use water line
        wave_att = att_resource.findAttribute('WaveModel')
        surface_item = wave_att.findDouble('WaterSurface')
        bounds[5] = surface_item.value()
        bounds_strlist = [str(x) for x in bounds]
        bounds_string = ', '.join(bounds_strlist)

        # Clip the object to those bounds
        clip_polydata = self._clip_geometry(object_polydata, bounds)
        if clip_polydata is None:
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Compute volume
        volume = self._compute_volume(clip_polydata)

        # Get density and write value to log
        if att_resource.templateVersion() == 0:
            density = 998.2
        else:
            att = att_resource.findAttribute('TransportProperties')
            density_item = att.itemAtPath('Water/Density')
            density = density_item.value()

        mass = volume * density
        text = 'Estimated mass: {}'.format(mass)
        self.log().addRecord(smtk.io.Logger.Info, text)

        result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        result.findDouble('mass').setValue(mass)
        return result

    def _read_geometry(self, object_path: str):
        """Loads source geometry file into vtkPolyData.

        Returns None if error detected.
        """
        _base, ext = os.path.splitext(object_path)
        # Instantiate reader based on file extension
        if ext == '.obj':
            from vtkmodules.vtkIOGeometry import vtkOBJReader
            reader = vtkOBJReader()
        elif ext in ['.stl', '.stlb']:
            from vtkmodules.vtkIOGeometry import vtkSTLReader
            reader = vtkSTLReader()
        else:
            self.log().addError('Unsupported file extension {}'.format(ext))
            return None

        # Read geometry file and return polydata
        reader.SetFileName(object_path)
        reader.Update()
        polydata = reader.GetOutput()
        return polydata

    def _clip_geometry(self, polydata, bounds: list[float]):
        """Clips polydata to the input bounds.

        bounds format is [xmin, xmax, ymin, ymax, zmin, zmax]
        Returns None if error detected.
        """
        # Clip ymin
        ymin = bounds[2]
        ymin_plane = vtkPlane()
        ymin_plane.SetNormal(0, -1, 0)
        ymin_plane.SetOrigin(0, ymin, 0)
        ymin_clip = vtkClipPolyData()
        ymin_clip.SetInputData(polydata)
        ymin_clip.SetClipFunction(ymin_plane)
        ymin_clip.InsideOutOn()  # important for min values!

        # Clip ymax
        ymax = bounds[3]
        ymax_plane = vtkPlane()
        ymax_plane.SetNormal(0, 1, 0)
        ymax_plane.SetOrigin(0, ymax, 0)
        ymax_clip = vtkClipPolyData()
        ymax_clip.SetInputConnection(ymin_clip.GetOutputPort())
        ymax_clip.SetClipFunction(ymax_plane)
        ymax_clip.InsideOutOn()

        # Clip zmax
        zmax = bounds[5]
        zmax_plane = vtkPlane()
        zmax_plane.SetNormal(0, 0, 1)
        zmax_plane.SetOrigin(0, 0, zmax)
        zmax_clip = vtkClipPolyData()
        zmax_clip.SetInputConnection(ymax_clip.GetOutputPort())
        zmax_clip.SetClipFunction(zmax_plane)
        zmax_clip.InsideOutOn()

        zmax_clip.Update()
        result = zmax_clip.GetOutput()
        return result

    def _compute_volume(self, polydata):
        mass_prop = vtkMassProperties()
        mass_prop.SetInputData(polydata)
        volume = mass_prop.GetVolume()
        return volume
