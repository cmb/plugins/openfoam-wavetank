# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""SetFields operation"""

import os
import shutil
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.dirname(__file__)
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin, DictTemplate
from control_dict_writer import ControlDictWriter
from process_status import ProcessStatus


class SetFields(smtk.operation.Operation, FoamMixin):
    """Configures and optionally runs setFields application.

    Uses foam_operation.sbt for template.
    """

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "Setup and run setFields"

    def createSpecification(self):
        spec = self._create_specification(app='setFields')
        return spec

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        # print('att_resource:', att_resource)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        workflows_folder = self._get_workflows_folder()
        if workflows_folder is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Get case directory (based on OpenFOAM application)
        solver = self.cd_writer.get_application_name(att_resource)
        if solver == 'interFoam':
            case_name = solver
            libs = None
        elif solver == 'overInterDyMFoam':
            case_name = 'analysis'
            libs = ['overset', 'fvMotionSolvers']
        else:
            self.log().addError('Unrecognized solver: {}'.format(solver))
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        project_dir = os.path.abspath(os.path.dirname(project.location()))
        case_att = self.parameters().findDirectory('CaseDirectory')
        if case_att.isEnabled():
            case_dir = case_att.value()
            case_name = os.path.basename(case_dir)
        else:
            case_name = 'analysis'
            case_dir = os.path.join(project_dir, 'foam', case_name)

        # Regenerate controlDict file
        if not self.cd_writer.generate_controlDict(case_dir, att_resource, libs=libs):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Copy constant folder
        solver_path = 'internal/foam/{}'.format(solver)
        dirname = 'constant'
        from_dir = os.path.join(workflows_folder, solver_path, dirname)
        to_dir = os.path.join(case_dir, dirname)
        shutil.copytree(from_dir, to_dir, dirs_exist_ok=True)

        # Generate 0.orig folder
        is_2d = self._is_2d_analysis(att_resource)
        dirname = '0.orig'
        from_dir = os.path.join(workflows_folder, solver_path, dirname)
        to_dir = os.path.join(case_dir, dirname)
        if not self._generate_orig_folder(from_dir, to_dir, solver, is_2d):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Generate setFieldsConfig file
        if not self._generate_setFieldsConfig(case_dir, att_resource):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Generate topoSetConfig for overInterFyMFoam
        if solver == 'overInterDyMFoam':
            #  Use point from overset's snappy inside point
            cast_att = att_resource.findAttribute('Castellation')
            point_item = cast_att.findDouble('InsidePoint')
            coords = [None] * 3
            for i in range(3):
                coords[i] = point_item.value(i)
            t = ('pointInOverset', coords)
            kv_list = [t]

            constant_dir = os.path.join(case_dir, 'constant')
            include_dir = os.path.join(constant_dir, 'include')
            if not os.path.exists(include_dir):
                os.makedirs(include_dir)
            config_path = os.path.join(include_dir, 'topoSetConfig')
            with open(config_path, 'w') as fp:
                self.cd_writer.write_kvlist(fp, kv_list)

            if not self._generate_dynamicMeshConfig(case_dir, att_resource):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Check if run is enabled
        run_item = self.parameters().findString('run')
        if not run_item.isEnabled():
            result = self._create_result()
            return result

        # Copy 0.orig to 0
        from_dir = os.path.join(case_dir, '0.orig')
        to_dir = os.path.join(case_dir, '0')
        if os.path.exists(to_dir):
            shutil.rmtree(to_dir)
        shutil.copytree(from_dir, to_dir)

        if solver == 'interFoam':
            if not self._run_openfoam('setFields', case_dir, run_item):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)
            result = self._create_result()
            return result

        # Remaining logic for overInterDyMFoam (only)

        # The operation runs (i) topoSet then (ii) setFields
        # Can ONLY be run synchronous mode
        run_mode = run_item.value()
        if run_mode == 'async':  # launch and return
            self.log().addError('Error - async run_mode not supported')
            return False

        elif run_mode != 'sync':
            self.log().addError('internal error - unrecognized run_mode {}'.format(run_mode))
            return False

        # Run topoSet to create the cell zones
        if not self._run_openfoam('topoSet', case_dir, run_item):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Run setFields
        if not self._run_openfoam('setFields', case_dir, run_item):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        result = self._create_result()
        return result

    def _generate_dynamicMeshConfig(self, case_dir: str, att_resource: smtk.attribute.Resource) -> bool:
        """Writes constant/include/dynamicMeshConfig file. Returns True on success."""
        # Build (key,value) list
        kv_list = list()
        complete = False

        # Make sure directory exists
        include_dir = os.path.join(case_dir, 'constant/include')
        if not os.path.exists(include_dir):
            os.makedirs(include_dir)

        # Get the mass properties att
        props_att = att_resource.findAttribute('MassProperties')

        name = 'Mass'
        mass_item = props_att.findDouble(name)
        mass = mass_item.value()
        kv_list.append((name, mass))

        name = 'CenterOfMass'
        com_item = props_att.findDouble(name)
        com_value = [com_item.value(i) for i in range(com_item.numberOfValues())]
        kv_list.append((name, com_value))

        name = 'MomentOfInertia'
        moi_item = props_att.findDouble(name)
        moi_value = [moi_item.value(i) for i in range(moi_item.numberOfValues())]
        kv_list.append((name, moi_value))

        path = os.path.join(include_dir, 'dynamicMeshConfig')
        with open(path, 'w') as fp:
            self.cd_writer.write_kvlist(fp, kv_list, double_space=False)
            complete = True

        return complete

    def _generate_orig_folder(self, from_dir: str, to_dir: str, solver: str, is_2d: bool) -> bool:
        """Writes 0.orig files."""
        if not os.path.exists(to_dir):
            os.makedirs(to_dir)

        # Configure front & back patches depending on dimension
        front_back_type = 'empty' if is_2d else 'symmetryPlane'
        filenames = ['alpha.water', 'p_rgh', 'U']

        if solver == 'overInterDyMFoam':
            filenames += ['pointDisplacement', 'zoneID']

        for filename in filenames:
            # Read source file (template)
            from_text = None
            from_path = os.path.join(from_dir, filename)
            with open(from_path) as fin:
                from_text = fin.read()
            if from_text is None:
                self.log().addError('Failed to read {}'.format(from_path))
                return False

            # Generate output text
            dict_template = DictTemplate(from_text)
            to_text = dict_template.safe_substitute(FrontBackType=front_back_type)

            # Write output file
            to_path = os.path.join(to_dir, filename)
            complete = False
            with open(to_path, 'w') as fout:
                fout.write(to_text)
                complete = True
            if not complete:
                self.log().addError('Failed to write {}'.format(to_path))
                return False

        return True

    def _generate_setFieldsConfig(self, case_dir: str, att_resource: smtk.attribute.Resource) -> bool:
        """Writes constant/include/setFieldsConfig file. Returns True on success."""
        # Build (key,value) list
        kv_list = list()
        complete = False

        # Make sure directory exists
        include_dir = os.path.join(case_dir, 'constant/include')
        if not os.path.exists(include_dir):
            os.makedirs(include_dir)

        # Get the water surface value
        water_att = att_resource.findAttribute('WaveModel')
        surface_item = water_att.findDouble('WaterSurface')
        t = ('waterSurface', surface_item.value())
        kv_list.append(t)

        path = os.path.join(include_dir, 'setFieldsConfig')
        with open(path, 'w') as fp:
            self.cd_writer.write_kvlist(fp, kv_list, double_space=False)
            complete = True

        return complete
