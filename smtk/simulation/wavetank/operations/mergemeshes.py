# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""MergeMeshes operation"""

import os
import shutil
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.abspath(os.path.dirname(__file__))
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin
from control_dict_writer import ControlDictWriter
from process_status import ProcessStatus


class MergeMeshes(smtk.operation.Operation, FoamMixin):
    """Configures and optionally runs mergeMeshes and topoSet.

    Uses predefined foam directories:
        background: the background mesh
        overset: the moving object mesh
        analysis: the resulting (merged) mesh
    Uses foam_operation.sbt for template.
    """

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "Setup and run mergeMeshes and topoSet"

    def createSpecification(self):
        spec = self._create_specification(app='mergeMeshes')
        return spec

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        # print('att_resource:', att_resource)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Check attributes
        att_names = ['controlDict']
        if not self._check_attributes(att_resource, att_names):
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        workflows_folder = self._get_workflows_folder()
        if workflows_folder is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        solver = self.cd_writer.get_application_name(att_resource)
        if solver != 'overInterDyMFoam':
            message = 'This operation was not designed for {}'.format(solver)
            self.log().addWarning(message)

        project_dir = os.path.abspath(os.path.dirname(project.location()))
        case_att = self.parameters().findDirectory('CaseDirectory')
        if case_att.isEnabled():
            case_dir = case_att.value()
            case_name = os.path.basename(case_dir)
        else:
            case_name = 'analysis'
            case_dir = os.path.join(project_dir, 'foam', case_name)
        foam_dir = os.path.abspath(os.path.join(case_dir, os.pardir))

        # Delete current analysis folder
        if os.path.exists(case_dir):
            shutil.rmtree(case_dir)

        # Create analysis folder
        os.makedirs(case_dir)

        # Copy constant folder from workflows
        to_solver_dir = 'internal/foam/{}'.format(solver)
        solver_dir = os.path.join(workflows_folder, to_solver_dir)
        from_dir = os.path.join(solver_dir, 'constant')
        to_dir = os.path.join(case_dir, 'constant')
        shutil.copytree(from_dir, to_dir, dirs_exist_ok=True)

        # Copy system folder from background case
        for name in ['system', '0']:
            from_dir = os.path.join(foam_dir, 'background', name)
            if os.path.exists(from_dir):
                to_dir = os.path.join(foam_dir, 'analysis', name)
                shutil.copytree(from_dir, to_dir)

        # Copy background mesh to analysis case
        from_dir = os.path.join(foam_dir, 'background/constant/polyMesh', )
        to_dir = os.path.join(foam_dir, 'analysis/constant/polyMesh')
        if os.path.exists(from_dir):
            if os.path.exists(to_dir):
                shutil.rmtree(to_dir)
            shutil.copytree(from_dir, to_dir)

        # Generate controlDict file
        if not self.cd_writer.generate_controlDict(case_dir, att_resource):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Check "Run" option
        run_item = self.parameters().findString('run')
        if run_item.isEnabled():
            if not os.path.exists(to_dir):
                self.log().addError(f'Cannot merge - no mesh at {to_dir}')
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)

            # Run mergeMeshes
            args = ['./analysis', './overset', '-overwrite']
            if not self._run_openfoam(
                    'mergeMeshes',
                    case_dir,
                    run_item,
                    args_list=args,
                    foamfile='merged.foam',
                    run_dir=foam_dir):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)

            # Manually add foamfile to case dir
            foamfile_path = os.path.join(case_dir, 'mergedMesh.foam')
            with open(foamfile_path, 'a'):
                os.utime(foamfile_path, None)

        result = self._create_result()
        return result
