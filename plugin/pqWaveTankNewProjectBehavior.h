//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWaveTankNewProjectBehavior_h
#define plugin_pqWaveTankNewProjectBehavior_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/operation/Operation.h"

#include "pqReaction.h"

#include <QObject>

class pqWaveTankNewProjectBehaviorInternals;

/// \brief A reaction for creating a new ACE3P project.
class pqWaveTankNewProjectReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  pqWaveTankNewProjectReaction(QAction* parent);
  ~pqWaveTankNewProjectReaction() = default;

protected:
  /// Called when the action is triggered.
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqWaveTankNewProjectReaction)
};

/// \brief Creates new ACE3P project.
class pqWaveTankNewProjectBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqWaveTankNewProjectBehavior* instance(QObject* parent = nullptr);
  ~pqWaveTankNewProjectBehavior() override;

  void newProject();

Q_SIGNALS:
  void projectCreated(smtk::project::ProjectPtr);

protected Q_SLOTS:
  void onOperationExecuted(const smtk::operation::Operation::Result& result);

protected:
  pqWaveTankNewProjectBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqWaveTankNewProjectBehavior);
};

#endif
