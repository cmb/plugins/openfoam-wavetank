//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWaveTankRecentProjectsMenu_h
#define plugin_pqWaveTankRecentProjectsMenu_h

#include <QObject>

#include <QPointer>

class QAction;
class QMenu;

/** \brief Adds a "Recently Used" submenu to the main Projects menu
  */
class pqWaveTankRecentProjectsMenu : public QObject
{
  Q_OBJECT

public:
  /**
  * Assigns the menu that will display the list of files
  */
  pqWaveTankRecentProjectsMenu(QMenu* menu, QObject* parent = nullptr);
  ~pqWaveTankRecentProjectsMenu() override;

private Q_SLOTS:
  void buildMenu();
  void onOpenProject(QAction*);

private:
  Q_DISABLE_COPY(pqWaveTankRecentProjectsMenu);

  QPointer<QMenu> m_menu;
};

#endif // plugin_pqWaveTankRecentProjectsMenu_h
