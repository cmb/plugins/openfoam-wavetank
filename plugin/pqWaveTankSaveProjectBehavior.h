//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWaveTankSaveProjectBehavior_h
#define plugin_pqWaveTankSaveProjectBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing an ace3p project to disk.
class pqWaveTankSaveProjectReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqWaveTankSaveProjectReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqWaveTankSaveProjectReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqWaveTankSaveProjectBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqWaveTankSaveProjectBehavior* instance(QObject* parent = nullptr);
  ~pqWaveTankSaveProjectBehavior() override;

  bool saveProject();

protected:
  pqWaveTankSaveProjectBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqWaveTankSaveProjectBehavior);
};

#endif
