//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqWaveTankControlsView.h"
#include "ui_pqWaveTankControlsView.h"

#include "smtk/simulation/wavetank/qt/qtControlsViewConfig.h"
#include "smtk/simulation/wavetank/qt/qtOpenFoamRunner.h"
#include "smtk/simulation/wavetank/qt/qtSessionData.h"

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/qt/qtOperationDialog.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/resource/Manager.h"
#include "smtk/resource/Resource.h"
#include "smtk/view/Configuration.h"
#include "smtk/view/Manager.h"

// ParaView includes
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqDataRepresentation.h"
#include "pqObjectBuilder.h"
#include "pqPipelineSource.h"
#include "pqReloadFilesReaction.h"
#include "pqRenderView.h"
#include "pqSMAdaptor.h"
#include "pqServer.h"
#include "pqServerManagerModel.h"
#include "pqView.h"

#include "vtkSMParaViewPipelineControllerWithRendering.h"
#include "vtkSMProxy.h"
#include "vtkSMSourceProxy.h"

// VTK includes
#include "vtkNew.h"

#include <QComboBox>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFrame>
#include <QMessageBox>
#include <QProcess>
#include <QSharedPointer>
#include <QSizePolicy>
#include <QSpinBox>
#include <QString>
#include <QStringList>

#include <cctype>
#include <set>
#include <string>

using FoamMode = smtk::simulation::wavetank::OpenFoamMode;

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

// --------------------------------------------------------------------
QMap<QString, pqPipelineSource*> pqWaveTankControlsView::s_pipelineSourceMap;

void pqWaveTankControlsView::clearSourceMap()
{
  s_pipelineSourceMap.clear();
}

// --------------------------------------------------------------------
class pqWaveTankControlsView::Internal : public Ui::pqWaveTankControlsView
{
  //Q_OBJECT
public:
  QWidget* m_widget = nullptr;
  smtk::simulation::wavetank::qtControlsViewConfig* m_config = nullptr;
  bool m_dataModified = false; // indicates if foam data needs to be reloaded

  void initialize(const smtk::view::ConfigurationPtr config, QWidget* widget);
};

void pqWaveTankControlsView::Internal::initialize(
  const smtk::view::ConfigurationPtr config,
  QWidget* widget)
{
  this->setupUi(widget);
  m_widget = widget;

  // Set widgets to retain geometry when hidden
  QSizePolicy policy = m_display->sizePolicy();
  policy.setRetainSizeWhenHidden(true);
  m_display->setSizePolicy(policy);
  m_opacity->setSizePolicy(policy);
  m_showLog->setSizePolicy(policy);

  // Ingest the view configuration
  m_config = new smtk::simulation::wavetank::qtControlsViewConfig(config);

  // Set run button visibility
  bool opVisible = !m_config->operationType().empty();
  m_showLog->setVisible(opVisible);
  m_lastRunLabel->setVisible(opVisible);
  m_run->setVisible(opVisible);

  // Set show-log button visibility
  const QString& foamApp = m_config->foamApplication();
  const QString& shellString = m_config->shellScript();
  if (!foamApp.isEmpty())
  {
    QString label = QString("Run %1").arg(foamApp);
    m_run->setText(label);
  }
  else if (!shellString.isEmpty())
  {
    ; // todo
  }
  else
  {
    m_lastRunLabel->setText("");
    m_showLog->setVisible(false);
  }

  // Set display options
  const QString& defaultRepr = m_config->defaultRepresentation();
  int comboIndex = m_representation->findText(defaultRepr);
  if (comboIndex >= 0)
  {
    m_representation->setCurrentIndex(comboIndex);
  }

  // Set ribbon color based on openfoam mode
  QString color = "teal";
  auto* sessionData = smtk::simulation::wavetank::qtSessionData::instance();
  auto foamMode = sessionData->openFoamMode();
  switch (foamMode)
  {
    case FoamMode::LocalInstall:
      color = "#559070"; // openfoam green
      break;

    case FoamMode::ContainerEngine:
      color = "#1d63ed"; // docker blue
      break;

    case FoamMode::TestLocal:
      color = "#ef5350"; // red 400 (https://m2.material.io/design/color/the-color-system.html)
      break;

    case FoamMode::Undefined:
    case FoamMode::NotUsed:
    default:
      color = "#7e3ff2"; // purple 400 (https://m2.material.io/design/color/the-color-system.html)
      break;
  }

  QString ribbonStyle = QString("background-color: %1; border-radius: 2px;").arg(color);
  m_ribbon->setStyleSheet(ribbonStyle);

  // Hide display controls if no foam file or resource specified
  if (m_config->foamfileName().isEmpty() && m_config->resourceRole().isEmpty())
  {
    m_display->hide();
    m_representation->hide();
    m_opacity->hide();
    m_hideOthers->hide();
  }
}

// --------------------------------------------------------------------
pqWaveTankControlsView::qtBaseView* createViewWidget(const smtk::view::Information& info)
{
  pqWaveTankControlsView* view = new pqWaveTankControlsView(info);
  return view;
}

pqWaveTankControlsView::pqWaveTankControlsView(const smtk::view::Information& info)
  : qtBaseView(info)
{
  m_internal = new Internal;
  this->createWidget();

  QObject::connect(
    m_internal->m_run, &QPushButton::clicked, this, &pqWaveTankControlsView::onRunClicked);

  QObject::connect(m_internal->m_showLog, &QPushButton::clicked, [this]() {
    auto* runner = smtk::simulation::wavetank::qtOpenFoamRunner::instance();
    runner->showLogFile(m_internal->m_config->logfilePath());
  });

  // If connected to model resource, find pqPipelineSource
  const QString& role = m_internal->m_config->resourceRole();
  if (!role.isEmpty())
  {
    // There might be a race condition with the pipeline source being created
    auto* source = this->locateSMTKResource(role);
    //qDebug() << "pqWaveTankControlsView:" << __LINE__ << "source" << source;
  }

  auto* runner = smtk::simulation::wavetank::qtOpenFoamRunner::instance();
  QObject::connect(runner, &smtk::simulation::wavetank::qtOpenFoamRunner::started, [this]() {
    m_internal->m_run->setEnabled(false);
  });

  QObject::connect(
    runner,
    &smtk::simulation::wavetank::qtOpenFoamRunner::finished,
    this,
    &pqWaveTankControlsView::onProcessFinished);

  QObject::connect(
    m_internal->m_display, &QPushButton::clicked, this, &pqWaveTankControlsView::onDisplayClicked);

  QObject::connect(
    m_internal->m_representation,
    QOverload<int>::of(&QComboBox::currentIndexChanged),
    this,
    &pqWaveTankControlsView::onRepresentationChanged);

  QObject::connect(
    m_internal->m_opacity,
    QOverload<int>::of(&QSpinBox::valueChanged),
    this,
    &pqWaveTankControlsView::onOpacityChanged);

  QObject::connect(
    m_internal->m_hideOthers,
    &QPushButton::clicked,
    this,
    &pqWaveTankControlsView::onHideOthersClicked);
}

pqWaveTankControlsView::~pqWaveTankControlsView()
{
  delete m_internal;
}

bool pqWaveTankControlsView::isEmpty() const
{
  auto* sessionData = smtk::simulation::wavetank::qtSessionData::instance();

  // View is only usable when project is loaded
  auto project = sessionData->project();
  if (project == nullptr)
  {
    return true;
  }

  // If foam app is specified, check OpenFoamMode
  QString foamApp = m_internal->m_config->foamApplication();
  if (!foamApp.isEmpty())
  {
    auto foamMode = sessionData->openFoamMode();
    switch (foamMode)
    {
      case FoamMode::LocalInstall:
      case FoamMode::ContainerEngine:
      case FoamMode::TestLocal:
        break; // keep going

      case FoamMode::Undefined:
      case FoamMode::NotUsed:
      default:
        return true; // hide
        break;
    }
  }

  // Check categories
  const QString& cat = m_internal->m_config->category();
  if (!cat.isEmpty())
  {
    std::string _cat = cat.toStdString();
    auto attResourceSet = project->resources().findByRole<smtk::attribute::Resource>("attributes");
    auto attResource = *attResourceSet.begin();
    const std::set<std::string>& activeCategories = attResource->activeCategories();
    if (activeCategories.find(_cat) == activeCategories.end())
    {
      return true;
    }
  }

  // else good to go (i.e., NOT empty)
  return false;
}

void pqWaveTankControlsView::createWidget()
{
  this->Widget = new QFrame(this->parentWidget());
  m_internal->initialize(this->configuration(), this->Widget);
}

bool pqWaveTankControlsView::runAllclean()
{
  QString caseDir = m_internal->m_config->caseDirectory();
  QString scriptLocation = caseDir + "/Allclean";
  QFile scriptFile(scriptLocation);
  if (!scriptFile.exists())
  {
    qDebug() << "Allclean script *not* found";
    return true;
  }

  int timeoutMsec = 5000;
  QProcess cleanProcess;
  cleanProcess.setProgram(scriptLocation);
  cleanProcess.setWorkingDirectory(caseDir);
  qDebug() << "Running Allclean script";
  cleanProcess.start();
  if (!cleanProcess.waitForStarted(timeoutMsec))
  {
    return false;
  }

  return cleanProcess.waitForFinished(timeoutMsec);
}

void pqWaveTankControlsView::updateUI()
{
  // qDebug() << "Entered pqWaveTankControlsView::updateUI()";
  this->updateLogControls();
  this->updateGeometryControls();
}

void pqWaveTankControlsView::onRunClicked()
{
  auto* sessionData = smtk::simulation::wavetank::qtSessionData::instance();
  auto opManager = sessionData->operationManager();

  std::string opType = m_internal->m_config->operationType();
  auto op = opManager->create(opType);
  if (op == nullptr)
  {
    QString message = QString("Internal Error: failed to create operation %1.").arg(opType.c_str());
    QMessageBox::critical(this->Widget, "Error", message);
    return;
  }

  auto project = sessionData->project();
  if (project == nullptr)
  {
    QString message =
      "The operation cannot run because there is no project loaded into the system.";
    QMessageBox::critical(this->Widget, "No Project Loaded", message);
    return;
  }

  // Check for delete-case config
  if (m_internal->m_config->deleteCaseDirectory())
  {
    // Check if case directory exists
    QDir caseDir(m_internal->m_config->caseDirectory());
    if (caseDir.exists())
    {
      QString message;
      QTextStream qs(&message);
      qs << "This action will delete the current analysis folder " << caseDir.canonicalPath()
         << " including any solution data there."
         << "\n\nAre you sure you want to proceed?";

      auto buttons = QMessageBox::Ok | QMessageBox::Cancel;
      auto reply =
        QMessageBox::warning(this->Widget, "Warning", message, buttons, QMessageBox::Cancel);
      if (reply != QMessageBox::Ok)
      {
        qInfo() << "Aborting Run";
        return;
      }
    } // if caseDir exists
  }   // if deleteCaseDirectory

  op->parameters()->associate(project);

  // Disable operation's run option so that we can run from here
  auto runItem = op->parameters()->find("run");
  if (runItem != nullptr)
  {
    runItem->setIsEnabled(false);
  }

  // Check for option to use operation dialog
  if (m_internal->m_config->useOperationDialog())
  {
    this->runOperationDialog(op);
    return;
  }

  // Special logic for block mesh operation
  if (m_internal->m_config->operationType() == "blockmesh.BlockMesh")
  {
    // Need to assign "Specification" attribute for background or overset
    QString category = m_internal->m_config->category();
    if (category.isEmpty())
    {
      qWarning() << "BlockMesh Controls missing Category element";
    }
    else
    {
      // Construct attribute name
      std::string attName = category.toStdString() + "BlockMesh";
      attName[0] = toupper(attName[0]);

      // Get the attribute resource
      auto attResourceSet =
        project->resources().findByRole<smtk::attribute::Resource>("attributes");
      auto attResource = *attResourceSet.begin();

      // Get the attribute
      auto att = attResource->findAttribute(attName);
      if (att == nullptr)
      {
        qWarning() << "Failed to find" << QString::fromStdString(attName) << "attribute";
      }
      else
      {
        op->parameters()->findComponent("Specification")->setValue(att);
      }
    }
  }

  // Special logic for CreateAllrun operation
  if (m_internal->m_config->operationType() == "create_allrun.CreateAllrun")
  {
    // Set operation's Export item
    bool isExport = m_internal->m_config->isExport();
    auto exportItem = op->parameters()->find("Export");
    if (exportItem != nullptr)
    {
      exportItem->setIsEnabled(isExport);
    }

    // For non-export case, we gotta set the op's directory param
    if (!isExport)
    {
      auto dirParam = op->parameters()->findDirectory("Directory");
      if (dirParam)
      {
        QString caseDir = m_internal->m_config->caseDirectory();
        dirParam->setValue(caseDir.toStdString());
      }
    }
  }

  // Run direct (no dialog)
  auto result = op->operate();
  this->onOperationExecuted(result);
}

void pqWaveTankControlsView::runOperationDialog(smtk::operation::OperationPtr op)
{
  auto sessionData = smtk::simulation::wavetank::qtSessionData::instance();
  auto project = sessionData->project();
  auto resourceManager = std::dynamic_pointer_cast<smtk::resource::Resource>(project)->manager();
  auto viewManager = sessionData->viewManager();

  // Construct a modal dialog for the operation spec
  QSharedPointer<smtk::extension::qtOperationDialog> dialog =
    QSharedPointer<smtk::extension::qtOperationDialog>(
      new smtk::extension::qtOperationDialog(op, resourceManager, viewManager, this->widget()));
  dialog->setObjectName("Operation Dialog");
  dialog->setWindowTitle("Specify Operation Properties");
  QObject::connect(
    dialog.get(),
    &smtk::extension::qtOperationDialog::operationExecuted,
    this,
    &pqWaveTankControlsView::onOperationExecuted);
  dialog->exec();
}

void pqWaveTankControlsView::onOperationExecuted(smtk::attribute::AttributePtr result)
{
  int outcome = result->findInt("outcome")->value();
  if (outcome != OP_SUCCEEDED)
  {
    QString message = QString("Error generating input files; outcome %1").arg(outcome);
    QMessageBox::critical(this->Widget, "Error", message);
    return;
  }
  // m_internal->m_config->dump();

  // Remove pipeline source if geometry is a project resource
  const QString& key = m_internal->m_config->resourceRole();
  if (!key.isEmpty())
  {
    s_pipelineSourceMap.remove(key);
  }

  // Set up async run
  const QString& caseDirectory = m_internal->m_config->caseDirectory();
  auto* runner = smtk::simulation::wavetank::qtOpenFoamRunner::instance();

  // Run foam application or shell script
  const QString& viewName = m_internal->m_config->viewName();
  const QString& foamApp = m_internal->m_config->foamApplication();
  const QString& shellScript = m_internal->m_config->shellScript();
  bool runInParent = m_internal->m_config->runInParentDirectory();
  bool canAbort = m_internal->m_config->canAbort();
  if (!foamApp.isEmpty())
  {
    QStringList args = m_internal->m_config->foamArguments();
    // Check operation results for cli args
    auto argsItem = result->findString("CommandLineArguments");
    if (argsItem != nullptr)
    {
      for (std::size_t i = 0; i < argsItem->numberOfValues(); ++i)
      {
        args << QString::fromStdString(argsItem->value(i));
      }
    }
    runner->start(viewName, foamApp, caseDirectory, args, runInParent, canAbort);
  }
  else if (!shellScript.isEmpty())
  {
    runner->startShellScript(viewName, shellScript, caseDirectory, runInParent, canAbort);
  }
  else
  {
    this->updateGeometryControls();
    return;
  }

  // TODO Hide geometry
}

void pqWaveTankControlsView::onProcessFinished(qint64 pid, int exitcode, const QString& viewName)
{
  m_internal->m_run->setEnabled(true);

  // If process not invoked by me, no further action required
  if (viewName != m_internal->m_config->viewName())
  {
    return;
  }

  const QString& foamfilePath = m_internal->m_config->foamfilePath();
  if (exitcode == 0 && !foamfilePath.isEmpty())
  {
    // Create or touch foamfile
    QFile file(foamfilePath);
    file.open(QFile::WriteOnly);
    file.close();

    // If the foamfile is already in memory, set modified flag
    auto iter = s_pipelineSourceMap.find(foamfilePath);
    if (iter != s_pipelineSourceMap.end())
    {
      m_internal->m_dataModified = true;
    }
  }

  this->updateUI();
}

void pqWaveTankControlsView::onDisplayClicked()
{
  // Only applies if active view is a render view
  pqView* view = pqActiveObjects::instance().activeView();
  pqRenderView* renderView = dynamic_cast<pqRenderView*>(view);
  if (renderView == nullptr)
  {
    qCritical() << "No active view or is not a render view";
    return;
  }

  // Look for the pipeline source
  pqPipelineSource* source = this->getPipelineSource();
  if (source == nullptr)
  {
    // Is this a foam file that hasn't been loaded yet?
    const QString& foamfilePath = m_internal->m_config->foamfilePath();
    if (!foamfilePath.isEmpty())
    {
      // Get the source (load if needed)
      bool isNew = false;
      source = this->getPipelineSource(foamfilePath, isNew);
    }
  }

  if (source == nullptr)
  {
    qCritical() << "Internal error: failed to find or load pipeline source.";
    return;
  }

  pqDataRepresentation* representation = source->getRepresentation(view);
  if (representation == nullptr)
  {
    qWarning() << "No representation for pipeline source.";
    return;
  }

  QString displayText = m_internal->m_display->text();
  if (displayText.startsWith("Hide"))
  {
    representation->setVisible(false);
    view->render();
    m_internal->m_display->setText("Show Data");
    return;
  }

  // (else)
  if (m_internal->m_dataModified)
  {
    auto* proxy = vtkSMSourceProxy::SafeDownCast(source->getProxy());
    pqReloadFilesReaction::reload(proxy);
    m_internal->m_dataModified = false;
  }

  vtkSMProxy* reprProxy = representation->getProxy();

  // Apply representation and opacity settings
  QString repType = m_internal->m_representation->currentText();
  pqSMAdaptor::setEnumerationProperty(reprProxy->GetProperty("Representation"), repType);

  double opacity = static_cast<double>(m_internal->m_opacity->value()) / 100.0;
  pqSMAdaptor::setElementProperty(reprProxy->GetProperty("Opacity"), opacity);

  representation->setVisible(true);
  view->render();
  m_internal->m_display->setText("Hide Data");
}

void pqWaveTankControlsView::onRepresentationChanged()
{
  pqDataRepresentation* representation = this->getActiveRepresentation();
  if (representation == nullptr)
  {
    qWarning() << "Internal error: representation is null" << __FILE__ << __LINE__;
    return;
  }

  vtkSMProxy* reprProxy = representation->getProxy();
  QString repType = m_internal->m_representation->currentText();
  pqSMAdaptor::setEnumerationProperty(reprProxy->GetProperty("Representation"), repType);
  representation->getInput()->getSourceProxy()->UpdateVTKObjects();
  if (representation->isVisible())
  {
    // Toggle visibility otherwise display doesn't update (dont know why)
    representation->setVisible(false);
    representation->setVisible(true);
    representation->getView()->render();
  }
}

void pqWaveTankControlsView::onOpacityChanged(int i)
{
  pqDataRepresentation* representation = this->getActiveRepresentation();
  if (representation == nullptr)
  {
    qWarning() << "Internal error: representation is null" << __FILE__ << __LINE__;
    return;
  }

  vtkSMProxy* reprProxy = representation->getProxy();
  double opacity = static_cast<double>(i) / 100.0;
  pqSMAdaptor::setElementProperty(reprProxy->GetProperty("Opacity"), opacity);
  representation->getInput()->getSourceProxy()->UpdateVTKObjects();
  if (representation->isVisible())
  {
    // Toggle visibility otherwise display doesn't update (dont know why)
    representation->setVisible(false);
    representation->setVisible(true);
    representation->getView()->render();
  }
}

void pqWaveTankControlsView::onHideOthersClicked()
{
  // Only applies if active view is a render view
  pqView* view = pqActiveObjects::instance().activeView();
  pqRenderView* renderView = dynamic_cast<pqRenderView*>(view);
  if (renderView == nullptr)
  {
    qCritical() << "No active view or is not a render view";
    return;
  }

  // Get this source (could be nullptr)
  pqPipelineSource* thisSource = this->getPipelineSource();

  // Traverse sources and hide others
  auto iter = s_pipelineSourceMap.begin();
  for (; iter != s_pipelineSourceMap.end(); ++iter)
  {
    auto* source = iter.value();
    if (source == thisSource)
    {
      continue;
    }

    auto* rep = source->getRepresentation(view);
    if (rep != nullptr)
    {
      rep->setVisible(false);
    }
  } // end (for)

  view->render();
}

void pqWaveTankControlsView::updateLogControls()
{
  // m_internal->m_config->dump();
  const QString& foamApp = m_internal->m_config->foamApplication();
  if (foamApp.isEmpty())
  {
    return; // no foamApp to run so no logs
  }

  m_internal->m_lastRunLabel->setText("");
  m_internal->m_showLog->setVisible(false);

  const QString& logfilePath = m_internal->m_config->logfilePath();
  QFileInfo fileInfo(logfilePath);
  if (!fileInfo.exists())
  {
    return;
  }

  m_internal->m_showLog->setVisible(true);

  QDateTime dt = fileInfo.lastModified();
  QString dtString = dt.toString("ddd dd-MMM-yyyy hh:mm");
  QString label = QString("Last run: %1").arg(dtString);
  m_internal->m_lastRunLabel->setText(label);
}

void pqWaveTankControlsView::updateGeometryControls()
{
  // Check whether or not to show geometry controls
  bool showButtons = false;
  pqPipelineSource* source = nullptr;

  const QString& foamfilePath = m_internal->m_config->foamfilePath();
  const QString& resourceRole = m_internal->m_config->resourceRole();
  if (!foamfilePath.isEmpty())
  {
    if (QFileInfo::exists(foamfilePath))
    {
      showButtons = true;
      auto iter = s_pipelineSourceMap.find(foamfilePath);
      if (iter == s_pipelineSourceMap.end())
      {
        // Data not yet loaded
        m_internal->m_display->setText("Load Data");
      }
      else if (m_internal->m_dataModified)
      {
        // Data needs to be loaded
        m_internal->m_display->setText("Reload Data");
      }
      else
      {
        source = iter.value();
      }
    }
  }
  else if (!resourceRole.isEmpty())
  {
    // Check map first
    auto iter = s_pipelineSourceMap.find(resourceRole);
    if (iter == s_pipelineSourceMap.end())
    {
      source = this->locateSMTKResource(resourceRole);
    }
    else
    {
      source = iter.value();
    }
    showButtons = source != nullptr;
  }

  // Is source currently visible?
  pqView* view = pqActiveObjects::instance().activeView();
  pqRenderView* renderView = dynamic_cast<pqRenderView*>(view);
  if (renderView == nullptr)
  {
    showButtons = false;
  }
  else if (source != nullptr)
  {
    auto* representation = source->getRepresentation(view);
    QString text = representation->isVisible() ? "Hide Data" : "Show Data";
    m_internal->m_display->setText(text);
  }

  // Finally, set button visibility
  m_internal->m_display->setVisible(showButtons);
  m_internal->m_representation->setVisible(showButtons);
  m_internal->m_opacity->setVisible(showButtons);
  m_internal->m_hideOthers->setVisible(showButtons);
}

pqPipelineSource* pqWaveTankControlsView::locateSMTKResource(const QString& resourceRole)
{
  // Find the project resource
  auto* sessionData = smtk::simulation::wavetank::qtSessionData::instance();
  auto project = sessionData->project();
  if (project == nullptr)
  {
    return nullptr;
  }

  std::set<smtk::resource::ResourcePtr> resourceSet =
    project->resources().findByRole(resourceRole.toStdString());
  if (resourceSet.empty())
  {
    return nullptr;
  }
  auto iter = resourceSet.begin();
  smtk::resource::ResourcePtr projectResource = *iter;

  // Find pipeline source for this resource
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqServerManagerModel* smModel = pqApplicationCore::instance()->getServerManagerModel();
  QList<pqSMTKResource*> sourceList = smModel->findItems<pqSMTKResource*>(server);
  Q_FOREACH (pqSMTKResource* source, sourceList)
  {
    if (source->getResource() == projectResource)
    {
      // Add resource to the map
      s_pipelineSourceMap.insert(resourceRole, source);
      return source;
    }
  } // for (each pqSMTKResource)

  return nullptr; // not found
}

pqDataRepresentation* pqWaveTankControlsView::getActiveRepresentation() const
{
  // Only applies if active view is a render view
  pqView* view = pqActiveObjects::instance().activeView();
  pqRenderView* renderView = dynamic_cast<pqRenderView*>(view);
  if (renderView == nullptr)
  {
    return nullptr;
  }

  pqPipelineSource* source = this->getPipelineSource();
  if (source == nullptr)
  {
    return nullptr;
  }

  return source->getRepresentation(view);
}

pqPipelineSource* pqWaveTankControlsView::getPipelineSource() const
{
  QString key;
  const QString& foamfilePath = m_internal->m_config->foamfilePath();
  const QString& role = m_internal->m_config->resourceRole();
  if (!foamfilePath.isEmpty())
  {
    key = foamfilePath;
  }
  else if (!role.isEmpty())
  {
    key = role;
  }
  else
  {
    return nullptr;
  }

  auto iter = s_pipelineSourceMap.find(key);
  return iter == s_pipelineSourceMap.end() ? nullptr : iter.value();
}

pqPipelineSource* pqWaveTankControlsView::getPipelineSource(const QString& foamfile, bool& isNew)
{
  if (foamfile.isEmpty())
  {
    qWarning() << "Foam File is empty" << __FILE__ << __LINE__;
    return nullptr;
  }

  auto iter = s_pipelineSourceMap.find(foamfile);
  if (iter != s_pipelineSourceMap.end())
  {
    return iter.value();
  }

  // Create pqPipelineSource
  pqApplicationCore* core = pqApplicationCore::instance();
  pqObjectBuilder* builder = core->getObjectBuilder();

  pqServer* server = pqActiveObjects::instance().activeServer();
  if (server == nullptr)
  {
    qWarning() << __FILE__ << __LINE__ << "active server is null";
    return nullptr;
  }
  pqServerManagerModel* smModel = core->getServerManagerModel();

  QStringList files;
  files << foamfile;
  pqPipelineSource* source = builder->createReader("sources", "OpenFOAMReader", files, server);
  vtkSMSourceProxy* readerProxy = source->getSourceProxy();
  isNew = true;

  // Push changes to server so that when the representation gets updated,
  // it uses the property values we set.
  readerProxy->UpdateVTKObjects();

  // ensures that new timestep range, if any gets fetched from the server.
  readerProxy->UpdatePipelineInformation();

  // Create representation for the active view if it's a render view
  pqView* view = pqActiveObjects::instance().activeView();
  pqRenderView* renderView = dynamic_cast<pqRenderView*>(view);
  if (renderView == nullptr)
  {
    qCritical() << "No active view or is not a render view";
    return source;
  }

  // Make representations.
  vtkNew<vtkSMParaViewPipelineControllerWithRendering> controller;
  // controller->HideAll(renderView->getViewProxy());
  controller->Show(readerProxy, 0, renderView->getViewProxy());

  renderView->resetCamera();

  // We have already made the representations and pushed everything to the
  // server manager.  Thus, there is no state left to be modified.
  source->setModifiedState(pqProxy::UNMODIFIED);

  s_pipelineSourceMap.insert(foamfile, source);
  return source;
}
