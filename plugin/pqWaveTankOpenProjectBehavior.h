//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWaveTankOpenProjectBehavior_h
#define plugin_pqWaveTankOpenProjectBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for opening a windtunnel project
class pqWaveTankOpenProjectReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqWaveTankOpenProjectReaction(QAction* parent);

  void openProject();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->openProject(); }

private:
  Q_DISABLE_COPY(pqWaveTankOpenProjectReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqWaveTankOpenProjectBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqWaveTankOpenProjectBehavior* instance(QObject* parent = nullptr);
  ~pqWaveTankOpenProjectBehavior() override;

protected:
  pqWaveTankOpenProjectBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqWaveTankOpenProjectBehavior);
};

#endif
