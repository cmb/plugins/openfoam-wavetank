//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWaveTankPluginLocation_h
#define plugin_pqWaveTankPluginLocation_h

#include <QObject>

#include <QDir>

class pqWaveTankPluginLocation : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqWaveTankPluginLocation(QObject* parent = nullptr);
  ~pqWaveTankPluginLocation() override;

  void StoreLocation(const char* location);

protected:
  void locateWorkflows(const QDir& pluginResourcesDirectory) const;

private:
  Q_DISABLE_COPY(pqWaveTankPluginLocation);
};

#endif
