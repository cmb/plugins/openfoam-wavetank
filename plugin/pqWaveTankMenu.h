//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWaveTankMenu_h
#define plugin_pqWaveTankMenu_h

#include "smtk/PublicPointerDefs.h"

#include <QActionGroup>

class QAction;

class pqServer;

class pqWaveTankRecentProjectsMenu;
class pqWaveTankRemoteParaViewMenu;

/** \brief Adds the "WaveTank" menu to the application main window
  */
class pqWaveTankMenu : public QActionGroup
{
  Q_OBJECT
  using Superclass = QActionGroup;

public Q_SLOTS:
  void onProjectOpened(smtk::project::ProjectPtr project);
  void onProjectClosed();

public:
  pqWaveTankMenu(QObject* parent = nullptr);
  ~pqWaveTankMenu() override;

protected:
  bool startup();
  void shutdown();

protected Q_SLOTS:

private:
  // In order they appear in the WaveTank menu:
  QAction* m_openProjectAction = nullptr;
  QAction* m_recentProjectsAction = nullptr;
  QAction* m_newProjectAction = nullptr;
  QAction* m_saveProjectAction = nullptr;
  QAction* m_closeProjectAction = nullptr;

  pqWaveTankRecentProjectsMenu* m_recentProjectsMenu = nullptr;
  bool m_saveMenuConfigured = false;

  Q_DISABLE_COPY(pqWaveTankMenu);
};

#endif // pqWaveTankMenu_h
