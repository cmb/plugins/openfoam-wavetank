//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "plugin/pqWaveTankPluginLocation.h"

#include "smtk/simulation/wavetank/Metadata.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>

pqWaveTankPluginLocation::pqWaveTankPluginLocation(QObject* parent)
  : Superclass(parent)
{
}

pqWaveTankPluginLocation::~pqWaveTankPluginLocation() {}

void pqWaveTankPluginLocation::StoreLocation(const char* fileLocation)
{
  if (fileLocation == nullptr)
  {
    qInfo() << "Internal Error: pqWaveTankPluginLocation::StoreLocation() called with null pointer";
    return;
  }

  QFileInfo fileInfo(fileLocation);
  QDir dirLocation(fileInfo.absoluteDir());

#ifndef NDEBUG
  qInfo() << "Plugin location: " << fileLocation;
  qInfo() << "Plugin directory: " << dirLocation.path();
#endif

#ifndef DEVELOPER_MODE
  this->locateWorkflows(dirLocation);
#endif
}

void pqWaveTankPluginLocation::locateWorkflows(const QDir& pluginDirectory) const
{
  // Save starting directory for diagnostics
  QDir startingDir(pluginDirectory);

  // Look for path to simulation workflows
  // * Depends on superbuild-packaging organization
  // * Which is platform-dependent
  QString relativePath;
#if defined(_WIN32)
  relativePath = "../../../share/cmb";
#elif defined(__APPLE__)
  relativePath = "../Resources";
#elif defined(__linux__)
#ifdef NDEBUG
  relativePath = "../../../share/cmb";
#else
  // Alternate path for development
  relativePath = "../../../install/share";
#endif
#endif

  if (relativePath.isEmpty())
  {
#ifdef NDEBUG
    qCritical() << "Missing installed path to workflow files";
#endif
    return;
  }

  // Locate path to workflows
  QDir dirLocation(pluginDirectory);
  bool exists = dirLocation.cd(relativePath);
  if (!exists)
  {
    QString path = startingDir.path() + "/" + relativePath;
    qCritical() << "Plugin workflows directory not found:" << path
                << "\n==> Do you need to set DEVELOPER_MODE (cmake variable)?";
    return;
  }

  // Locate workflows directory
  {
    QDir dir(dirLocation);
    QString relativePath("workflows/WaveTank/workflows");
    bool exists = dir.cd(relativePath);
    if (exists)
    {
      // #ifndef NDEBUG
      qInfo() << "Setting workflows directory to" << dir.absolutePath();
      // #endif
      smtk::simulation::wavetank::Metadata::WORKFLOWS_DIRECTORY = dir.absolutePath().toStdString();
    }
    else
    {
      qCritical() << "No workflows directory found for starting directory"
                  << dirLocation.absolutePath() << "and relative path" << relativePath;
    }
  }

  // Locate operations directory
  {
    QDir dir(dirLocation);
    QString relativePath("workflows/WaveTank/operations");
    bool exists = dir.cd(relativePath);
    if (exists)
    {
      // #ifndef NDEBUG
      qInfo() << "Setting operations directory to" << dir.absolutePath();
      // #endif
      smtk::simulation::wavetank::Metadata::OPERATIONS_DIRECTORY = dir.absolutePath().toStdString();
    }
    else
    {
      qCritical() << "No operations directory found for starting directory"
                  << dirLocation.absolutePath() << "and relative path" << relativePath;
    }
  }
}
