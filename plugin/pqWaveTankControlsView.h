//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWaveTankControlsView_h
#define plugin_pqWaveTankControlsView_h

#include "smtk/extension/qt/qtBaseView.h"

#include <QMap>
#include <QString>

class pqDataRepresentation;
class pqPipelineSource;

/** \brief A custom smtk view for presenting controls to the user.
 *
 * The controls include running an operation to generate OpenFOAM
 * input files, running the OpenFOAM application itself, setting
 * visibility for the resulting data, etc.
 *
 * Uses/supports additional XML elements inside <View>:
 *
 *  <Operation> specifies the smtk operation to run
 *    - set element's content to the operation name
 *    - optional attribute UseDialog="True" to display the operation dialog
 *
 *  optional <Category> has 2 uses
 *    - show/hide the control based on the current active categories
 *    - required for blockMesh views to identify which attribute to use for settings
 *
 *  optional <FoamApplication> specifies the OpenFOAM executable to run after the operation completes
 *    - set required attribute CaseDirectory with the name of the case directory to use
 *
 *  optional <FoamArguments> is used to specify arguments to pass to the foam executable
 *     - use <Argument> child element for each argument
 *
 *  optional <ShellScript> specifies a shell script to run after the operation completes
 *    - set required attribute CaseDirectory with the name of the case directory to use
 *
 *  optional <Geometry> specifies the result produced by the operation
 *    - attribute Type specifies either "ResourceRole" or "FoamFile"
 *    - For ResourceRole, set the element's content to the resource's project role
 *    - For FoamFile, set the element's content to the .foam filename
 *    - optional Representation element to set the initial display rep, e.g., "Surface With Edges"
 *
 * */

class pqWaveTankControlsView : public smtk::extension::qtBaseView
{
  Q_OBJECT
public:
  smtkTypenameMacro(pqWaveTankControlsView);

  static qtBaseView* createViewWidget(const smtk::view::Information& info);
  pqWaveTankControlsView(const smtk::view::Information& info);
  virtual ~pqWaveTankControlsView();

  bool isEmpty() const override;

  /** \brief Clears source map; should be called when project is closed. */
  static void clearSourceMap();

Q_SIGNALS:

public Q_SLOTS:
  /** \brief Update controls based on current project state.
     *
     * This is called each time the view is displayed, which is
     * typically when its parent tab is selected.
     */
  void updateUI() override;

protected Q_SLOTS:
  /** \brief Runs the operation and launches the foam application. */
  void onRunClicked();

  /** \brief Creates and runs operation dialog. */
  void runOperationDialog(smtk::operation::OperationPtr op);

  /** \brief Processes result from operation */
  void onOperationExecuted(smtk::attribute::AttributePtr result);

  /** \brief Called when the foam application completes */
  void onProcessFinished(qint64 pid, int exitcode, const QString& viewName);

  /** \brief Other user actions */
  void onDisplayClicked();
  void onRepresentationChanged();
  void onOpacityChanged(int i);
  void onHideOthersClicked();

protected:
  void createWidget() override;
  bool runAllclean();
  void updateLogControls();
  void updateGeometryControls();

  /** \brief Locates pqSMTKResource associated with given resource role
     *
     * Also inserts it into the map.
     * Note that pqSMTKResource is subclass of pqPipelineSource.
     */
  pqPipelineSource* locateSMTKResource(const QString& resourceRole);

  /** \brief Looks up representation, checking if its in the active view */
  pqDataRepresentation* getActiveRepresentation() const;

  /** \brief Looks up pipeline source in s_foamSourceMap or project resources */
  pqPipelineSource* getPipelineSource() const;

  /** \brief Finds or creates pipeline source for give foam file
     *
     *  Sets isNew true if source loaded from file.
     */
  pqPipelineSource* getPipelineSource(const QString& foamfile, bool& isNew);

  /** \brief Shared dictionary of pipeline sources keyed by string identifier
     *
     * Identifier is either foam file or resource role.
     */
  static QMap<QString, pqPipelineSource*> s_pipelineSourceMap;

private:
  class Internal;
  Internal* m_internal;
};

#endif
