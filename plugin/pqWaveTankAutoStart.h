//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWaveTankAutoStart_h
#define plugin_pqWaveTankAutoStart_h

#include <QObject>

class pqServer;
class pqSMTKWrapper;
class pqWaveTankContainerEngineSetup;
class vtkSMProxy;

class pqWaveTankAutoStart : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqWaveTankAutoStart(QObject* parent = nullptr);
  ~pqWaveTankAutoStart() = default;

  void startup();
  void shutdown();

protected Q_SLOTS:
  void resourceManagerAdded(pqSMTKWrapper* mgr, pqServer* server);
  void resourceManagerRemoved(pqSMTKWrapper* mgr, pqServer* server);

  /** \brief Wait for other parts of the app to start
   *
   * Before initializing, this plugin needs:
   *   - the main widget to be instantiated
   *   - the plugin location to be resolved
   *   - smtk settings are instantiated
   *   - the resource manager to be added
   */
  void waitForApplication();

  /** \brief Checks that openfoam can be accessed. */
  void setupLocalInstall();

  /** \brief Runs steps to find and configure container engine. */
  void setupContainerEngine();

  /** \brief Registers operations */
  void registerOperations();

protected:
  void initializeSmtkSettings(vtkSMProxy* settingsProxy);
  bool readWaveTankSettings(vtkSMProxy* settingsProxy);

  pqWaveTankContainerEngineSetup* m_containerSetup = nullptr;

private:
  Q_DISABLE_COPY(pqWaveTankAutoStart);
};

#endif
