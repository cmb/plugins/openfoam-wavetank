//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWaveTankContainerEngineSetup_h
#define plugin_pqWaveTankContainerEngineSetup_h

#include <QObject>

#include <QProcess>
#include <QString>

class QWidget;

class qtProgressDialog;

/**\brief pqWaveTankContainerEngineSetup
 *
 * Handles the UI logic for configuring the container engine
 * and fetching the openfoam docker image.
 */

class pqWaveTankContainerEngineSetup : public QObject
{
  Q_OBJECT
public:
  using Superclass = QObject;

  enum ContainerEngineStatus
  {
    NotUsed = 0,
    Uninitialized,
    Pending,
    Ready,
    Unavailable,
    Error
  };

  pqWaveTankContainerEngineSetup(QWidget* parentWidget);
  ~pqWaveTankContainerEngineSetup() = default;

  /** \brief Runs the setup/configuration logic
     *
     */
  bool start();

  QString containerEnginePath() const { return m_containerEnginePath; }
  ContainerEngineStatus status() const { return m_status; }
  QString statusAsString() const;

Q_SIGNALS:
  void finished();

public Q_SLOTS:

protected Q_SLOTS:
  void onProcessError(QProcess::ProcessError error);
  void onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
  void onTryLsError();
  void pullDockerImage(); // pulls public OpenFOAM image (opencfd/openfoam-run)
  void buildLocalImage(); // builds local image to workaround known issues with OpenFOAM image

protected:
  enum ProcessMode
  {
    Undefined = 0,
    TryLs,
    TryPull,
    TryBuild
  };

  bool readSetting(QString& containerEnginePath) const;
  bool updateSetting(const QString& containerEnginePath) const;

  // Runs container "image ls" command  for openfoam docker image
  void tryLs(const QString& containerEnginePath);

  /** \brief methods getting/checking the docker executable */
  void askUserForPath(const QString& headline, const QString& details, QString& result) const;
  bool checkPath(const QString& path, QString& reason) const;
  bool getContainerEnginePath(QString& containerEnginePath) const;

  QString m_containerEnginePath;
  ContainerEngineStatus m_status = Uninitialized;
  ProcessMode m_processMode = Undefined;
  QProcess* m_process = nullptr;
  QWidget* m_parentWidget = nullptr;
  qtProgressDialog* m_progressDialog = nullptr;
};

#endif
