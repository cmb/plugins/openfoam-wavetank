//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pqWaveTankProjectLoader_h
#define pqWaveTankProjectLoader_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/operation/Observer.h"

#include <QObject>

class pqServer;
class pqServerResource;

/** \brief Handles opening projects from main menu and recent projects list
  */
class pqWaveTankProjectLoader : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  static pqWaveTankProjectLoader* instance(QObject* parent = nullptr);
  ~pqWaveTankProjectLoader() override;

  // Indicates if paraview resource can be loaded
  bool canLoad(const pqServerResource& resource) const;

  // Open project from path on server
  bool load(pqServer* server, const QString& path);

  // Open project from recent projects list
  bool load(const pqServerResource& resource);

Q_SIGNALS:
  void projectOpened(smtk::project::ProjectPtr);

protected:
  pqWaveTankProjectLoader(QObject* parent = nullptr);
  std::shared_ptr<smtk::operation::Operation> m_readOp;
  smtk::operation::Observers::Key m_observer;

  // A method called when operations are run, used to update the "Recent projects" menu.
  int observer(
    const smtk::operation::Operation& op,
    smtk::operation::EventType event,
    smtk::operation::Operation::Result result,
    pqServer* server,
    const QString& directoryPath);

private:
  Q_DISABLE_COPY(pqWaveTankProjectLoader);
};

#endif // pqWaveTankProjectLoader_h
