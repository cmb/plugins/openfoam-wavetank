//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWaveTankCloseProjectBehavior_h
#define plugin_pqWaveTankCloseProjectBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for closing a project.
class pqWaveTankCloseProjectReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqWaveTankCloseProjectReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqWaveTankCloseProjectReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqWaveTankCloseProjectBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqWaveTankCloseProjectBehavior* instance(QObject* parent = nullptr);
  ~pqWaveTankCloseProjectBehavior() override;

  /** \brief Closes project, returning true if NOT canceled by the user
   *
   * In other words, the return value reports if the close action
   * was "not cancelled".
   */
  bool closeProject();

Q_SIGNALS:
  void projectClosed();

protected:
  pqWaveTankCloseProjectBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqWaveTankCloseProjectBehavior);
};

#endif
