//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

// local includes
#include "plugin/pqWaveTankAutoStart.h"
#include "plugin/pqWaveTankContainerEngineSetup.h"

#include "plugin/pqWaveTankCloseProjectBehavior.h"
#include "plugin/pqWaveTankControlsView.h"
#include "smtk/simulation/wavetank/Metadata.h"
#include "smtk/simulation/wavetank/Registrar.h"
#include "smtk/simulation/wavetank/qt/qtOpenFoamRunner.h"
#include "smtk/simulation/wavetank/qt/qtSessionData.h"

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResourcePanel.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/paraview/server/vtkSMTKSettings.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Project.h"
#include "smtk/view/Manager.h"
#include "smtk/view/ResourcePhraseModel.h"
#include "smtk/view/ViewWidgetFactory.h"

// ParaView includes
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqAutoApplyReaction.h"
#include "pqCoreConfiguration.h"
#include "pqCoreUtilities.h"
#include "pqMainWindowEventManager.h"
#include "pqServer.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMProperty.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMStringVectorProperty.h"

#include <QApplication>
#include <QCloseEvent>
#include <QDebug>
#include <QDir>
#include <QList>
#include <QMainWindow>
#include <QMessageBox>
#include <QProcess>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QTimer>
#include <QWidget>

// Set this for more info during startup sequence
#define DEBUG_STARTUP 0

using FoamMode = smtk::simulation::wavetank::OpenFoamMode;

namespace
{
// Configure resource panel to omit displaying projects
// Code is copied from smtk::project::AutoStart
void setView()
{
  for (QWidget* w : QApplication::topLevelWidgets())
  {
    QMainWindow* mainWindow = dynamic_cast<QMainWindow*>(w);
    if (mainWindow)
    {
      pqSMTKResourcePanel* dock = mainWindow->findChild<pqSMTKResourcePanel*>();
      // If the dock is not there, just try it again.
      if (dock)
      {
        auto phraseModel = std::dynamic_pointer_cast<smtk::view::ResourcePhraseModel>(
          dock->resourceBrowser()->phraseModel());
        if (phraseModel)
        {
          phraseModel->setFilter([](const smtk::resource::Resource& resource) {
            return !resource.isOfType(smtk::common::typeName<smtk::project::Project>());
          });
        }
      }
      else
      {
        QTimer::singleShot(10, []() { setView(); });
      }
    }
  }
}

int retryCount = 0;
} // namespace

pqWaveTankAutoStart::pqWaveTankAutoStart(QObject* parent)
  : Superclass(parent)
{
}

void pqWaveTankAutoStart::startup()
{
  qInfo() << "running startup\n";
// Set workflows folder for developer builds
#ifdef WORKFLOWS_SOURCE_DIR
  QDir workflowsDir(WORKFLOWS_SOURCE_DIR);
  if (workflowsDir.exists())
  {
    smtk::simulation::wavetank::Metadata::WORKFLOWS_DIRECTORY = WORKFLOWS_SOURCE_DIR;
#ifndef NDEBUG
    qDebug() << "Using Workflows directory" << WORKFLOWS_SOURCE_DIR;
#endif
  }
#endif

#ifdef OPERATIONS_SOURCE_DIR
  QDir operationsDir(OPERATIONS_SOURCE_DIR);
  if (operationsDir.exists())
  {
    smtk::simulation::wavetank::Metadata::OPERATIONS_DIRECTORY = OPERATIONS_SOURCE_DIR;
#ifndef NDEBUG
    qDebug() << "Using Operations directory" << OPERATIONS_SOURCE_DIR;
#endif
  }
#endif

  // Check for current/active pqServer
  pqServer* server = pqActiveObjects::instance().activeServer();
  if (server != nullptr)
  {
    pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
    this->resourceManagerAdded(wrapper, server);
  }

  pqAutoApplyReaction::setAutoApply(true);

  // Connect to application close event
  QObject::connect(
    pqApplicationCore::instance()->getMainWindowEventManager(),
    &pqMainWindowEventManager::close,
    [](QCloseEvent* closeEvent) {
      auto runtime = smtk::simulation::wavetank::qtSessionData::instance();
      if (!runtime)
      {
        closeEvent->accept();
        return;
      }

      bool notCanceled = true;

      auto project = runtime->project();
      if (project && !project->clean())
      {
        notCanceled = pqWaveTankCloseProjectBehavior::instance()->closeProject();
      }

      closeEvent->setAccepted(notCanceled);
    });

  // Instantiate singletons
  smtk::simulation::wavetank::qtSessionData::instance(this);
  auto* runner = smtk::simulation::wavetank::qtOpenFoamRunner::instance(this);
  runner->useOpenFoamDocker(true);

  // Finish initialization after application is ready
  this->waitForApplication();
}

void pqWaveTankAutoStart::shutdown() {}

void pqWaveTankAutoStart::resourceManagerAdded(pqSMTKWrapper* wrapper, pqServer* server)
{
  if (!wrapper || !server)
  {
    return;
  }

  smtk::resource::ManagerPtr resManager = wrapper->smtkResourceManager();
  smtk::view::ManagerPtr viewManager = wrapper->smtkViewManager();
  smtk::operation::ManagerPtr opManager = wrapper->smtkOperationManager();
  smtk::project::ManagerPtr projManager = wrapper->smtkProjectManager();
  if (!resManager || !viewManager || !opManager || !projManager)
  {
    return;
  }

  // Register custom view (mini control panel)
  viewManager->viewWidgetFactory().registerType<pqWaveTankControlsView>();
  viewManager->viewWidgetFactory().addAlias<pqWaveTankControlsView>("WaveTankControls");

  auto* sessionData = smtk::simulation::wavetank::qtSessionData::instance();
  sessionData->setOperationManager(opManager);
  sessionData->setViewManager(viewManager);
}

void pqWaveTankAutoStart::resourceManagerRemoved(pqSMTKWrapper* wrapper, pqServer* server)
{
  if (!wrapper || !server)
  {
    return;
  }

  smtk::view::ManagerPtr viewManager = wrapper->smtkViewManager();
  if (viewManager != nullptr)
  {
  }

  smtk::project::ManagerPtr projManager = wrapper->smtkProjectManager();
  if (projManager != nullptr)
  {
    smtk::simulation::wavetank::Registrar::unregisterFrom(projManager);
  }
}

void pqWaveTankAutoStart::waitForApplication()
{
  // Wait for main widget
  QWidget* mainWidget = pqCoreUtilities::mainWidget();
  if (mainWidget == nullptr)
  {
    QTimer::singleShot(11, this, &pqWaveTankAutoStart::waitForApplication);
    return;
  }

  // Wait for plugin location
  std::string workflowsDir = smtk::simulation::wavetank::Metadata::WORKFLOWS_DIRECTORY;
  std::string operationsDir = smtk::simulation::wavetank::Metadata::OPERATIONS_DIRECTORY;
  if (workflowsDir.empty() || operationsDir.empty())
  {
    QTimer::singleShot(11, this, &pqWaveTankAutoStart::waitForApplication);
    return;
  }

  // Wait for pqServer
  pqServer* server = pqActiveObjects::instance().activeServer();
  if (server == nullptr)
  {
    QTimer::singleShot(11, this, &pqWaveTankAutoStart::waitForApplication);
    return;
  }

  // Wait for SMTK settings
  vtkSMProxy* smtkProxy = server->proxyManager()->GetProxy("settings", "SMTKSettings");
  if (smtkProxy == nullptr)
  {
    QTimer::singleShot(11, this, &pqWaveTankAutoStart::waitForApplication);
    return;
  }

  // Wait for Wave Tank Settings
  vtkSMProxy* waveTankProxy = server->proxyManager()->GetProxy("settings", "WaveTankSettings");
  if (waveTankProxy == nullptr)
  {
    qDebug() << "Waiting for WaveTankSettings";
    QTimer::singleShot(11, this, &pqWaveTankAutoStart::waitForApplication);
    return;
  }
  // Can now start plugin initialization
  auto* sessionData = smtk::simulation::wavetank::qtSessionData::instance();
  sessionData->setMainWidget(mainWidget);
  smtk::simulation::wavetank::qtOpenFoamRunner::instance()->setParentWidget(mainWidget);

  qDebug() << "initializeSmtkSettings...\n";
  this->initializeSmtkSettings(smtkProxy);
  this->readWaveTankSettings(waveTankProxy);

  // This is hack/workaround to enable package testing in cmb-superbuild.
  // If any test scripts are specified, skip the user-interface dialogs to configure
  // the container engine.
  auto* coreConfig = pqCoreConfiguration::instance();
  if (coreConfig == nullptr)
  {
    qWarning() << "pqCoreConfiguration instance is null";
  }
  else if (coreConfig->testScriptCount() > 0)
  {
    qInfo() << "Test script count is" << coreConfig->testScriptCount()
            << "==> skipping container engine setup.";

    m_containerSetup = new pqWaveTankContainerEngineSetup(mainWidget);
    qDebug() << "registering ops...\n";
    this->registerOperations();
    return;
  }

  // Finish setup based on OpenFoamMode
  auto foamMode = sessionData->openFoamMode();
  qDebug() << "foamMode:" << sessionData->openFoamModeAsString();
  switch (foamMode)
  {
    case FoamMode::Undefined:
    {
      QString text;
      QTextStream qs(&text);
      qs << "OpenFOAM access has not been set."
         << "  Go to the Settings panel ==> WaveTank tab"
         << ", and set the \"OpenFOAM Executable\" field to"
         << " \"Local Install\", \"Container Engine\", or \"Not Used\"."
         << "<br/><br/>Note that you will need to RESTART MODELBUILDER"
         << " after changing that setting.";
      QMessageBox::information(mainWidget, "OpenFOAM Access Not Defined", text);
    }
      return;

    case FoamMode::LocalInstall:
    case FoamMode::TestLocal:
      this->registerOperations();
      this->setupLocalInstall();
      break;

    case FoamMode::ContainerEngine:
      this->setupContainerEngine();
      break;

    case FoamMode::NotUsed:
    default:
      this->registerOperations();
      break;
  } // switch (foamMode)
}

void pqWaveTankAutoStart::setupLocalInstall()
{
  auto foamMode = smtk::simulation::wavetank::qtSessionData::instance()->openFoamMode();

  QString program;
  QStringList arguments;
  if (foamMode == FoamMode::TestLocal)
  {
    program = "echo";
    arguments << "Hello World";
  }
  else
  {
    qDebug() << "Check for local OpenFOAM";
    program = "icoFoam";
    arguments << "-help";
  }

  int exitCode = QProcess::execute(program, arguments);
  if (exitCode != 0)
  {
    QString text;
    QTextStream qs(&text);
    qs << "Cannot access OpenFOAM applications."
       << " Control views will not be available.";

    auto* sessionData = smtk::simulation::wavetank::qtSessionData::instance();
    QWidget* mainWidget = sessionData->mainWidget();
    QMessageBox::warning(mainWidget, "Cannot Access OpenFOAM", text);

    // Update session data to NotUsed
    sessionData->setOpenFoamMode(FoamMode::NotUsed);
  }
  else
  {
    qDebug() << "Confirmed OpenFOAM access.";
  }
}

void pqWaveTankAutoStart::setupContainerEngine()
{
#if DEBUG_STARTUP
  static int counter = 0;
  qDebug() << "Start container engine setup" << ++counter;
#endif
  QWidget* mainWidget = smtk::simulation::wavetank::qtSessionData::instance()->mainWidget();

  if (m_containerSetup == nullptr)
  {
    m_containerSetup = new pqWaveTankContainerEngineSetup(mainWidget);
    QObject::connect(
      m_containerSetup,
      &pqWaveTankContainerEngineSetup::finished,
      this,
      &pqWaveTankAutoStart::registerOperations);
  }

  if (m_containerSetup->status() == pqWaveTankContainerEngineSetup::Uninitialized)
  {
    m_containerSetup->start();
    QTimer::singleShot(103, this, &pqWaveTankAutoStart::setupContainerEngine);
    return;
  }
  else if (m_containerSetup->status() == pqWaveTankContainerEngineSetup::Pending)
  {
    // qDebug() << "Waiting for container engine status";
    QTimer::singleShot(103, this, &pqWaveTankAutoStart::setupContainerEngine);
    return;
  }
  else if (m_containerSetup->status() == pqWaveTankContainerEngineSetup::Error)
  {
    QString msg = "There was an error during container engine configuration."
                  " The application is not able to run OpenFOAM applications.";
    QMessageBox::critical(mainWidget, "Internal Error", msg);
    smtk::simulation::wavetank::qtSessionData::instance()->setContainerEngine("");
    return;
  }
#if DEBUG_STARTUP
  qDebug() << "Container Status" << m_containerSetup->statusAsString();
#endif
}

void pqWaveTankAutoStart::registerOperations()
{
  pqServer* server = pqActiveObjects::instance().activeServer();
  auto smtkBehavior = pqSMTKBehavior::instance();
  auto wrapper = smtkBehavior->resourceManagerForServer();
  if (wrapper == nullptr)
  {
    QTimer::singleShot(11, this, &pqWaveTankAutoStart::registerOperations);
  }
  this->resourceManagerAdded(wrapper, server);

  smtk::operation::ManagerPtr opManager = wrapper->smtkOperationManager();
  smtk::project::ManagerPtr projManager = wrapper->smtkProjectManager();
  // Register operations
  smtk::simulation::wavetank::Registrar::registerTo(projManager);

  // Run SetupPaths operation
  std::string setupOpName = "setup_paths.SetupPaths";
  auto setupOp = opManager->create(setupOpName);
  if (setupOp == nullptr)
  {
    qCritical() << "Internal Error: Unable to create" << setupOpName.c_str() << "operation";
    return;
  }
  setupOp->parameters()
    ->findDirectory("workflows_folder")
    ->setValue(smtk::simulation::wavetank::Metadata::WORKFLOWS_DIRECTORY);
  setupOp->parameters()
    ->findDirectory("operations_folder")
    ->setValue(smtk::simulation::wavetank::Metadata::OPERATIONS_DIRECTORY);
  setupOp->parameters()->findString("container_engine")->setIsEnabled(false);

  if (m_containerSetup && m_containerSetup->status() == pqWaveTankContainerEngineSetup::Ready)
  {
    std::string containerEnginePath = m_containerSetup->containerEnginePath().toStdString();
    auto engineItem = setupOp->parameters()->findString("container_engine");
    engineItem->setIsEnabled(true);
    engineItem->setValue(containerEnginePath);
  }

  auto result = setupOp->operate();
  int outcome = result->findInt("outcome")->value();
  if (outcome != static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
  {
    qCritical() << "Error setting up operation paths, outcome" << outcome;
    return;
  }

  auto* sessionData = smtk::simulation::wavetank::qtSessionData::instance();

  std::string uid = result->findString("uid")->value();
  QString QUID = QString::fromStdString(uid);
  sessionData->setDockerUID(QUID);

  std::string gid = result->findString("gid")->value();
  QString QGID = QString::fromStdString(gid);
  sessionData->setDockerGID(QGID);

  // Setup connections for add/remote resource manager
  QObject::connect(
    smtkBehavior,
    static_cast<void (pqSMTKBehavior::*)(pqSMTKWrapper*, pqServer*)>(
      &pqSMTKBehavior::addedManagerOnServer),
    this,
    &pqWaveTankAutoStart::resourceManagerAdded);
  QObject::connect(
    smtkBehavior,
    static_cast<void (pqSMTKBehavior::*)(pqSMTKWrapper*, pqServer*)>(
      &pqSMTKBehavior::removingManagerFromServer),
    this,
    &pqWaveTankAutoStart::resourceManagerRemoved);

  qInfo() << "Wave Tank plugin initialized.";
}

void pqWaveTankAutoStart::initializeSmtkSettings(vtkSMProxy* proxy)
{
  // Disable the smtk save-on-close dialog (superseded in our close behavior)
  vtkSMProperty* ssProp = proxy->GetProperty("ShowSaveResourceOnClose");
  vtkSMIntVectorProperty* ssIntProp = vtkSMIntVectorProperty::SafeDownCast(ssProp);
  if (ssIntProp != nullptr)
  {
    ssIntProp->SetElement(0, vtkSMTKSettings::DontShowAndDiscard);
  }
  else
  {
    qWarning() << "Property not found: ShowSaveResourceOnClose";
  }

  // Enable highlight on hover
  vtkSMProperty* hhProp = proxy->GetProperty("HighlightOnHover");
  vtkSMIntVectorProperty* hhIntProp = vtkSMIntVectorProperty::SafeDownCast(hhProp);
  if (hhIntProp != nullptr)
  {
    hhIntProp->SetElement(0, 1);
  } // if (ssIntProp)

  proxy->UpdateVTKObjects();
}

bool pqWaveTankAutoStart::readWaveTankSettings(vtkSMProxy* proxy)
{
  auto* sessionData = smtk::simulation::wavetank::qtSessionData::instance();

  // Read OpenFOAM exe mode
  vtkSMProperty* fmProp = proxy->GetProperty("OpenFoamMode");
  auto* fmIntProp = vtkSMIntVectorProperty::SafeDownCast(fmProp);
  if (fmIntProp == nullptr)
  {
    qCritical() << "Internal Error: OpenFoamMode not found in WaveTank settings.";
    return false;
  }
  int intMode = fmIntProp->GetElement(0);
  auto foamMode = static_cast<smtk::simulation::wavetank::OpenFoamMode>(intMode);
  sessionData->setOpenFoamMode(foamMode);

  // Read ContainerEnginePath
  vtkSMProperty* pathProp = proxy->GetProperty("ContainerEnginePath");
  auto* pathStringProp = vtkSMStringVectorProperty::SafeDownCast(pathProp);
  if (!pathStringProp)
  {
    qCritical() << "Internal Error: ContainerEnginePath not found in WaveTank settings.";
    return false;
  }
  const char* path = pathStringProp->GetElement(0);
  qDebug() << "Read settings: "
           << "OpenFoamMode" << foamMode << ", ContainerEnginePath:" << path << ".";
  sessionData->setContainerEngine(path);

  return true;
}
