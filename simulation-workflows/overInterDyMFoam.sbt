<?xml version="1.0"?>
<SMTK_AttributeResource
  Version="6"
  DisplayHint="true"
  TemplateType="foam.wavetank"
  TemplateVersion="1"
>
<!-- 27-Feb-2023 TemplateVersion 1: adds TransportProperties attribute. -->

  <Categories>
    <Cat>overset</Cat>
    <Cat>background</Cat>
    <Cat>object</Cat>
    <Cat>3d</Cat>
    <Cat>2d</Cat>
  </Categories>

  <!-- For now, make all background/object/overset categories active -->
  <Analyses Exclusive="true">
    <Analysis Type="overInterDyMFoam" Exclusive="True">
      <Cat>overset</Cat>
      <Cat>background</Cat>
      <Cat>object</Cat>
    </Analysis>
    <Analysis Type="Two Dimension" BaseType="overInterDyMFoam">
      <Cat>2d</Cat>
    </Analysis>
    <Analysis Type="Three Dimension" BaseType="overInterDyMFoam">
      <Cat>3d</Cat>
    </Analysis>
  </Analyses>


  <Includes>
    <File>internal/templates/wavetank-common.sbt</File>
  </Includes>

  <Definitions>
    <!-- Explicity specify analysis attribute definition so that we can
         initialize it the way we want.
    -->
    <AttDef Type="Analysis" Label="Analysis">
      <ItemDefinitions>
        <String Name="Analysis" Label="Solver" NumberOfRequiredValues="1">
          <ChildrenDefinitions>
            <String Name="overInterDyMFoam" Label="overInterDyMFoam" NumberOfRequiredValues="1">
              <DiscreteInfo DefaultIndex="0">
                <!-- <Value Enum="Two Dimension">Two Dimension</Value> -->
                <Value Enum="Three Dimension">Three Dimension</Value>
              </DiscreteInfo>
            </String>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="overInterDyMFoam">overInterDyMFoam</Value>
              <Items>
                <Item>overInterDyMFoam</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="BackgroundBlockMesh" BaseType="AbstractBlockMesh">
      <!-- Use inheritance mode to include BaseType items w/o adding categories -->
      <CategoryInfo InheritanceMode="LocalOnly">
        <Include><Cat>background</Cat></Include>
      </CategoryInfo>
    </AttDef>

    <AttDef Type="OversetBlockMesh" BaseType="AbstractBlockMesh">
      <!-- Use inheritance mode to include BaseType items w/o adding categories -->
      <CategoryInfo InheritanceMode="LocalOnly">
        <Include><Cat>overset</Cat></Include>
      </CategoryInfo>
    </AttDef>

  </Definitions>

  <Views>
    <!-- _______________Top level tab group_______________ -->
    <View Type="Group" Title="Wave Tank" TopLevel="true" TabPosition="North"
    FilterByAdvanceLevel="true" FilterByCategory="false">
      <Views>
        <View Title="ModelGroup" />
        <View Title="OversetMesh" />
        <View Title="BackgroundMesh" />
        <View Title="MergeMesh" />
        <View Title="Physics" />
        <View Title="Control" />
      </Views>
    </View>

     <!-- __________Model tab__________ -->
     <View Type="Group" Title="ModelGroup" Label="Model" Style="Tiled">
      <Views>
        <View Title="ImportModel" />
        <View Title="SurfaceInertia" />
      </Views>
     </View>

    <View Name="ImportModel" Type="WaveTankControls" Label="Import Model">
      <Operation UseDialog="true">import_model.ImportModel</Operation>
      <Geometry Type="ResourceRole">model</Geometry>
    </View>

    <View Name="SurfaceInertia" Type="WaveTankControls" Label="surfaceInertia">
      <CaseDirectory>overset</CaseDirectory>
      <Operation UseDialog="true">surfaceinertia.SurfaceInertia</Operation>
      <FoamApplication>surfaceInertia</FoamApplication>
    </View>

    <View Type="Instanced" Title="ModelInstance" Label="Model Point">
      <InstancedAttributes>
        <Att Type="Object" Name="Object">
          <ItemViews>
            <View Path="/InsidePoint" Type="Point" ShowControls="true" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Name="SurfaceFeatureControls" Type="WaveTankControls" Label="surfaceFeatureExtract">
      <CaseDirectory>overset</CaseDirectory>
      <Operation>surfacefeatureextract.SurfaceFeatureExtract</Operation>
      <FoamApplication>surfaceFeatureExtract</FoamApplication>
    </View>

    <!-- Analysis view is contained inside Control view in wavetank-common.sbt -->
    <View Type="Analysis" Title="Solver" Label="Solver"
      AnalysisAttributeName="Analysis" AnalysisAttributeType="Analysis">
    </View>

    <!-- __________Overset mesh tab group__________ -->
    <View Title="OversetMesh" Type="Group" Label="Overset Mesh" TabPosition="North">
      <Views>
        <!-- <View Title="Object" /> -->
        <View Title="OversetBlockMeshGroup" />
        <View Title="SnappyGroup" />
      </Views>
    </View>

    <View Type="Group" Title="Object" Label="Object" Style="Tiled">
      <Views>
        <View Title="ObjectPoint" />
        <View Title="SurfaceFeatures" />
<!--         <View Title="MassProperties" /> -->
      </Views>
    </View>

    <View Type="Instanced" Title="OversetBlockMesh" Label="blockMesh">
      <InstancedAttributes>
        <Att Type="OversetBlockMesh" Name="OversetBlockMesh">
          <ItemViews>
            <View Item="box" Type="Box" Min="point 0" Max="point 1" Angles="angles" ShowControls="true"/>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Name="OversetBlockControls" Type="WaveTankControls" Label="blockMeshControl">
      <CaseDirectory>overset</CaseDirectory>
      <Operation>blockmesh.BlockMesh</Operation>
      <Category>overset</Category>
      <FoamApplication>blockMesh</FoamApplication>
      <Geometry Type="FoamFile" Representation="Surface With Edges">oversetMesh.foam</Geometry>
    </View>

    <View Title="OversetBlockMeshGroup" Type="Group" Label="Overset BlockMesh"  Style="Tiled">
      <Views>
        <View Title="OversetBlockMesh" />
        <View Title="OversetBlockControls" />
      </Views>
    </View>

    <!-- __________Background Mesh__________ -->
    <View Title="BackgroundMesh" Type="Group" Label="Background Mesh" TabPosition="North">
      <Views>
        <View Title="backgroundBlockMeshGroup" />
        <View Title="refineMesh" />
      </Views>
    </View>

    <View Type="Instanced" Title="BackgroundBlockMesh" Label="blockMesh">
      <InstancedAttributes>
        <Att Type="BackgroundBlockMesh" Name="BackgroundBlockMesh">
          <ItemViews>
            <View Item="box" Type="Box" Min="point 0" Max="point 1" Angles="angles" ShowControls="true"/>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Name="BackgroundBlockControls" Type="WaveTankControls" Label="bgBlockMeshControl">
      <CaseDirectory>background</CaseDirectory>
      <Operation>blockmesh.BlockMesh</Operation>
      <Category>background</Category>
      <FoamApplication>blockMesh</FoamApplication>
      <Geometry Type="FoamFile" Representation="Surface With Edges">backgroundMesh.foam</Geometry>
    </View>

   <View Title="backgroundBlockMeshGroup" Type="Group" Label="Background BlockMesh"  Style="Tiled">
      <Views>
        <View Title="BackgroundBlockMesh" />
        <View Title="BackgroundBlockControls" />
      </Views>
    </View>
    <!-- __________MergeMesh tab group__________ -->
    <View Title="MergeMesh" Type="Group" Style="Tiled">
      <Views>
        <View Title="MergeMeshControls" />
        <!-- <View Title="SetFieldsControls" /> -->
      </Views>
    </View>

    <View Name="MergeMeshControls" Type="WaveTankControls" Label="Merge Meshes">
      <CaseDirectory Delete="true">analysis</CaseDirectory>
      <Operation UseDialog="false">mergemeshes.MergeMeshes</Operation>
      <FoamApplication RunInParentDirectory="true">mergeMeshes</FoamApplication>
      <FoamArguments>
        <Argument>./analysis</Argument>
        <Argument>./overset</Argument>
        <Argument>-overwrite</Argument>
      </FoamArguments>
      <Geometry Type="FoamFile" Representation="Surface With Edges">mergedMesh.foam</Geometry>
    </View>

    <View Name="SetFieldsControls" Type="WaveTankControls" Label="Set Fields">
      <Operation UseDialog="false">setfields.SetFields</Operation>
      <!-- <Geometry Type="ResourceRole">model</Geometry> -->
    </View>

    <!-- __________Physics tab group__________ -->
    <View Title="Physics" Type="Group" Style="Tiled">
      <Views>
        <View Title="MassProperties" />
        <View Title="TransportProperties" />
        <View Title="WaveModel" />
      </Views>
    </View>

    <View Type="Instanced" Title="TransportProperties" Label="Transport Properties">
      <InstancedAttributes>
        <Att Type="TransportProperties" Name="TransportProperties" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="WaveModel" Label="Wave Model">
      <InstancedAttributes>
        <Att Type="WaveModel" Name="WaveModel" />
      </InstancedAttributes>
    </View>
  </Views>

</SMTK_AttributeResource>
