<?xml version="1.0"?>
<SMTK_AttributeResource
  Version="6"
  DisplayHint="true"
  TemplateType="foam.wavetank.interfoam"
  TemplateVersion="1"
>
<!-- 27-Feb-2023 TemplateVersion 1: adds TransportProperties attribute. -->

  <Categories>
    <Cat>interfoam</Cat>
    <Cat>background</Cat>
    <Cat>object</Cat>
  </Categories>

  <!-- For now, make all categories active -->
  <Analyses>
    <Analysis Type="interFoam">
      <Cat>interfoam</Cat>
      <Cat>background</Cat>
      <Cat>object</Cat>
    </Analysis>
  </Analyses>


  <Includes>
    <File>internal/templates/wavetank-common.sbt</File>
  </Includes>

  <Definitions>
    <!-- Explicity specify analysis attribute definition so that we can
         initialize it the way we want.

         Note that group items are marked AdvanceLevel 1 so that they do not
         appear in the standard UI. The contents of these groups are intended
         to be modified internally by the application software.
    -->
    <AttDef Type="Analysis">
      <ItemDefinitions>
        <String Name="Analysis" Label="Solver" NumberOfRequiredValues="1">
          <ChildrenDefinitions>
            <Group Name="interFoam" NumberOfRequiredGroups="1" AdvanceLevel="1">
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Value>interFoam</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="BlockMesh" BaseType="AbstractBlockMesh" />
  </Definitions>

  <Views>
    <View Type="Group" Title="Wave Tank" TopLevel="true" TabPosition="North"
    FilterByAdvanceLevel="true" FilterByCategory="false">
      <Views>
        <View Title="Control" />
        <View Title="blockMesh" />
        <View Title="refineMesh" />
        <View Title="Object" />
        <View Title="Snappy" />
        <View Title="Physics" />
      </Views>
    </View>

    <!-- Analysis view is contained inside Control view in wavetank-common.sbt -->
    <View Type="Analysis" Title="Solver" Label="Solver"
      AnalysisAttributeName="Analysis" AnalysisAttributeType="Analysis">
    </View>

    <View Type="Instanced" Title="blockMesh">
      <InstancedAttributes>
        <Att Type="Scale" Name="Scale" />
        <Att Type="BlockMesh" Name="BlockMesh">
          <ItemViews>
            <View Item="box" Type="Box" Min="point 0" Max="point 1" Angles="angles" ShowControls="true"/>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Type="Group" Title="Object" Label="Object" Style="Tiled">
      <Views>
        <View Title="ObjectPoint" />
        <View Title="SurfaceFeatures" />
      </Views>
    </View>

    <View Type="Instanced" Title="WaveModel" Label="Wave Model">
      <InstancedAttributes>
        <Att Type="WaveModel" Name="WaveModel" />
      </InstancedAttributes>
    </View>

    <View Title="Physics" Type="Group" Style="Tiled">
      <Views>
        <View Title="TransportProperties" />
        <View Title="WaveModel" />
      </Views>
    </View>

    <View Type="Instanced" Title="TransportProperties" Label="Transport Properties">
      <InstancedAttributes>
        <Att Type="TransportProperties" Name="TransportProperties" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="WaveModel" Label="Wave Model">
      <InstancedAttributes>
        <Att Type="WaveModel" Name="WaveModel" />
      </InstancedAttributes>
    </View>

  </Views>
</SMTK_AttributeResource>
