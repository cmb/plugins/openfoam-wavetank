<?xml version="1.0"?>
<SMTK_AttributeResource Version="5">

  <!-- Note that <Includes> paths must be relative to root .sbt file -->
  <Includes>
    <File>internal/templates/controlDict.sbt</File>
    <File>internal/templates/blockMesh.sbt</File>
    <File>internal/templates/snappyHexMesh.sbt</File>
    <File>internal/templates/refineMesh.sbt</File>
    <File>internal/templates/object.sbt</File>
    <File>internal/templates/physics.sbt</File>
  </Includes>

  <Definitions>
    <AttDef Type="Parallel">
      <Categories>
        <Cat>background</Cat>
      </Categories>
      <ItemDefinitions>
        <Group Name="Parallel" Label="Enable Parallel" Optional="true" IsEnabledByDefault="false">
          <ItemDefinitions>
            <Int Name="Subdomains" NumberOfRequiredValues="3">
              <ComponentLabels>
                <Label>X:</Label>
                <Label>Y:</Label>
                <Label>Z:</Label>
              </ComponentLabels>
              <DefaultValue>2,2,1</DefaultValue>
            </Int>
            <!-- <Directory Name="Directory"></Directory> -->
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <!--********** Views **********-->
  <Views>
    <View Type="Group" Title="Control" Label="Solver" Style="Tiled">
      <Views>
        <View Title="Solver" />
        <View Title="ControlDict" />
        <View Title="Parallel" />
        <View Title="RunSolver" />
        <View Title="ExportPackageControls" />
      </Views>
    </View>

    <View Type="Instanced" Title="Parallel" Label="Parallel">
      <InstancedAttributes>
        <Att Type="Parallel" Name="Parallel" />
      </InstancedAttributes>
    </View>

    <View Title="RunSolver" Type="WaveTankControls" Label="Run Solver Locally">
      <CaseDirectory Delete="true">analysis</CaseDirectory>
      <Operation>overinterdymfoam.OverInterDyMFoam</Operation>
      <ShellScript RunInParentDirectory="true" CanAbort="true">./merge_solve.sh</ShellScript>
      <Geometry Type="FoamFile">analysis.foam</Geometry>
    </View>

    <View Title="ExportPackageControls" Type="WaveTankControls" Label="Export Allrun Package">
      <Operation Export="true" UseDialog="true">create_allrun.CreateAllrun</Operation>
    </View>

    <View Type="Instanced" Title="ControlDict" Label="Time">
      <InstancedAttributes>
        <Att Type="controlDict" Name="controlDict" />
      </InstancedAttributes>
    </View>

    <View Type="Group" Title="refineMesh" Style="Tiled">
      <Views>
        <View Title="RefinementDirection" />
        <View Title="RefinementRegions" />
        <View Title="RefineMeshControls" />
      </Views>
    </View>

    <View Type="Instanced" Title="RefinementDirection" Label="Refinement Directions">
      <InstancedAttributes>
        <Att Type="RefinementDirection" Name="RefinementDirection" />
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="RefinementRegions" Label="Refinement Regions" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="RefinementRegion">
          <ItemViews>
            <View Item="box" Type="Box" Min="point 0" Max="point 1" Angles="angles" ShowControls="true"/>
          </ItemViews>
        </Att>
      </AttributeTypes>
    </View>

    <View Name="RefineMeshControls" Type="WaveTankControls" Label="refineMesh">
      <CaseDirectory>background</CaseDirectory>
      <Operation>refinemesh.RefineMesh</Operation>
      <FoamApplication>refineMesh</FoamApplication>
      <Geometry Type="FoamFile" Representation="Surface With Edges">backgroundMesh.foam</Geometry>
    </View>

    <View Type="Instanced" Title="ObjectPoint" Label="Location">
      <InstancedAttributes>
        <Att Type="Object" Name="Object">
          <ItemViews>
            <View Path="/InsidePoint" Type="Point" ShowControls="true" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="SurfaceFeatures" Label="Surface Features">
      <InstancedAttributes>
        <Att Type="SurfaceFeatures" Name="SurfaceFeatures" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="MassProperties" Label="Mass Properties">
      <InstancedAttributes>
        <Att Type="MassProperties" Name="MassProperties" />
      </InstancedAttributes>
    </View>


    <View Type="Group" Title="SnappyGroup" Label="snappyHexMesh" Style="Tiled">
      <Views>
        <View Title="ModelInstance" />
        <View Title="SurfaceFeatures" />
        <!-- <View Title="SurfaceFeatureControls" /> -->
        <View Title="Snappy" />
        <View Title="SnappyHexMeshControls" />
      </Views>
    </View>

    <View Type="Group" Title="Snappy" Label="Mesh Settings" TabPosition="North">
      <Views>
        <View Title="Castellation" />
        <View Title="Snapping" />
      </Views>
    </View>

    <View Type="Instanced" Title="Castellation">
      <InstancedAttributes>
        <Att Type="Castellation" Name="Castellation">
          <ItemViews>
            <View Path="/Castellation/InsidePoint" Type="Point" ShowControls="true" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="Snapping" >
      <InstancedAttributes>
        <Att Type="SnapControls" Name="SnapControls" />
      </InstancedAttributes>
    </View>

    <View Name="SnappyHexMeshControls" Type="WaveTankControls" Label="snappyHexMesh">
    <CaseDirectory>overset</CaseDirectory>
      <Operation>snappyhexmesh.SnappyHexMesh</Operation>
      <!-- <FoamApplication>snappyHexMesh</FoamApplication>
      <FoamArguments><Argument>-overwrite</Argument></FoamArguments> -->
      <ShellScript>./run_snappy.sh</ShellScript>
      <Geometry Type="FoamFile" Representation="Surface With Edges">snappy.foam</Geometry>
    </View>
    <!-- <Att Type="MeshQualityControls" Name="MeshQualityControls" /> -->
  </Views>

</SMTK_AttributeResource>
