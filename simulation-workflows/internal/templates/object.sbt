<?xml version="1.0"?>
<SMTK_AttributeResource Version="5">

  <Categories>
    <Cat>object</Cat>
  </Categories>

  <Definitions>
    <!-- Store filename internally -->
    <AttDef Type="Object">
      <Categories><Cat>object</Cat></Categories>
      <ItemDefinitions>
        <String Name="Filename" Label="Asset Filename" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false" />

        <Double Name="InsidePoint" Label="MODEL Point (Inside Object)" NumberOfRequiredValues="3">
          <BriefDescription>Specify point inside the object to prevent meshing inside.</BriefDescription>
          <DefaultValue>0.0,0.1,0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="SurfaceFeatures" Label="Suface Features">
      <Categories><Cat>object</Cat></Categories>
      <ItemDefinitions>
        <String Name="Edges" Label="Keep Edges">
          <ChildrenDefinitions>
            <Double Name="Angle" Label=" ">
              <DefaultValue>120.0</DefaultValue>
              <RangeInfo>
                <Min Include="false">0.0</Min>
                <Max Include="false">180.0</Max>
              </RangeInfo>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="No Edges">none</Value>
            <Structure>
              <Value Enum="Inner Angle">angle</Value>
              <Items><Item>Angle</Item></Items>
            </Structure>
            <Value Enum="All Edges">all</Value>
          </DiscreteInfo>
        </String>

        <Double Name="Tolerance" Label="surfaceFeature Tolerance">
          <DefaultValue>0.001</DefaultValue>
          <RangeInfo>
            <Min Include="false">0.0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="MassProperties">
      <Categories><Cat>object</Cat></Categories>
      <ItemDefinitions>
        <Double Name="Mass">
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo><Min Include="true">0.0</Min></RangeInfo>
        </Double>
        <Double Name="CenterOfMass" Label="Center of Mass" NumberOfRequiredValues="3">
          <ComponentLabels>
            <Label>X:</Label>
            <Label>Y:</Label>
            <Label>Z:</Label>
          </ComponentLabels>
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="MomentOfInertia" Label="Moment of Inertia" NumberOfRequiredValues="3">
          <ComponentLabels>
            <Label>Ix:</Label>
            <Label>Iy:</Label>
            <Label>Iz:</Label>
          </ComponentLabels>
          <DefaultValue>1.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

</SMTK_AttributeResource>
