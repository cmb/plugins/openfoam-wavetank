#!/bin/sh
cd "${0%/*}" || exit                                # Run from this directory
. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions        # Tutorial run functions
#------------------------------------------------------------------------------#

# Exit on error
set -e

# Copy background mesh to analysis case
rm -rf ./analysis/constant/polyMesh
echo Copying background mesh to analysis case
cp -r ./background/constant/polyMesh ./analysis/constant

# Merge overset mesh
rm -f log.mergeMeshes
runApplication mergeMeshes ./analysis ./overset -overwrite
touch ./analysis/analysis.foam

# Run solver
(cd analysis && ./Allrun)
