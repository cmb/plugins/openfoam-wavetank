FROM opencfd/openfoam-run:2112

# Install binutils so error messages provide stack trace info:
RUN apt-get update
RUN apt-get -y install binutils

# Corrections for use with CMB modelbuilder

# Fix entrypoint script per https://develop.openfoam.com/packaging/containers/-/issues/4
RUN sed -i 's,/bin/sh,/bin/bash,' /openfoam/run

# Remove warning about missing tutorials directory (not in openfoam-run image)
RUN sed -i '/FOAM_TUTORIAL/d' /usr/lib/openfoam/openfoam2112/bin/tools/RunFunctions
