# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

"""Simple operation to import all smtk modules. Load this *before* using python shell.

Use "File" => "Export Simulation" to load to operation. Then "Cancel" the export dialog.
Then it is safe to open and use the Python Shell view.

Note: tried adding the content of modelbuilder_setup.py to the operateInternal()
method, however, when executing the operation, modelbuilder crashes somewhere under
`pqSMTKWrapper::smtkOperationManger()`.
"""

# Import ALL smtk modules
import smtk
import smtk.attribute
import smtk.attribute_builder
import smtk.common
import smtk.extension.paraview.appcomponents
import smtk.io
import smtk.mesh
import smtk.model
import smtk.operation
import smtk.project
import smtk.resource
import smtk.session.mesh
import smtk.simulation
import smtk.testing
import smtk.view


class LoadSMTKOp(smtk.operation.Operation):
    def __init__(self):
        smtk.operation.Operation.__init__(self)

    def name(self):
        return "load smtk"

    def operateInternal(self):
        smtk.InfoMessage(self.log(), 'module:info: {}'.format(smtk))
        return self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)

    def createSpecification(self):
        spec = self.createBaseSpecification()
        op_def = spec.createDefinition('load smtk', 'operation')
        result_def = spec.createDefinition('load result', 'result')
        return spec
