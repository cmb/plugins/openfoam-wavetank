# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

"""Generates 2D wavetank project in hard-coded location on the file system.

Run this script in the modelbuilder Python Shell view.
Wave tank project must be registered and operations loaded before running this script.
Note that the path to the project directory is hard-coded in the PROJECT_PATH variable.
"""
import os
import sys

import smtk
import smtk.attribute
import smtk.operation
import smtk.project
import smtk.resource
import smtk.extension.paraview.appcomponents as appcomps


OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Hard coded relative path to 2D project
PROJECT_PATH = '../../../projects/overset_2d'

XMIN = -8.0
XMAX = 8.0
YMIN = 0.0
YMAX = 0.4

# Check for project directory
source_dir = os.path.abspath(os.path.dirname(__file__))
project_dir = os.path.abspath(os.path.join(source_dir, PROJECT_PATH))
if not os.path.exists(project_dir):
    os.makedirs(project_dir)
print('Using project dir {}'.format(project_dir))

# Get smtk managers
behavior = appcomps.pqSMTKBehavior.instance()
proj_manager = behavior.activeWrapperProjectManager()
res_manager = proj_manager.resourceManager()
op_manager = proj_manager.operationManager()

# User must close all projects and other resources
if proj_manager.find('smtk::project::Project'):
    raise RuntimeError('Close all smtk projects and rerun')
if res_manager.resources():
    raise RuntimeError('Close all smtk resources and rerun')

# Run CreateProject op
create_op = op_manager.createOperation('create_project.CreateProject')
create_op.parameters().findDirectory('directory').setValue(project_dir)
create_op.parameters().findString('solver').setValue('overInterDyMFoam')
create_op.parameters().findVoid('overwrite').setIsEnabled(True)
create_result = create_op.operate()
outcome = create_result.findInt('outcome').value()
if outcome != OP_SUCCEEDED:
    print(create_op.log().convertToString())
    raise RuntimeError('create operation outcome {}'.format(outcome))

# Get the attribute resource
project = create_result.findResource('resource').value()
att_resource_set = project.resources().findByRole('attributes')
att_resource = att_resource_set.pop()

# Update the background mesh attributes
mesh_att = att_resource.findAttribute('BackgroundBlockMesh')
box_item = mesh_att.findDouble('box')
box_item.setValue(0, XMIN)
box_item.setValue(1, XMAX)
box_item.setValue(2, YMIN)
box_item.setValue(3, YMAX)

size_item = mesh_att.findString('MeshSize')
size_item.setValue('numcells')
direction_item = size_item.findChild(
    'numcells-eachdir', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
direction_item.setValue(1, 1)  # number of cells in y direction

# Create refineMesh attribute
rr_defn = att_resource.findDefinition('RefinementRegion')
rr_att = att_resource.createAttribute(rr_defn)

# Load 2D geometry (wave object)
waveobject_path = os.path.join(source_dir, os.pardir, 'data/model/3d/obj/waveobject_thin.obj')
model_op = op_manager.createOperation('import_model.ImportModel')
model_op.parameters().associate(project)
model_op.parameters().findFile('filename').setValue(waveobject_path)
model_op.parameters().findVoid('overwrite').setIsEnabled(True)
model_result = model_op.operate()
outcome = model_result.findInt('outcome').value()
if outcome != OP_SUCCEEDED:
    print(model_op.log().convertToString())
    raise RuntimeError('ImportModel operation outcome {}'.format(outcome))

# Update the overset mesh attributes
mesh_att = att_resource.findAttribute('OversetBlockMesh')
box_item = mesh_att.findDouble('box')
box_item.setValue(0, XMIN // 2)
box_item.setValue(1, XMAX // 2)
box_item.setValue(2, YMIN)
box_item.setValue(3, YMAX)
box_item.setValue(4, -2.0)  # zmin
box_item.setValue(5, 2.0)  # zmzx

size_item = mesh_att.findString('MeshSize')
size_item.setValue('numcells')
direction_item = size_item.findChild(
    'numcells-eachdir', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
direction_item.setValue(1, 1)  # number of cells in y direction

cast_att = att_resource.findAttribute('Castellation')
point_item = cast_att.findDouble('InsidePoint')
point_item.setValue(0, -3.0)


# Write project to file system
write_op = op_manager.createOperation('smtk::project::Write')
write_op.parameters().associate(project)
write_result = write_op.operate()
write_outcome = write_result.findInt('outcome').value()
if write_outcome != OP_SUCCEEDED:
    print(write_op.log().convertToString())
    raise RuntimeError('write operation outcome {}'.format(outcome))

print('Wrote new project to {}'.format(project_dir))
