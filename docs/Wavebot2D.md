Wavebot 2D Example
===================


1\. Setup
----------

After starting modelbuilder, be sure to:

1.1 Use the File -> Export Simulation menu to load the `load_smtk._op.py` file.

1.2 Open the Python Shell and run the `modelbuilder_setup.py` script.

1.3 If you are using openfoam-docker, run the `docker_setup.py` script.


2\. New project
-------------------

2.1 Open the Operations view and find the `create_project.CreateProject` item.
    Double-click it to open the `Operation editor` field at the bottom of the view.

2.2 In the `Operation Editor`, use the `Browse` button to open a file system
    dialog. Create a directory or navigate to an existing directory to use for
    storing project files. For example, use a directory name such as `wavebot2d`.

2.3 In the `Solver` dropdown list, select `overInterDyMFoam`.

2.4 Click the `Apply` button. This will create a modelbuilder project that contains an
    attribute resource for specifying the simulation. The attribute resource UI is
    displayed in the `Attribute Editor` view.

2.5 In the `Attribute Editor` view, go to the `controlDict` tab. Find the `Solver`
    section and look for the dropdown list that is set to `Three Dimension (Default)`.
    Change it to `Two Dimension`.


3\. Geometry import
-------------------------

3.1 In the `Operations` panel, make sure the checkbox labeled `Limit by select`
    (near the top of the panel) is NOT checked.

3.2 In the `Operations` panel, find the `import_model.ImportModel` item and double
    click it to open the `Operation editor`.

3.3 In the editor, set the `Project` dropdown field to the project's name.

3.4 For the `filename` field, browse to the wavebot model file in the source repository
    at `data/model/3d/stl/waveBOT.stlb`.

3.5 Click the `Apply` button and note that the geometry is displayed in the 3D render
    view. (Use the "+Y" direction for the canonical display.)


4\. Overset mesh
-----------------

4.1 In the `Attribute Editor` view, select the `Overset Mesh` tab.

4.2 In the `Object` sub-tab, set these fields

* In the `Location` section, set `Point Inside Object` to < 0, 0, 0>

4.3 In the `blockMesh` sub-tab, set these fields

* Set `Position` to <-2, -0.1, -1>
* Set `Scale` to <4, 0.2, 2>
* Set `Mesh Size` to "Number of cells in each direction",
  and then set the values to <25, 1, 12>

4.4 In the `Operations` view, run the `blockmesh.BlockMesh` item.

* Double click to open the editor.
* Set `Project` to the dropdown field to the project's name.
* Set `Specification` to "OversetBlockMesh".
* Click the `Apply` button. The cursor should change to the "busy" icon
  for several seconds while blockMesh runs.

4.5 To view the block mesh,

* Click on the gray paraview icon to open the paraview toolbars and
  pipeline browser.
* Use the `File Open` menu item and navigate into the project folder.
  Select the file `<project-root>/foam/overset/overset.foam`.
* In the toolbar, change the surface representation to "Surface With Edges".


4.6 To generate the final overset mesh, follow these steps to run
    surfaceFeatureExtract, snappyHexMesh, and extrudeMesh.

* In the `Attribute Editor`, go to the `snappyHexMesh` sub-tab and set
  `Point Inside Mesh` to <-1, -0.05, 0>
* In the `Operation` view, find and double-click the
  `surfacefeatureextract.SurfaceFeatureExtract` item.
* Set the `Project` field to the project name and click the `Apply`
  button.
* In the `Operation` view, find and double-click the
  `snappyhexmesh.SnappyHexMesh` item.
* Set the `Project` field to the project name and click `Apply`.
  The cursor should change to the "busy" icon for some time,
  typically less than 15 seconds, while the mesher runs.
* In the `Operation` view, find and double-click the
  `extrudemesh.ExtrudeMesh` item.
* Set the `Project` field to the project name and click `Apply`.
  The cursor should change to the "busy" icon for several seconds
  while the extrusion code runs.
* To view the result, find the `overset.foam` item in the Pipeline
  browser, right click on it, and select `Reload Files` from the
  context menu.

5\. Background Mesh
---------------------

5.1 In the `Attribute Editor` view, select the `Background Mesh` tab.

5.2 In the `blockMesh` sub-tab, set these fields

* Set `Position` to <-15, -0.1, -4>
* Set `Scale` to <30, 0.2, 8>
* Set `Mesh Size` to "Number of cells in each direction",
  and then set the values to <100, 1, 20>

5.3 In the `Operations` view, run the `blockmesh.BlockMesh` item.

* Double click to open the editor.
* Set `Project` to the dropdown field to the project's name.
* Set `Specification` to "BackgroundBlockMesh".
* Click the `Apply` button. The cursor should change to the "busy" icon
  for several seconds while blockMesh runs.

5.4 To view the block mesh,

* Use the `File Open` menu item and navigate into the project folder.
  Select the file `<project-root>/foam/background/background.foam`.
* In the toolbar, change the surface representation to "Surface With Edges".

5.5 You can optionally refine regions in the background mesh. This is not
required for the baseline example, but the steps to do so are as follows:

* In the `Attribute Editor` view, select the `refineMesh` sub-tab.
* In the `Refinement Directions` section, only check all 2 boxes labeled `X`, `Z`.
* In the `Refinement Regions` section, click the `New` button to create the region.
* Set the `Position` and `Scale` fields or manipulate the widget to specify the regions.
* Create and edit additional `Refinement Regions` as desired.
* In the `Operations` view, select the `refineMesh.RefineMesh` item. Set the `Project`
  field and click the `Apply` button.

6\. Merge Meshes
-----------------

* In the `Operation` view, find and double-click the
  `mergemeshes.MergeMeshes` item.
* Set the `Project` field to the project name and click `Apply`.
  The cursor should change to the "busy" icon for several seconds
  while the mergeMeshes code runs.

7\. Set Fields
---------------
* In the `Operation` view, find and double-click the
  `setfields.SetFields` item.
* Set the `Project` field to the project name and click `Apply`.
  The cursor should change to the "busy" icon for several seconds
  while the mergeMeshes code runs.

8\. overInterDyMFoam Solver
----------------------------

8.1 In the `Attribute Editor` view, select the `Physics` tab, which has
    3 sections for editing mass properties, transport properties, and
    wave specifications

* For the `Mass` enter 75.
* Alternatively, you can also use the `estimate_mass.EstimateMass`
  operation to calculate mass from the water displacement. The operation
  uses the bounds of the overset mesh, the `Water surface` value, and
  the `Density (rho)` of water. The results is output to the
  `Output Messages` view. (You can copy text from that view when the
  `Show full messages` checkbox is checked.)
* For the remaining content on the `Physics` tab, the default values
  can be used.

8.2 In the `Operation` view, find and double-click the
  `overInterDyMFoam.OverInterDyMFoam` item.

* Set the `Project` field to the project name and click `Apply`.
  The cursor should change to the "busy" icon for some time,
  typically less than 30 seconds, while the solver runs.

8.3 To view the results,

* Use the `File Open` menu item and navigate into
  the project folder. Select the file
  `<project-root>/foam/analysis/overInterDyMFoam.foam`.
* In the field dropdown, select `alpha.water` to display

8.4 The default settings in the `controlDict` tab are set to only
  output 6 times steps (0.0, 0.1, 0.2, ... 0.5).
  To run a longer simulation,

* Change the `startFrom` dropdown to "latestTime".
* Change the `stopAt` value to some larger number, e.g., 20.0
* In the `Operation` view, find and double-click the
  `overInterDyMFoam.OverInterDyMFoam` item, then click the
  `Apply` button.
* This version of modelbuilder does not use the standard OpenFOAM
  naming convention for log files. You can still track progress with
  `tail -f` on the file at
  `<project-root>/foam/analysis/logs/overInterDymFoam.log`.

9\. Project Save/Restore
-------------------------

9.1 To save all changes in the `Attribute Editor`,

* In the `Operation View`, find the `smtk::project::Write` item and
  double click it.
* Set the `Project` field to the project name and click `Apply`.

9.2 To read a project into modelbuilder,

* In the `Operation View`, find the `smtk::project::Read` item and
  double click it.
* In the `File Name` field, enter the path to the .smtk file in the
  project directory. If the directory is named `wavebot2d` for example,
  the file to enter is named `wavebot2d.project.smtk`.
* Click the `Apply` button, and modelbuilder will load the project
  files, populate the `Attribute Editor` view, and display the 3D
  object in the renderview.
